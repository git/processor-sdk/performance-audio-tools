/*
*  Copyright {C} 2016 Texas Instruments Incorporated - http://www.ti.com/ 
*  ALL RIGHTS RESERVED 
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// Synchronous Rate Conversion alpha codes
//

#ifndef _SRC_A
#define _SRC_A

#include <paftyp_a.h>
#include <acpbeta.h>

#define  readSRCMode 0xc200+STD_BETA_SRC,0x0400
#define writeSRCModeDisable 0xca00+STD_BETA_SRC,0x0400
#define writeSRCModeEnable 0xca00+STD_BETA_SRC,0x0401

#define  readSRCRateRequest 0xc200+STD_BETA_SRC,0x0500
#define writeSRCRateRequestFull 0xca00+STD_BETA_SRC,0x0500
#define writeSRCRateRequestHalf 0xca00+STD_BETA_SRC,0x0501
#define writeSRCRateRequestQuarter 0xca00+STD_BETA_SRC,0x0502
#define writeSRCRateRequestDouble 0xca00+STD_BETA_SRC,0x0503
#define writeSRCRateRequestQuadruple 0xca00+STD_BETA_SRC,0x0504
#define writeSRCRateRequestMax192 0xca00+STD_BETA_SRC,0x0580
#define writeSRCRateRequestMin32 0xca00+STD_BETA_SRC,0x0580
#define writeSRCRateRequestMax96 0xca00+STD_BETA_SRC,0x0581
#define writeSRCRateRequestMax48 0xca00+STD_BETA_SRC,0x0582
#define writeSRCRateRequestMin64 0xca00+STD_BETA_SRC,0x0583
#define writeSRCRateRequestMin128 0xca00+STD_BETA_SRC,0x0584

#define  readSRCRateStream 0xc200+STD_BETA_SRC,0x0600
#define wroteSRCRateStreamFull 0xca00+STD_BETA_SRC,0x0600
#define wroteSRCRateStreamHalf 0xca00+STD_BETA_SRC,0x0601
#define wroteSRCRateStreamQuarter 0xca00+STD_BETA_SRC,0x0602
#define wroteSRCRateStreamDouble 0xca00+STD_BETA_SRC,0x0603
#define wroteSRCRateStreamQuadruple 0xca00+STD_BETA_SRC,0x0604

#define  readSRCSampleRate 0xc200+STD_BETA_SRC,0x0700
#define wroteSRCSampleRateUnknown 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_UNKNOWN
#define wroteSRCSampleRateNone 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_NONE
#define wroteSRCSampleRate11025Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_11025HZ
#define wroteSRCSampleRate12000Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_12000HZ
#define wroteSRCSampleRate16000Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_16000HZ
#define wroteSRCSampleRate22050Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_22050HZ
#define wroteSRCSampleRate24000Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_24000HZ
#define wroteSRCSampleRate32000Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_32000HZ
#define wroteSRCSampleRate44100Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_44100HZ
#define wroteSRCSampleRate48000Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_48000HZ
#define wroteSRCSampleRate64000Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_64000HZ
#define wroteSRCSampleRate88200Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_88200HZ
#define wroteSRCSampleRate96000Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_96000HZ
#define wroteSRCSampleRate128000Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_128000HZ
#define wroteSRCSampleRate176400Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_176400HZ
#define wroteSRCSampleRate192000Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_192000HZ

#define  readSRCStatus 0xc508,STD_BETA_SRC
#define  readSRCControl \
         readSRCMode, \
         readSRCRateRequest

#define  readSRC_A_Mode 0xc200+STD_BETA_SRC,0x0400
#define writeSRC_A_ModeDisable 0xca00+STD_BETA_SRC,0x0400
#define writeSRC_A_ModeEnable 0xca00+STD_BETA_SRC,0x0401

#define  readSRC_A_RateRequest 0xc200+STD_BETA_SRC,0x0500
#define writeSRC_A_RateRequestFull 0xca00+STD_BETA_SRC,0x0500
#define writeSRC_A_RateRequestHalf 0xca00+STD_BETA_SRC,0x0501
#define writeSRC_A_RateRequestQuarter 0xca00+STD_BETA_SRC,0x0502
#define writeSRC_A_RateRequestDouble 0xca00+STD_BETA_SRC,0x0503
#define writeSRC_A_RateRequestQuadruple 0xca00+STD_BETA_SRC,0x0504
#define writeSRC_A_RateRequestMax192 0xca00+STD_BETA_SRC,0x0580
#define writeSRC_A_RateRequestMin32 0xca00+STD_BETA_SRC,0x0580
#define writeSRC_A_RateRequestMax96 0xca00+STD_BETA_SRC,0x0581
#define writeSRC_A_RateRequestMax48 0xca00+STD_BETA_SRC,0x0582
#define writeSRC_A_RateRequestMin64 0xca00+STD_BETA_SRC,0x0583
#define writeSRC_A_RateRequestMin128 0xca00+STD_BETA_SRC,0x0584

#define  readSRC_A_RateStream 0xc200+STD_BETA_SRC,0x0600
#define wroteSRC_A_RateStreamFull 0xca00+STD_BETA_SRC,0x0600
#define wroteSRC_A_RateStreamHalf 0xca00+STD_BETA_SRC,0x0601
#define wroteSRC_A_RateStreamQuarter 0xca00+STD_BETA_SRC,0x0602
#define wroteSRC_A_RateStreamDouble 0xca00+STD_BETA_SRC,0x0603
#define wroteSRC_A_RateStreamQuadruple 0xca00+STD_BETA_SRC,0x0604

#define  readSRC_A_SampleRate 0xc200+STD_BETA_SRC,0x0700
#define wroteSRC_A_SampleRateUnknown 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_UNKNOWN
#define wroteSRC_A_SampleRateNone 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_NONE
#define wroteSRC_A_SampleRate11025Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_11025HZ
#define wroteSRC_A_SampleRate12000Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_12000HZ
#define wroteSRC_A_SampleRate16000Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_16000HZ
#define wroteSRC_A_SampleRate22050Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_22050HZ
#define wroteSRC_A_SampleRate24000Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_24000HZ
#define wroteSRC_A_SampleRate32000Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_32000HZ
#define wroteSRC_A_SampleRate44100Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_44100HZ
#define wroteSRC_A_SampleRate48000Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_48000HZ
#define wroteSRC_A_SampleRate64000Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_64000HZ
#define wroteSRC_A_SampleRate88200Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_88200HZ
#define wroteSRC_A_SampleRate96000Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_96000HZ
#define wroteSRC_A_SampleRate128000Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_128000HZ
#define wroteSRC_A_SampleRate176400Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_176400HZ
#define wroteSRC_A_SampleRate192000Hz 0xca00+STD_BETA_SRC,0x0700+PAF_SAMPLERATE_192000HZ

#define  readSRC_A_Status 0xc508,STD_BETA_SRC
#define  readSRC_A_Control \
         readSRC_A_Mode, \
         readSRC_A_RateRequest

#define  readSRC_B_Mode 0xc200+STD_BETA_SUC,0x0400
#define writeSRC_B_ModeDisable 0xca00+STD_BETA_SUC,0x0400
#define writeSRC_B_ModeEnable 0xca00+STD_BETA_SUC,0x0401

#define  readSRC_B_RateRequest 0xc200+STD_BETA_SUC,0x0500
#define writeSRC_B_RateRequestFull 0xca00+STD_BETA_SUC,0x0500
#define writeSRC_B_RateRequestHalf 0xca00+STD_BETA_SUC,0x0501
#define writeSRC_B_RateRequestQuarter 0xca00+STD_BETA_SUC,0x0502
#define writeSRC_B_RateRequestDouble 0xca00+STD_BETA_SUC,0x0503
#define writeSRC_B_RateRequestQuadruple 0xca00+STD_BETA_SUC,0x0504
#define writeSRC_B_RateRequestMax192 0xca00+STD_BETA_SUC,0x0580
#define writeSRC_B_RateRequestMin32 0xca00+STD_BETA_SUC,0x0580
#define writeSRC_B_RateRequestMax96 0xca00+STD_BETA_SUC,0x0581
#define writeSRC_B_RateRequestMax48 0xca00+STD_BETA_SUC,0x0582
#define writeSRC_B_RateRequestMin64 0xca00+STD_BETA_SUC,0x0583
#define writeSRC_B_RateRequestMin128 0xca00+STD_BETA_SUC,0x0584

#define  readSRC_B_RateStream 0xc200+STD_BETA_SUC,0x0600
#define wroteSRC_B_RateStreamFull 0xca00+STD_BETA_SUC,0x0600
#define wroteSRC_B_RateStreamHalf 0xca00+STD_BETA_SUC,0x0601
#define wroteSRC_B_RateStreamQuarter 0xca00+STD_BETA_SUC,0x0602
#define wroteSRC_B_RateStreamDouble 0xca00+STD_BETA_SUC,0x0603
#define wroteSRC_B_RateStreamQuadruple 0xca00+STD_BETA_SUC,0x0604

#define  readSRC_B_SampleRate 0xc200+STD_BETA_SUC,0x0700
#define wroteSRC_B_SampleRateUnknown 0xca00+STD_BETA_SUC,0x0700+PAF_SAMPLERATE_UNKNOWN
#define wroteSRC_B_SampleRateNone 0xca00+STD_BETA_SUC,0x0700+PAF_SAMPLERATE_NONE
#define wroteSRC_B_SampleRate11025Hz 0xca00+STD_BETA_SUC,0x0700+PAF_SAMPLERATE_11025HZ
#define wroteSRC_B_SampleRate12000Hz 0xca00+STD_BETA_SUC,0x0700+PAF_SAMPLERATE_12000HZ
#define wroteSRC_B_SampleRate16000Hz 0xca00+STD_BETA_SUC,0x0700+PAF_SAMPLERATE_16000HZ
#define wroteSRC_B_SampleRate22050Hz 0xca00+STD_BETA_SUC,0x0700+PAF_SAMPLERATE_22050HZ
#define wroteSRC_B_SampleRate24000Hz 0xca00+STD_BETA_SUC,0x0700+PAF_SAMPLERATE_24000HZ
#define wroteSRC_B_SampleRate32000Hz 0xca00+STD_BETA_SUC,0x0700+PAF_SAMPLERATE_32000HZ
#define wroteSRC_B_SampleRate44100Hz 0xca00+STD_BETA_SUC,0x0700+PAF_SAMPLERATE_44100HZ
#define wroteSRC_B_SampleRate48000Hz 0xca00+STD_BETA_SUC,0x0700+PAF_SAMPLERATE_48000HZ
#define wroteSRC_B_SampleRate64000Hz 0xca00+STD_BETA_SUC,0x0700+PAF_SAMPLERATE_64000HZ
#define wroteSRC_B_SampleRate88200Hz 0xca00+STD_BETA_SUC,0x0700+PAF_SAMPLERATE_88200HZ
#define wroteSRC_B_SampleRate96000Hz 0xca00+STD_BETA_SUC,0x0700+PAF_SAMPLERATE_96000HZ
#define wroteSRC_B_SampleRate128000Hz 0xca00+STD_BETA_SUC,0x0700+PAF_SAMPLERATE_128000HZ
#define wroteSRC_B_SampleRate176400Hz 0xca00+STD_BETA_SUC,0x0700+PAF_SAMPLERATE_176400HZ
#define wroteSRC_B_SampleRate192000Hz 0xca00+STD_BETA_SUC,0x0700+PAF_SAMPLERATE_192000HZ

#define  readSRC_B_Status 0xc508,STD_BETA_SUC
#define  readSRC_B_Control \
         readSRC_B_Mode, \
         readSRC_B_RateRequest

#endif /* _SRC_A */

