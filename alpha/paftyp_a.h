
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework General Constant Declarations
//
//
//

#ifndef PAFTYP_A_
#define PAFTYP_A_

// Basic system parameters:

#ifndef PAF_MAXNUMCHAN
#define PAF_MAXNUMCHAN 32
#endif /* PAF_MAXNUMCHAN */

#ifndef ATMOS
#define ATMOS
#endif 

#ifndef FULL_SPEAKER
#define FULL_SPEAKER
#endif 
// PA/F Extended channels:

#define PAF_MAXNUMCHAN_HD 32

// Audio frame data types:

#ifdef PAF_MAXNUMCHAN_HD
#define PAF_MAXNUMCHAN_AF PAF_MAXNUMCHAN_HD
#else /* PAF_MAXNUMCHAN_HD */
#define PAF_MAXNUMCHAN_AF PAF_MAXNUMCHAN
#endif /* PAF_MAXNUMCHAN_HD */

// PAF_AudioData is the type of data carried in the stream.

#define PAF_AUDIODATATYPE_DOUBLE 0
#define PAF_AUDIODATATYPE_FLOAT 1
#define PAF_AUDIODATATYPE_INT 2
#define PAF_AUDIODATATYPE_INT32 2
#define PAF_AUDIODATATYPE_INT16 3
#define PAF_AUDIODATATYPE_INT8 4

#ifndef PAF_AUDIODATATYPE
#ifdef _PATE_
#define PAF_AUDIODATATYPE PAF_AUDIODATATYPE_DOUBLE
#else /* _PATE_ */
#define PAF_AUDIODATATYPE PAF_AUDIODATATYPE_FLOAT
#endif /* _PATE_ */
#endif /* PAF_AUDIODATATYPE */

#define PAF_AUDIODATATYPE_FIXED (PAF_AUDIODATATYPE >= PAF_AUDIODATATYPE_INT)

// PAF_ChannelMask indicates which channels in the audio frame data
// carry content (1) and which are to be ignored (0).

#define PAF_CHANNELMASK ~(~0<<PAF_MAXNUMCHAN-1)

//
// Channel usage enumeration:
//
// Channel usage is dependent upon the maximum number of channels.
//
// PAF_X enumerates that channel. Channels used for singletons are always
// the left of the corresponding pair (SURR=LSUR, etc.).
//
// PAF_Y_0 starts the Y channels, and PAF_Y_N is one past it:
// Y = FRoNT, REAR, BASS (subwoofer/lfe), and HEADphone, in that order.
//
// - Front-channel range: PAF_FRNT_0<=Y<PAF_FRNT_N.
// - Rear-channel range: PAF_REAR_0<=Y<PAF_REAR_N.
// - Satellite-channel range: PAF_FRNT_0<=Y<PAF_REAR_N.
// - Bass-channel range: PAF_BASS_0<=Y<PAF_BASS_N.
// - Headphone-channel range: PAF_HEAD_0<=Y<PAF_HEAD_N.
//

#if PAF_MAXNUMCHAN == 2
#define PAF_LEFT   0
#define PAF_RGHT   1
#define PAF_FRNT_0 0
#define PAF_FRNT_N 2
#define PAF_REAR_0 2
#define PAF_REAR_N 2
#define PAF_BASS_0 2
#define PAF_BASS_N 2
#define PAF_HEAD_0 2
#define PAF_HEAD_N 2
#elif PAF_MAXNUMCHAN == 4
#define PAF_LEFT   0
#define PAF_RGHT   1
#define PAF_CNTR   2
#define PAF_SURR   3
#define PAF_FRNT_0 0
#define PAF_FRNT_N 3
#define PAF_REAR_0 3
#define PAF_REAR_N 4
#define PAF_BASS_0 4
#define PAF_BASS_N 4
#define PAF_HEAD_0 4
#define PAF_HEAD_N 4
#elif PAF_MAXNUMCHAN == 6
#define PAF_LEFT   0
#define PAF_RGHT   1
#define PAF_CNTR   2
#define PAF_SURR   3
#define PAF_LSUR   3
#define PAF_RSUR   4
#define PAF_SUBW   5
#define PAF_FRNT_0 0
#define PAF_FRNT_N 3
#define PAF_REAR_0 3
#define PAF_REAR_N 5
#define PAF_BASS_0 5
#define PAF_BASS_N 6
#define PAF_HEAD_0 6
#define PAF_HEAD_N 6
#elif PAF_MAXNUMCHAN == 8
#define PAF_LEFT   0
#define PAF_RGHT   1
#define PAF_CNTR   2
#define PAF_SURR   3
#define PAF_LSUR   3
#define PAF_RSUR   4
#define PAF_BACK   5
#define PAF_LBAK   5
#define PAF_RBAK   6
#define PAF_SUBW   7
#define PAF_FRNT_0 0
#define PAF_FRNT_N 3
#define PAF_REAR_0 3
#define PAF_REAR_N 7
#define PAF_BASS_0 7
#define PAF_BASS_N 8
#define PAF_HEAD_0 8
#define PAF_HEAD_N 8
#elif PAF_MAXNUMCHAN == 16
#ifdef ATMOS
#define PAF_LEFT   0
#define PAF_RGHT   1
#define PAF_CNTR   2
#define PAF_LCTR   2
#define PAF_RCTR   3
#define PAF_WIDE   4
#define PAF_LWID   4
#define PAF_RWID   5

#ifdef FULL_SPEAKER
#define PAF_LTRR   18
#define PAF_RTRR   19
#define PAF_LTRH   20
#define PAF_RTRH   21
#else
#define PAF_LTRR   4
#define PAF_RTRR   5
#define PAF_LTRH   4
#define PAF_RTRH   5
#endif

#define PAF_OVER   6
#define PAF_LOVR   6
#define PAF_ROVR   7
#define PAF_LTMD   6
#define PAF_RTMD   7
#define PAF_SURR   8
#define PAF_LSUR   8
#define PAF_RSUR   9
#define PAF_BACK   10
#define PAF_LBAK   10
#define PAF_RBAK   11
#define PAF_SUBW   12
#define PAF_LSUB   12
#define PAF_RSUB   13
#define PAF_HEAD   14
#define PAF_LHED   14
#define PAF_RHED   15
#define PAF_LTFT   14
#define PAF_RTFT   15

#ifdef FULL_SPEAKER
#define PAF_LTFH   16
#define PAF_RTFH   17
#else
#define PAF_LTFH   14
#define PAF_RTFH   15
#endif

#define PAF_FRNT_0 0
#define PAF_FRNT_N 8
#define PAF_REAR_0 8
#define PAF_REAR_N 12
#define PAF_BASS_0 12
#define PAF_BASS_N 14
#define PAF_HEAD_0 14
#define PAF_HEAD_N 16
#else
#define PAF_LEFT   0
#define PAF_RGHT   1
#define PAF_CNTR   2
#define PAF_LCTR   2
#define PAF_RCTR   3
#define PAF_WIDE   4
#define PAF_LWID   4
#define PAF_RWID   5
#define PAF_OVER   6
#define PAF_LOVR   6
#define PAF_ROVR   7
#define PAF_SURR   8
#define PAF_LSUR   8
#define PAF_RSUR   9
#define PAF_BACK   10
#define PAF_LBAK   10
#define PAF_RBAK   11
#define PAF_SUBW   12
#define PAF_LSUB   12
#define PAF_RSUB   13
#define PAF_HEAD   14
#define PAF_LHED   14
#define PAF_RHED   15
#define PAF_FRNT_0 0
#define PAF_FRNT_N 8
#define PAF_REAR_0 8
#define PAF_REAR_N 12
#define PAF_BASS_0 12
#define PAF_BASS_N 14
#define PAF_HEAD_0 14
#define PAF_HEAD_N 16
#endif
#elif PAF_MAXNUMCHAN == 32
#ifdef ATMOS
#define PAF_LEFT   0
#define PAF_RGHT   1
#define PAF_CNTR   2
#define PAF_LCTR   2
#define PAF_RCTR   3
#define PAF_WIDE   4
#define PAF_LWID   4
#define PAF_RWID   5

#ifdef FULL_SPEAKER
#define PAF_LTRR   26
#define PAF_RTRR   27
#define PAF_LTRH   20
#define PAF_RTRH   21
#else
#define PAF_LTRR   4
#define PAF_RTRR   5
#define PAF_LTRH   4
#define PAF_RTRH   5
#endif

#define PAF_OVER   6
#define PAF_LOVR   6
#define PAF_ROVR   7
#define PAF_LTMD   6
#define PAF_RTMD   7
#define PAF_SURR   8
#define PAF_LSUR   8
#define PAF_RSUR   9
#define PAF_BACK   10
#define PAF_LBAK   10
#define PAF_RBAK   11
#define PAF_SUBW   12
#define PAF_LSUB   12
#define PAF_RSUB   13
#define PAF_HEAD   14
#define PAF_LHED   14
#define PAF_RHED   15
#define PAF_LTFT   14
#define PAF_RTFT   15

#ifdef FULL_SPEAKER
#define PAF_LTFH   3
#define PAF_RTFH   22
#else
#define PAF_LTFH   14
#define PAF_RTFH   15
#endif

#define PAF_FRNT_0 0
#define PAF_FRNT_N 8
#define PAF_REAR_0 8
#define PAF_REAR_N 12
#define PAF_BASS_0 12
#define PAF_BASS_N 14
#define PAF_HEAD_0 14
#define PAF_HEAD_N 16
#else /* PAF_MAXNUMCHAN */
#error unsupported option
#endif /* PAF_MAXNUMCHAN */
#endif

// ............................................................................

#ifdef PAF_MAXNUMCHAN_HD


#ifdef ATMOS
#define PAF_CHED    6	// = Cvh
#define PAF_OVER_HD 7	// = Ts
#define PAF_LSD	    6	//Newly Added
#define PAF_RSD	    7	//Newly Added
#define PAF_LCTR_HD 6	// = Lc
#define PAF_RCTR_HD 7	// = Rc
#define PAF_LHSI    6	// = Lts
#define PAF_RHSI    7	// = Rts


#define PAF_LS1	   16	//Left Surround 1
#define PAF_RS1	   17	//Right surround 1
#define PAF_LS2	   18	//Left Surround 2
#define PAF_RS2	   19	//Right surround 2
#define PAF_LHBK   20	//Height Left Back for DTS
#define PAF_RHBK   21	//Height Right Back for DTS
#define PAF_CHBK   22	//Height Center Back for DTS
#define PAF_CS     23	//Center Surround
#define PAF_LCS    24	//Left CS
#define PAF_RCS    25	//Right CS
#define PAF_LRS1   26	//Left Surround 1
#define PAF_RRS1   27	//Right surround 1
#define PAF_LRS2   28	//Left Surround 2
#define PAF_RRS2   29	//Right surround 2
#define PAF_LSC	   30	//Left Screen
#define PAF_RSC	   31	//Right Screen


#else
// the following channels are replaced in HD system with PAF_XXXX_HD counterparts
#undef  PAF_LCTR  // 2
#undef  PAF_RCTR  // 3
#undef  PAF_OVER  // 6
#define PAF_CHED   16
#define PAF_OVER_HD   17
#define PAF_LHSI   18
#define PAF_RHSI   19
#define PAF_LHBK   20
#define PAF_RHBK   21
#define PAF_CHBK   22
#define PAF_LCTR_HD   26
#define PAF_RCTR_HD   27
#endif
#endif /* PAF_MAXNUMCHAN_HD */

// ............................................................................

#ifdef PAF_SUBW
#define PAF_LFE PAF_SUBW
#endif /* PAF_SUBW */

// Channel form enumeration:

#define PAF_FORM_NSSE 0
#define PAF_FORM_YSSE 1
#define PAF_FORM_NBSE 2
#define PAF_FORM_YBSE 3
#define PAF_FORM_MONO 4
#define PAF_FORM_DUAL 5

// Sample rate enumeration:
//
// A sample rate may be supported, approximated, or recognized by any 
// particular PA Framework:
//   Supported sample rates are fully implemented.
//   Approximated sample rates are implemented but only approximately using
//     filter coefficients or other of another sample rate.
//   Recognized sample rates are not implemented. They are typically used
//     only for indicators.
//
// In no case should any sample rate value produce an error.
//
// The values given for sample rates below is universal. The interpretation 
// of sample rates given below is typical but not universal. Any particular 
// PA Framework may provide any sample rate interpretation.

#define PAF_SAMPLERATE_UNKNOWN 0 /* approximated as 48 kHz */
#define PAF_SAMPLERATE_NONE 1 /* recognized */
#define PAF_SAMPLERATE_32000HZ 2 /* supported */
#define PAF_SAMPLERATE_44100HZ 3 /* supported */
#define PAF_SAMPLERATE_48000HZ 4 /* supported */
#define PAF_SAMPLERATE_88200HZ 5 /* supported */
#define PAF_SAMPLERATE_96000HZ 6 /* supported */
#define PAF_SAMPLERATE_192000HZ 7 /* supported */
#define PAF_SAMPLERATE_64000HZ 8 /* recognized */
#define PAF_SAMPLERATE_128000HZ 9 /* recognized */
#define PAF_SAMPLERATE_176400HZ 10 /* recognized */
#define PAF_SAMPLERATE_8000HZ 11 /* recognized */
#define PAF_SAMPLERATE_11025HZ 12 /* recognized */
#define PAF_SAMPLERATE_12000HZ 13 /* recognized */
#define PAF_SAMPLERATE_16000HZ 14 /* recognized */
#define PAF_SAMPLERATE_22050HZ 15 /* recognized */
#define PAF_SAMPLERATE_24000HZ 16 /* recognized */
#define PAF_SAMPLERATE_N 17

// IEC Channel Status Information
#define PAF_IEC_AUDIOMODE_UNKNOWN  0
#define PAF_IEC_AUDIOMODE_AUDIO    1
#define PAF_IEC_AUDIOMODE_NONAUDIO 2

#define PAF_IEC_PREEMPHASIS_UNKNOWN  0
#define PAF_IEC_PREEMPHASIS_NO       1
#define PAF_IEC_PREEMPHASIS_YES      2

// Alpha Types
#define ALPHA_READ  0xc0
#define ALPHA_WRITE 0xc8

#define ALPHA_READ_5_N(N)  (((ALPHA_READ+5)<<8)+(N))
#define ALPHA_WRITE_5_N(N) (((ALPHA_WRITE+5)<<8)+(N))

// prefix for sending alpha code so as to generate no response
// 0xcd:0b = alpha:eta (Type 5-11 Write--"Synchronized Write")
//     5+N = lambda (subsequent length, in (16-bit) words;
//                   N=no. of words of alpha code to follow)
// 0x00:04 = chi    (comparison--always true) : phi (delay function--sleep)
//       0 = kappa  (repeat count)
//       0 = tau    (delay between comparisons)
//     0,0 = delta  (compare-to value--arbitrary w/ chi=0)
#define writeNoResponseN(N) ALPHA_WRITE_5_N(11),5+(N),0x0004,0,0,0,0

// start of defines used by DTSX
// PAF_MAP defines give a name to a speaker channel.
// These maps are used with a mask to specify non-standard speaker layouts.
#define PAF_MAP_LEFT        0   // primary left front: 30-45 degrees
#define PAF_MAP_RIGHT       1   // primary right front: 30-45 degrees
#define PAF_MAP_CENTER      2   // primary single center: 0 degrees
#define PAF_MAP_LSUR        3   // Left Side Surround: 90 degrees
#define PAF_MAP_RSUR        4   // Right Side Surround: 90 degrees
#define PAF_MAP_LBACK       5   // Left Back:  135-150 degrees
#define PAF_MAP_RBACK       6   // Right Back:  135-150 degrees
#define PAF_MAP_CSUR        7   // Center Surround: 180 degrees
#define PAF_MAP_LWIDE       8   // Wide:  Left of left speaker: 50-70 degrees
#define PAF_MAP_RWIDE       9   // Right of right speaker: 50-70 degrees
#define PAF_MAP_LCTR        10  // Left center: 15-30 degrees
#define PAF_MAP_RCTR        11  // right center: 15-30 degrees
#define PAF_MAP_LSUR1       12  // Left Surround 1: 75 degrees (Dolby)
#define PAF_MAP_RSUR1       13  // Right surround 1: 75 degrees (Dolby)
#define PAF_MAP_LOVR        14  // Top front left: 45 up, 45 off center. FH for Dolby.
#define PAF_MAP_ROVR        15  // Top front right: 45 up, 45 off center. FH for Dolby.
#define PAF_MAP_LHIGH_FRONT 14  // Top front left: 45 up, 45 off center. FH for Dolby.
#define PAF_MAP_RHIGH_FRONT 15  // Top front right: 45 up, 45 off center. FH for Dolby.
#define PAF_MAP_LTOP_MID    16  // Left half of a pair directly overhead.  "top middle"
#define PAF_MAP_RTOP_MID    17  // Right half of a pair directly overhead.  "top middle"
#define PAF_MAP_LTOP_FRNT   18  // left top front
#define PAF_MAP_RTOP_FRNT   19  // right top front
#define PAF_MAP_LTOP_REAR   20  // Left Top rear
#define PAF_MAP_RTOP_REAR   21  // Right Top Rear
#define PAF_MAP_LHIGH_BAK   22  // Height Left Back for DTS
#define PAF_MAP_RHIGH_BAK   23  // Height Right Back for DTS
#define PAF_MAP_C_SUB       24  // Center Subwoofer
#define PAF_MAP_R_SUB       25  // Right subwoofer
#define PAF_MAP_LSCREEN     26  // Left screen (Dolby) 10 degrees off center
#define PAF_MAP_RSCREEN     27  // Right screen (Dolby) 10 degrees off center
#define PAF_MAP_LSUR2       28  // Left Surround 2  (Dolby) 100 degrees
#define PAF_MAP_RSUR2       29  // Right surround 2 (Dolby) 100 degrees
#define PAF_MAP_LREAR_SUR1  30  // Left rear Surround 1 (Dolby) 120 degrees
#define PAF_MAP_RREAR_SUR1  31  // Right rear surround 1 (Dolby) 120 degrees
#define PAF_MAP_LREAR_SUR2  32  // Left rear Surround 2 (Dolby) 160 degrees
#define PAF_MAP_RREAR_SUR2  33  // Right rear surround 2  (Dolby) 160 degrees
#define PAF_MAP_LCTR_SUR    34  // Left center surround (Dolby) 170 degrees
#define PAF_MAP_RCTR_SUR    35  // Right center surround (Dolby) 170 degrees
#define PAF_MAP_CTR_HIGH    36  // Center high (DTS) center over the screen
#define PAF_MAP_TOP_CTR_SUR 37  // Directly overhead
#define PAF_MAP_LOW_LF      38  // Left front low
#define PAF_MAP_LOW_CF      39  // center front low
#define PAF_MAP_LOW_RF      40  // Right front low
#define PAF_MAP_CHBK        41  // Height Center Back for DTS 
#define PAF_MAP_LSUB        42  // Left subwoofer
// end of defines used by DTSX
#endif  /* PAFTYP_A_ */
