
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Alpha Code Processor Customer Series Beta declarations
//
//
//

#ifndef _CUSBETA_A
#define _CUSBETA_A

//
// Beta unit numbers in this series are reserved to the customer. These
// assignments can be changed arbitrarily as desired. Those give here are
// for the purpose of example only.
//
// The number of custom beta units provided differs according to system:
//
// - PA SDK5: "Number of CUS Beta codes supported is 512" (paf-migration.pdf)
//

#define CUS_BETA_AE            0x00 /* I, D, Y, and Z topologies */
#define CUS_BETA_CASS          0x01 /* D and Y topologies only */
#define CUS_BETA_CASJ          0x02 /* Z topology only */
#define CUS_BETA_CSS           0x03 /* I, D, Y, and Z topologies */
#define CUS_BETA_UNUSED4       0x04
#define CUS_BETA_UNUSED5       0x05
#define CUS_BETA_UNUSED6       0x06

#endif /* _CUSBETA_A */
