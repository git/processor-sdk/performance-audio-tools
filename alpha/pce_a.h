
/*
*  Copyright {C} 2016 Texas Instruments Incorporated - http://www.ti.com/ 
*  ALL RIGHTS RESERVED 
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// PCM Encoder alpha codes
//
//
//

#ifndef _PCE_A
#define _PCE_A

#include <paftyp_a.h>
#include <acpbeta.h>

// The PCE_VERSION build control should be used to distinguish PCE1
// from PCE2.

#ifndef PCE_VERSION
#define PCE_VERSION 2
#endif /* PCE_VERSION */

#define  readPCEMode 0xc200+STD_BETA_PCE,0x0400
/* Note: this should not be disabled */
#define  writePCEModeEnable 0xca00+STD_BETA_PCE,0x0401

#define PCE_PHASE_BASE 0x06
#define PCE_PHASE_LENGTH 0x0c

#define  readPCEModePhase0Mode 0xc200+STD_BETA_PCE,(PCE_PHASE_BASE << 8)+0x00
#define writePCEModePhase0ModeDisable 0xca00+STD_BETA_PCE,(PCE_PHASE_BASE << 8)+0x00
#define writePCEModePhase0ModeEnable 0xca00+STD_BETA_PCE,(PCE_PHASE_BASE << 8)+0x01

#define  readPCEModePhase0Type 0xc200+STD_BETA_PCE,((PCE_PHASE_BASE+1) << 8)+0x00
#define writePCEModePhase0TypeUnused 0xca00+STD_BETA_PCE,((PCE_PHASE_BASE+1) << 8)+0x00

#define  readPCEModePhase1Mode 0xc200+STD_BETA_PCE,((PCE_PHASE_BASE+2) << 8)+0x00
#define writePCEModePhase1ModeDisable 0xca00+STD_BETA_PCE,((PCE_PHASE_BASE+2) << 8)+0x00
#define writePCEModePhase1ModeEnable 0xca00+STD_BETA_PCE,((PCE_PHASE_BASE+2) << 8)+0x01

#define  readPCEModePhase1Type 0xc200+STD_BETA_PCE,((PCE_PHASE_BASE+3) << 8)+0x00
#define writePCEModePhase1TypeUnused 0xca00+STD_BETA_PCE,((PCE_PHASE_BASE+3) << 8)+0x00

#define  readPCEModePhase2Mode 0xc200+STD_BETA_PCE,((PCE_PHASE_BASE+4) << 8)+0x00
#define writePCEModePhase2ModeDisable 0xca00+STD_BETA_PCE,((PCE_PHASE_BASE+4) << 8)+0x00
#define writePCEModePhase2ModeEnable 0xca00+STD_BETA_PCE,((PCE_PHASE_BASE+4) << 8)+0x01

#define  readPCEModePhase2Type 0xc200+STD_BETA_PCE,((PCE_PHASE_BASE+5) << 8)+0x00
#define writePCEModePhase2TypeUnused 0xca00+STD_BETA_PCE,((PCE_PHASE_BASE+5) << 8)+0x00

#define  readPCEModePhase3Mode 0xc200+STD_BETA_PCE,((PCE_PHASE_BASE+6) << 8)+0x00
#define writePCEModePhase3ModeDisable 0xca00+STD_BETA_PCE,((PCE_PHASE_BASE+6) << 8)+0x00
#define writePCEModePhase3ModeEnable 0xca00+STD_BETA_PCE,((PCE_PHASE_BASE+6) << 8)+0x01

#define  readPCEModePhase3Type 0xc200+STD_BETA_PCE,((PCE_PHASE_BASE+7) << 8)+0x00
#define writePCEModePhase3TypeUnused 0xca00+STD_BETA_PCE,((PCE_PHASE_BASE+7) << 8)+0x00

#if PCE_VERSION == 2

#define  readPCEModePhase4Mode 0xc200+STD_BETA_PCE,((PCE_PHASE_BASE+8) << 8)+0x00
#define writePCEModePhase4ModeDisable 0xca00+STD_BETA_PCE,((PCE_PHASE_BASE+8) << 8)+0x00
#define writePCEModePhase4ModeEnable 0xca00+STD_BETA_PCE,((PCE_PHASE_BASE+8) << 8)+0x01

#define  readPCEModePhase4Type 0xc200+STD_BETA_PCE,((PCE_PHASE_BASE+9) << 8)+0x00
#define writePCEModePhase4TypeUnused 0xca00+STD_BETA_PCE,((PCE_PHASE_BASE+9) << 8)+0x00

#define  readPCEModePhase5Mode 0xc200+STD_BETA_PCE,((PCE_PHASE_BASE+10) << 8)+0x00
#define writePCEModePhase5ModeDisable 0xca00+STD_BETA_PCE,((PCE_PHASE_BASE+10) << 8)+0x00
#define writePCEModePhase5ModeEnable 0xca00+STD_BETA_PCE,((PCE_PHASE_BASE+10) << 8)+0x01

#define  readPCEModePhase5Type 0xc200+STD_BETA_PCE,((PCE_PHASE_BASE+11) << 8)+0x00
#define writePCEModePhase5TypeUnused 0xca00+STD_BETA_PCE,((PCE_PHASE_BASE+11) << 8)+0x00

#define  readPCEVOLMode readPCEModePhase0Mode
#define writePCEVOLModeDisable writePCEModePhase0ModeDisable
#define writePCEVOLModeEnable writePCEModePhase0ModeEnable

#define  readPCEVOLType readPCEModePhase0Type
#define writePCEVOLTypeUnused writePCEModePhase0TypeUnused

#define  readPCEDELMode readPCEModePhase1Mode
#define writePCEDELModeDisable writePCEModePhase1ModeDisable
#define writePCEDELModeEnable writePCEModePhase1ModeEnable

#define  readPCEDELType readPCEModePhase1Type
#define writePCEDELTypeUnused writePCEModePhase1TypeUnused

#define  readPCEOUTMode readPCEModePhase2Mode
#define writePCEOUTModeDisable writePCEModePhase2ModeDisable
#define writePCEOUTModeEnable writePCEModePhase2ModeEnable

#define  readPCEOUTType readPCEModePhase2Type
#define writePCEOUTTypeUnused writePCEModePhase2TypeUnused

// PCE SLD Alpha codes
#define PCE_PHASE_DELAY_BASE PCE_PHASE_BASE+PCE_PHASE_LENGTH 
#define PCE_PHASE_DELAY_LENGTH 0x04

#define  readPCEDELUnit 0xc200+STD_BETA_PCE,((PCE_PHASE_DELAY_BASE+1) << 8)+0x00
#define writePCEDELUnitTimeSamples 0xca00+STD_BETA_PCE,((PCE_PHASE_DELAY_BASE+1) << 8)+0x00
#define writePCEDELUnitTimeMillisecondsQ0 0xca00+STD_BETA_PCE,((PCE_PHASE_DELAY_BASE+1) << 8)+0x01
#define writePCEDELUnitTimeMillisecondsQ1 0xca00+STD_BETA_PCE,((PCE_PHASE_DELAY_BASE+1) << 8)+0x02
#define writePCEDELUnitTimeCentimeters 0xca00+STD_BETA_PCE,((PCE_PHASE_DELAY_BASE+1) << 8)+0x03
#define writePCEDELUnitTimeFeet 0xca00+STD_BETA_PCE,((PCE_PHASE_DELAY_BASE+1) << 8)+0x04
#define writePCEDELUnitTimeYards 0xca00+STD_BETA_PCE,((PCE_PHASE_DELAY_BASE+1) << 8)+0x05
#define writePCEDELUnitTimeMeters 0xca00+STD_BETA_PCE,((PCE_PHASE_DELAY_BASE+1) << 8)+0x06
#define writePCEDELUnitTimeDecimilliseconds 0xca00+STD_BETA_PCE,((PCE_PHASE_DELAY_BASE+1) << 8)+0x07
#define writePCEDELUnitLocationSamples 0xca00+STD_BETA_PCE,((PCE_PHASE_DELAY_BASE+1) << 8)+0x80
#define writePCEDELUnitLocationMillisecondsQ0 0xca00+STD_BETA_PCE,((PCE_PHASE_DELAY_BASE+1) << 8)+0x81
#define writePCEDELUnitLocationMillisecondsQ1 0xca00+STD_BETA_PCE,((PCE_PHASE_DELAY_BASE+1) << 8)+0x82
#define writePCEDELUnitLocationCentimeters 0xca00+STD_BETA_PCE,((PCE_PHASE_DELAY_BASE+1) << 8)+0x83
#define writePCEDELUnitLocationFeet 0xca00+STD_BETA_PCE,((PCE_PHASE_DELAY_BASE+1) << 8)+0x84
#define writePCEDELUnitLocationYards 0xca00+STD_BETA_PCE,((PCE_PHASE_DELAY_BASE+1) << 8)+0x85
#define writePCEDELUnitLocationMeters 0xca00+STD_BETA_PCE,((PCE_PHASE_DELAY_BASE+1) << 8)+0x86
#define writePCEDELUnitLocationDecimilliseconds 0xca00+STD_BETA_PCE,((PCE_PHASE_DELAY_BASE+1) << 8)+0x87

#define writePCEDELUnitTimeMilliseconds writePCEDELUnitTimeMillisecondsQ0
#define writePCEDELUnitTimeMilliseconds2 writePCEDELUnitTimeMillisecondsQ1
#define writePCEDELUnitLocationMilliseconds writePCEDELUnitLocationMillisecondsQ0
#define writePCEDELUnitLocationMilliseconds2 writePCEDELUnitLocationMillisecondsQ1

#define  readPCEDELNumb 0xc200+STD_BETA_PCE,((PCE_PHASE_DELAY_BASE+2) << 8)+0x00
#define writePCEDELNumbXX(XX) 0xca00+STD_BETA_PCE,((PCE_PHASE_DELAY_BASE+2) << 8)+0x##XX

#define  readPCEDELUnused 0xc200+STD_BETA_PCE,((PCE_PHASE_DELAY_BASE+3) << 8)+0x00

#define PCE_PHASE_DELAY_CHAN_BASE PCE_PHASE_DELAY_BASE+PCE_PHASE_DELAY_LENGTH
#define PCE_PHASE_DELAY_CHAN_LENGTH PAF_MAXNUMCHAN_AF*2

#if 0 < PAF_MAXNUMCHAN_AF
#define  readPCEDELDelayLeft 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE
#define writePCEDELDelayLeftN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE,NN
#define  readPCEDELDelayRght 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+2
#define writePCEDELDelayRghtN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+2,NN

#endif /* 0 < PAF_MAXNUMCHAN_AF */
#if PAF_MAXNUMCHAN_AF == 4
#define  readPCEDELDelayCntr 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+4
#define writePCEDELDelayCntrN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+4,NN
#define  readPCEDELDelaySurr 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+6
#define writePCEDELDelaySurrN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+6,NN

#define  readPCEDELDelayMaster 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+8
#define writePCEDELDelayMasterN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+8,NN

#elif PAF_MAXNUMCHAN_AF == 6
#define  readPCEDELDelayCntr 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+4
#define writePCEDELDelayCntrN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+4,NN
#define  readPCEDELDelaySurr 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+6
#define writePCEDELDelaySurrN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+6,NN
#define  readPCEDELDelayLSur 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+6
#define writePCEDELDelayLSurN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+6,NN
#define  readPCEDELDelayRSur 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+8
#define writePCEDELDelayRSurN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+8,NN
#define  readPCEDELDelaySubw 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+10
#define writePCEDELDelaySubwN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+10,NN

#define  readPCEDELDelayMaster 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+12
#define writePCEDELDelayMasterN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+12,NN

#elif PAF_MAXNUMCHAN_AF == 8
#define  readPCEDELDelayCntr 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+4
#define writePCEDELDelayCntrN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+4,NN
#define  readPCEDELDelaySurr 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+6
#define writePCEDELDelaySurrN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+6,NN
#define  readPCEDELDelayLSur 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+6
#define writePCEDELDelayLSurN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+6,NN
#define  readPCEDELDelayRSur 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+8
#define writePCEDELDelayRSurN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+8,NN
#define  readPCEDELDelaySubw 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+10
#define writePCEDELDelaySubwN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+10,NN
#define  readPCEDELDelayBack 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+12
#define writePCEDELDelayBackN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+12,NN
#define  readPCEDELDelayLBak 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+12
#define writePCEDELDelayLBakN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+12,NN
#define  readPCEDELDelayRBak 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+14
#define writePCEDELDelayRBakN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+14,NN

#define  readPCEDELDelayMaster 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+16
#define writePCEDELDelayMasterN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+16,NN

#elif PAF_MAXNUMCHAN_AF == 16

#define  readPCEDELDelayCntr 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+4
#define writePCEDELDelayCntrN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+4,NN
#define  readPCEDELDelayLCtr 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+4
#define writePCEDELDelayLCtrN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+4,NN
#define  readPCEDELDelayRCtr 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+6
#define writePCEDELDelayRCtrN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+6,NN
#define  readPCEDELDelayWide 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+8
#define writePCEDELDelayWideN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+8,NN
#define  readPCEDELDelayLWid 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+8
#define writePCEDELDelayLWidN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+8,NN
#define  readPCEDELDelayRWid 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+10
#define writePCEDELDelayRWidN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+10,NN
#define  readPCEDELDelayOver 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+12
#define writePCEDELDelayOverN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+12,NN
#define  readPCEDELDelayLOvr 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+12
#define writePCEDELDelayLOvrN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+12,NN
#define  readPCEDELDelayROvr 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+14
#define writePCEDELDelayROvrN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+14,NN
#define  readPCEDELDelaySurr 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+16
#define writePCEDELDelaySurrN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+16,NN
#define  readPCEDELDelayLSur 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+16
#define writePCEDELDelayLSurN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+16,NN
#define  readPCEDELDelayRSur 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+18
#define writePCEDELDelayRSurN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+18,NN
#define  readPCEDELDelayBack 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+20
#define writePCEDELDelayBackN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+20,NN
#define  readPCEDELDelayLBak 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+20
#define writePCEDELDelayLBakN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+20,NN
#define  readPCEDELDelayRBak 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+22
#define writePCEDELDelayRBakN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+22,NN
#define  readPCEDELDelaySubw 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+24
#define writePCEDELDelaySubwN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+24,NN
#define  readPCEDELDelayLSub 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+24
#define writePCEDELDelayLSubN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+24,NN
#define  readPCEDELDelayRSub 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+26
#define writePCEDELDelayRSubN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+26,NN
#define  readPCEDELDelayLHed 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+28
#define writePCEDELDelayLHedN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+28,NN
#define  readPCEDELDelayRHed 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+30
#define writePCEDELDelayRHedN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+30,NN

#define  readPCEDELDelayMaster 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+32
#define writePCEDELDelayMasterN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+32,NN

//ATMOS
#define  readPCEDELDelayLtrr 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+12
#define writePCEDELDelayLtrrN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+12,NN
#define  readPCEDELDelayRtrr 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+14
#define writePCEDELDelayRtrrN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+14,NN
#define  readPCEDELDelayLtrh 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+12
#define writePCEDELDelayLtrhN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+12,NN
#define  readPCEDELDelayRtrh 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+14
#define writePCEDELDelayRtrhN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+14,NN
#define  readPCEDELDelayLtmd 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+28
#define writePCEDELDelayLtmdN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+28,NN
#define  readPCEDELDelayRtmd 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+30
#define writePCEDELDelayRtmd(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+30,NN
#define  readPCEDELDelayLtft 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+28
#define writePCEDELDelayLtftN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+28,NN
#define  readPCEDELDelayRtft 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+30
#define writePCEDELDelayRtftN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+30,NN
#define  readPCEDELDelayLtfh 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+28
#define writePCEDELDelayLtfhN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+28,NN
#define  readPCEDELDelayRtfh 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+30
#define writePCEDELDelayRtfh(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+30,NN


#elif PAF_MAXNUMCHAN_AF == 32
   
    
    #define  readPCEDELDelayCntr 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+4
    #define writePCEDELDelayCntrN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+4,NN
    #define  readPCEDELDelayLCtr 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+4
    #define writePCEDELDelayLCtrN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+4,NN
    #define  readPCEDELDelayRCtr 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+6
    #define writePCEDELDelayRCtrN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+6,NN
    #define  readPCEDELDelayWide 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+8
    #define writePCEDELDelayWideN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+8,NN
    #define  readPCEDELDelayLWid 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+8
    #define writePCEDELDelayLWidN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+8,NN
    #define  readPCEDELDelayRWid 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+10
    #define writePCEDELDelayRWidN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+10,NN
    #define  readPCEDELDelayOver 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+12
    #define writePCEDELDelayOverN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+12,NN
    #define  readPCEDELDelayLOvr 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+12
    #define writePCEDELDelayLOvrN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+12,NN
    #define  readPCEDELDelayROvr 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+14
    #define writePCEDELDelayROvrN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+14,NN
    #define  readPCEDELDelayLtmd 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+12
    #define writePCEDELDelayLtmdN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+12,NN
    #define  readPCEDELDelayRtmd 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+14
    #define writePCEDELDelayRtmdN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+14,NN
    #define  readPCEDELDelaySurr 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+16
    #define writePCEDELDelaySurrN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+16,NN
    #define  readPCEDELDelayLSur 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+16
    #define writePCEDELDelayLSurN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+16,NN
    #define  readPCEDELDelayRSur 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+18
    #define writePCEDELDelayRSurN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+18,NN
    #define  readPCEDELDelayBack 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+20
    #define writePCEDELDelayBackN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+20,NN
    #define  readPCEDELDelayLBak 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+20
    #define writePCEDELDelayLBakN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+20,NN
    #define  readPCEDELDelayRBak 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+22
    #define writePCEDELDelayRBakN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+22,NN
    #define  readPCEDELDelaySubw 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+24
    #define writePCEDELDelaySubwN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+24,NN
    #define  readPCEDELDelayLSub 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+24
    #define writePCEDELDelayLSubN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+24,NN
    #define  readPCEDELDelayRSub 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+26
    #define writePCEDELDelayRSubN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+26,NN
    #define  readPCEDELDelayLHed 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+28
    #define writePCEDELDelayLHedN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+28,NN
    #define  readPCEDELDelayRHed 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+30
    #define writePCEDELDelayRHedN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+30,NN
    //
    #define  readPCEDELDelayCHed         0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+32
    #define  writePCEDELDelayCHedN(NN)   0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+32,NN
    #define  readPCEDELDelayTOvr         0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+34
    #define  writePCEDELDelayTOvrN(NN)   0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+34,NN
    #define  readPCEDELDelayLHSide       0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+36
    #define  writePCEDELDelayLHSideN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+36,NN
    #define  readPCEDELDelayRHSide       0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+38
    #define  writePCEDELDelayRHSideN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+38,NN
    #define  readPCEDELDelayLHRear       0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+40
    #define  writePCEDELDelayLHRearN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+40,NN
    #define  readPCEDELDelayRHRear       0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+42
    #define  writePCEDELDelayRHRearN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+42,NN
    #define  readPCEDELDelayCHRear       0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+44
    #define  writePCEDELDelayCHRearN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+44,NN
    //..
    // Three channels -unassigned 
    //..
    #define  readPCEDELDelayLCtrInr      0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+52
    #define  writePCEDELDelayLCtrInrN(NN)0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+52,NN
    #define  readPCEDELDelayRCtrInr      0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+54
    #define  writePCEDELDelayRCtrInrN(NN)0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+54,NN
    //..
    // Four channels -unassigned ie till PCE_PHASE_DELAY_CHAN_BASE+60
    //..
    
    #define  readPCEDELDelayMaster 0xc300+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+PAF_MAXNUMCHAN_AF*2
    #define writePCEDELDelayMasterN(NN) 0xcb00+STD_BETA_PCE,PCE_PHASE_DELAY_CHAN_BASE+PAF_MAXNUMCHAN_AF*2,NN


/* PAF_MAXNUMCHAN_AF == 32 */

#else /* PAF_MAXNUMCHAN */
    #error unsupported option
#endif /* PAF_MAXNUMCHAN */



    
    //
    //---
//PCE OUTPUT PHASE Alpha codes
#define PCE_PHASE_OUTPUT_BASE        PCE_PHASE_DELAY_CHAN_BASE+PCE_PHASE_DELAY_CHAN_LENGTH+2
//+2 is for DelayMaster 
#define PCE_PHASE_OUTPUT_BASE_LENGTH 3 //4 - byte elements added
//Eg:-
//#define readPCEDELDelayMaster 0xc32e,0x0036
//#define readPCEExceptionDetectMode 0xc22e,0x3800

#define readPCEExceptionDetectMode      0xc200+STD_BETA_PCE,((PCE_PHASE_OUTPUT_BASE)<<8)+0x00 
#define writePCEExceptionDetectDisable  0xca00+STD_BETA_PCE,((PCE_PHASE_OUTPUT_BASE)<<8)+0x00 
#define writePCEExceptionDetectEnable   0xca00+STD_BETA_PCE,((PCE_PHASE_OUTPUT_BASE)<<8)+0x01

#define readPCEExceptionDetectFlag      0xc200+STD_BETA_PCE,((PCE_PHASE_OUTPUT_BASE+1)<<8)+0x00 
#define writePCEExceptionDetectFlagOff  0xca00+STD_BETA_PCE,((PCE_PHASE_OUTPUT_BASE+1)<<8)+0x00 
#define writePCEExceptionDetectFlagOn   0xca00+STD_BETA_PCE,((PCE_PHASE_OUTPUT_BASE+1)<<8)+0x01

#define readPCEExceptionDetectMute      0xc200+STD_BETA_PCE,((PCE_PHASE_OUTPUT_BASE+2)<<8)+0x00 
#define writePCEExceptionDetectUnmute   0xca00+STD_BETA_PCE,((PCE_PHASE_OUTPUT_BASE+2)<<8)+0x00 
#define writePCEExceptionDetectMute     0xca00+STD_BETA_PCE,((PCE_PHASE_OUTPUT_BASE+2)<<8)+0x01

#define readPCEClipDetectFlag           0xc200+STD_BETA_PCE,((PCE_PHASE_OUTPUT_BASE+3)<<8)+0x00 
#define writePCEClipDetectFlagOff       0xca00+STD_BETA_PCE,((PCE_PHASE_OUTPUT_BASE+3)<<8)+0x00 
#define writePCEClipDetectFlagOn        0xca00+STD_BETA_PCE,((PCE_PHASE_OUTPUT_BASE+3)<<8)+0x01

#define readPCEBsMetadataType           0xc200+STD_BETA_PCE,((PCE_PHASE_OUTPUT_BASE+4)<<8)+0x00 

#define readPCEMdInsert                 0xc200+STD_BETA_PCE,((PCE_PHASE_OUTPUT_BASE+5)<<8)+0x00 
#define writePCEMdInsertDisable         0xca00+STD_BETA_PCE,((PCE_PHASE_OUTPUT_BASE+5)<<8)+0x00 
#define writePCEMdInsertEnable          0xca00+STD_BETA_PCE,((PCE_PHASE_OUTPUT_BASE+5)<<8)+0x01 

#define readPCEMaxNumChMd               0xc200+STD_BETA_PCE,((PCE_PHASE_OUTPUT_BASE+6)<<8)+0x00
#define writePCEMaxNumChMd(N)           0xca00+STD_BETA_PCE,((PCE_PHASE_OUTPUT_BASE+6)<<8)+((N)&0xff)


#endif /* PCE_VERSION */

#define  readPCEStatus 0xc508,STD_BETA_PCE

#if PCE_VERSION == 1
#define  readPCEControl \
         readPCEMode, \
         readPCEModePhase0Mode, \
         readPCEModePhase0Type, \
         readPCEModePhase1Mode, \
         readPCEModePhase1Type, \
         readPCEModePhase2Mode, \
         readPCEModePhase2Type, \
         readPCEModePhase3Mode, \
         readPCEModePhase3Type
#elif PCE_VERSION == 2
#define  readPCEControl \
         readPCEMode, \
         readPCEModePhase0Mode, \
         readPCEModePhase0Type, \
         readPCEModePhase1Mode, \
         readPCEModePhase1Type, \
         readPCEModePhase2Mode, \
         readPCEModePhase2Type, \
         readPCEModePhase3Mode, \
         readPCEModePhase3Type, \
         readPCEModePhase4Mode, \
         readPCEModePhase4Type, \
         readPCEModePhase5Mode, \
         readPCEModePhase5Type, \
         readPCEDELUnit, \
         readPCEDELNumb, \
         readPCEDELDelayLeft, \
         readPCEDELDelayRght, \
         readPCEDELDelayCntr, \
         readPCEDELDelayLCtr, \
         readPCEDELDelayRCtr, \
         readPCEDELDelayWide, \
         readPCEDELDelayLWid, \
         readPCEDELDelayRWid, \
         readPCEDELDelayOver, \
         readPCEDELDelayLOvr, \
         readPCEDELDelayROvr, \
         readPCEDELDelaySurr, \
         readPCEDELDelayLSur, \
         readPCEDELDelayRSur, \
         readPCEDELDelayBack, \
         readPCEDELDelayLBak, \
         readPCEDELDelayRBak, \
         readPCEDELDelaySubw, \
         readPCEDELDelayLSub, \
         readPCEDELDelayRSub, \
         readPCEDELDelayLHed, \
         readPCEDELDelayRHed
//         readPCEDELDelayMaster

/* in support of inverse compilation only */
#define writePCEDELNumbXX__10__ writePCEDELNumbXX(10)
#define wrotePCEDELDelayLeft 0x0800+readPCEDELDelayLeft
#define wrotePCEDELDelayRght 0x0800+readPCEDELDelayRght
#define wrotePCEDELDelayCtr  0x0800+readPCEDELDelayCntr
#define wrotePCEDELDelayLCtr 0x0800+readPCEDELDelayLCtr
#define wrotePCEDELDelayRCtr 0x0800+readPCEDELDelayRCtr
#define wrotePCEDELDelayWide 0x0800+readPCEDELDelayWide
#define wrotePCEDELDelayLWid 0x0800+readPCEDELDelayLWid
#define wrotePCEDELDelayRWid 0x0800+readPCEDELDelayRWid
#define wrotePCEDELDelayOver 0x0800+readPCEDELDelayOver
#define wrotePCEDELDelayLOvr 0x0800+readPCEDELDelayLOvr
#define wrotePCEDELDelayROvr 0x0800+readPCEDELDelayROvr
#define wrotePCEDELDelaySurr 0x0800+readPCEDELDelaySurr
#define wrotePCEDELDelayLSur 0x0800+readPCEDELDelayLSur
#define wrotePCEDELDelayRSur 0x0800+readPCEDELDelayRSur
#define wrotePCEDELDelayBack 0x0800+readPCEDELDelayBack
#define wrotePCEDELDelayLBak 0x0800+readPCEDELDelayLBak
#define wrotePCEDELDelayRBak 0x0800+readPCEDELDelayRBak
#define wrotePCEDELDelaySubw 0x0800+readPCEDELDelaySubw
#define wrotePCEDELDelayLSub 0x0800+readPCEDELDelayLSub
#define wrotePCEDELDelayRSub 0x0800+readPCEDELDelayRSub
#define wrotePCEDELDelayLHed 0x0800+readPCEDELDelayLHed
#define wrotePCEDELDelayRHed 0x0800+readPCEDELDelayRHed

#endif /* PCE_VERSION */

#endif /* _PCE_A */

