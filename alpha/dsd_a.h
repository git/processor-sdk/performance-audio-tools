/******************************************************************************
* Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

#ifndef _DSD_A
#define _DSD_A

#include <acpbeta.h>

#define  readDSDChannelConfigurationProgram                     0xc200+STD_BETA_DSD,0x0400
#define writeDSDChannelConfigurationProgramStereoUnknown        0xca00+STD_BETA_DSD,0x0400
#define writeDSDChannelConfigurationProgramSurround2Unknown_0   0xca00+STD_BETA_DSD,0x0401
#define writeDSDChannelConfigurationProgramSurround2Unknown_1   0xca00+STD_BETA_DSD,0x0402

#define  readDSDOperationalMode                 0xc200+STD_BETA_DSD,0x0500
#define writeDSDOperationalModeDecoding1X       0xca00+STD_BETA_DSD,0x0500
#define writeDSDOperationalModeDecoding2X       0xca00+STD_BETA_DSD,0x0501
#define writeDSDOperationalModeDecoding4X       0xca00+STD_BETA_DSD,0x0502
#define writeDSDOperationalModePASS             0xca00+STD_BETA_DSD,0x0503

#define writeDSDOperationalModeDecoding2822To44KHz  writeDSDOperationalModeDecoding1X
#define writeDSDOperationalModeDecoding2822To88KHz  writeDSDOperationalModeDecoding2X
#define writeDSDOperationalModeDecoding2822To176KHz writeDSDOperationalModeDecoding4X
#define writeDSDOperationalModeDecoding5644To44KHz  0xca00+STD_BETA_DSD,0x0504
#define writeDSDOperationalModeDecoding5644To88KHz  0xca00+STD_BETA_DSD,0x0505
#define writeDSDOperationalModeDecoding5644To176KHz 0xca00+STD_BETA_DSD,0x0506
#define writeDSDOperationalModeDecoding11288To44KHz 0xca00+STD_BETA_DSD,0x0507
#define writeDSDOperationalModeDecoding11288To88KHz 0xca00+STD_BETA_DSD,0x0508
#define writeDSDOperationalModeDecoding11288To176KHz 0xca00+STD_BETA_DSD,0x0509


#define  readDSDModulationPercent               0xc200+STD_BETA_DSD,0x0600
#define writeDSDModulationPercent50             0xca00+STD_BETA_DSD,0x0600
#define writeDSDModulationPercent100            0xca00+STD_BETA_DSD,0x0601

#define readDSDSilenceStatus                    0xc200+STD_BETA_DSD,0x0700

#define  readDSDMute                            0xc200+STD_BETA_DSD,0x0800
#define writeDSDMuteOff                         0xca00+STD_BETA_DSD,0x0800
#define writeDSDMuteOn                          0xca00+STD_BETA_DSD,0x0801

#define  readDSDDelayBypass                     0xc200+STD_BETA_DSD,0x0900
#define writeDSDDelayBypassEnable               0xca00+STD_BETA_DSD,0x0900
#define writeDSDDelayBypassDisable              0xca00+STD_BETA_DSD,0x0901

#define  readDSDDelayUse                         readDSDDelayBypass
#define writeDSDDelayUseDisable                 writeDSDDelayBypassEnable
#define writeDSDDelayUseEnable                  writeDSDDelayBypassDisable

#define  readDSDDelayLeft                       0xc300+STD_BETA_DSD,0x000a
#define writeDSDDelayLeftN(NN)                  0xcb00+STD_BETA_DSD,0x000a,NN
#define  readDSDDelayRght                       0xc300+STD_BETA_DSD,0x000c
#define writeDSDDelayRghtN(NN)                  0xcb00+STD_BETA_DSD,0x000c,NN
#define  readDSDDelayLsur                       0xc300+STD_BETA_DSD,0x000e
#define writeDSDDelayLsurN(NN)                  0xcb00+STD_BETA_DSD,0x000e,NN
#define  readDSDDelayRsur                       0xc300+STD_BETA_DSD,0x0010
#define writeDSDDelayRsurN(NN)                  0xcb00+STD_BETA_DSD,0x0010,NN
#define  readDSDDelayCntr                       0xc300+STD_BETA_DSD,0x0012
#define writeDSDDelayCntrN(NN)                  0xcb00+STD_BETA_DSD,0x0012,NN
#define  readDSDDelaySubw                       0xc300+STD_BETA_DSD,0x0014
#define writeDSDDelaySubwN(NN)                  0xcb00+STD_BETA_DSD,0x0014,NN

#define  readDSDDelayUnit                       0xc200+STD_BETA_DSD,0x1600
#define writeDSDDelayUnitTimeSamples            0xca00+STD_BETA_DSD,0x1600
#define writeDSDDelayUnitTimeMilliseconds       0xca00+STD_BETA_DSD,0x1601

#define readDSDStatus                           0xc508,STD_BETA_DSD
#define readDSDControl                          0xc508,STD_BETA_DSD

#endif /* _DSD_A */
