
/*
* Copyright (C) 2004-2017 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Alpha Code Processor Standard Series Beta declarations
//
//
//

#ifndef STDBETA_
#define STDBETA_

#ifndef VERSION_STD_BETA
#define VERSION_STD_BETA 2
#endif  /* VERSION_STD_BETA */

// Non-relocatable beta units

#define STD_BETA_ACP      	0x00 		/* for error reporting purposes only? */
#define STD_BETA_BETATABLE  0x01
#define STD_BETA_PHITABLE   0x02
#define STD_BETA_SIGMATABLE 0x03
#define STD_BETA_IDENTITY   0x04

#define STD_BETA_UART       0x08 /* unused */
#define STD_BETA_FTP        0x09 /* unused */
#define STD_BETA_LED        0x10 /* unused */
#define STD_BETA_PANEL    	0x11 /* unused */

// Relocatable beta units

#define STD_BETA_PRIME_BASE 0x20

#define STD_BETA_SYSIDL     0x20
#define STD_BETA_SYSINT     0x21
#define STD_BETA_IB         0x22
#define STD_BETA_OB         0x23
#define STD_BETA_DECODE     0x24
#define STD_BETA_ENCODE    	0x25
#define STD_BETA_VOLUME     0x26
#define STD_BETA_ASSERT     0x27 /* NIC - Notify Information Change */

#define STD_BETA_ACE        0x28 /* unused */ /* AAC Encoder */
#define STD_BETA_ACE2       0x29 /* unused */
#define STD_BETA_MPE        0x2a /* unused */ /* MPEG Encoder */
#define STD_BETA_MPE2       0x2b /* unused */
#define STD_BETA_PCF        0x2c /* unused */ /* Alternate PCM Encoder */
#define STD_BETA_PCF2       0x2d /* unused */
#define STD_BETA_PCE        0x2e              /* Standard PCM Encoder */
#define STD_BETA_PCE2       0x2f /* unused */

#define STD_BETA_SNG        0x30              /* Signal/Noise Generator */
#define STD_BETA_SNG2       0x31 /* unused */
#define STD_BETA_NG         0x32 /* unused */ /* Noise Generator */
#define STD_BETA_NG2        0x33 /* unused */
#define STD_BETA_PCM        0x34              /* Standard PCM Decoder */
#define STD_BETA_PCM2       0x35 /* unused */
#define STD_BETA_PCN        0x36 /* unused */ /* Alternate PCM Decoder */
#define STD_BETA_PCN2       0x37 /* unused */
#define STD_BETA_AC3        0x38 /* unused */ /* AC3 Decoder */
#define STD_BETA_AC32       0x39 /* unused */
#define STD_BETA_DTS        0x3a /* unused */ /* DTS Decoder */
#define STD_BETA_DTS2       0x3b /* unused */
#define STD_BETA_AAC        0x3c              /* AAC Decoder */
#define STD_BETA_AAC2       0x3d
#define STD_BETA_MPG        0x3e /* unused */ /* MPEG Decoder */
#define STD_BETA_MPG2       0x3f /* unused */

#define STD_BETA_BM         0x40
#define STD_BETA_PL         0x41 /* unused */ /* Pro Logic */
#define STD_BETA_DEL        0x42              /* Speaker Delay */
#define STD_BETA_NEO        0x43              /* Neo:6 */
#define STD_BETA_ML         0x44              /* MIPS Load */
#define STD_BETA_PL2        0x45 /* reused */ /* Pro Logic II */
#define STD_BETA_PL2x       0x45              /* Pro Logic IIx */
#define STD_BETA_ASS        0x46              /* Audio Stream Split */
#define STD_BETA_SRC        0x47              /* Synchronous Sample-Rate Converter */

/* THX ASP beta codes */
#define STD_BETA_BGC        0x48              /* Boundary Gain Compensation */
#define STD_BETA_BC         0x49              /* Bass Level Peak Manager */
#define STD_BETA_AD         0x4a              /* Adaptive Decorrelation */
#define STD_BETA_REQ        0x4b              /* Re-Equalization */
#define STD_BETA_TM         0x4c              /* Timbre Matching */
#define STD_BETA_ASA        0x4d              /* Advanced Speaker Array */
/* THX ASP beta codes */

#define STD_BETA_DMX        0x4e              /* Downmix */
#define STD_BETA_SUC        0x4f              /* Sample Rate Up-convert */
#define STD_BETA_DEM        0x50              /* Deemphasis */
#define STD_BETA_DH         0x51              /* Dolby Headphone */
#define STD_BETA_DH2        0x51              /* Dolby Headphone 2.0 */
#define STD_BETA_DVS        0x52              /* Dolby Virtual Speaker */
#define STD_BETA_DVS2       0x52              /* Dolby Virtual Speaker 2.0 */
#define STD_BETA_ASJ        0x53              /* Audio Stream Join */
#define STD_BETA_RVB        0x54              /* TI SFX - Room Simulator */
#define STD_BETA_GEQ        0x55              /* TI SFX - Graphic Equalizer */
#define STD_BETA_MTX        0x56              /* TI SFX - Matrix */

#define STD_BETA_HDD        0x57              /* HDCD Detect */
#define STD_BETA_HDF        0x58              /* HDCD Filter */
#define STD_BETA_LOU        0x59              /* TI SFX - Loudness */
#define STD_BETA_DM         0x5a              /* DM - Downmix module using CDM */
#define STD_BETA_ARC        0x5b              /* ARC - Asynchronous sample Rate Converter */
#define STD_BETA_SYSERR     0x5c
#define STD_BETA_VSH        0x5d              /* TI Headphone Virtualizer */
#define STD_BETA_VSS        0x5e              /* TI Speaker Virtualizer */
#define STD_BETA_X5F        0x5f /* unused */

#if VERSION_STD_BETA == 1

#error STD_BETA version 1 is obsolete

#elif VERSION_STD_BETA == 2

#define STD_BETA_WMA        0x60              /* WMA Decoder */
#define STD_BETA_WMA2       0x61
#define STD_BETA_WME        0x62              /* WMA Encoder */
#define STD_BETA_WME2       0x63
#define STD_BETA_MP3        0x64              /* MP3 Decoder */
#define STD_BETA_MP32       0x65
#define STD_BETA_MQ3        0x66              /* MP3 Encoder */
#define STD_BETA_MQ32       0x67
#define STD_BETA_DSD        0x68              /* DSD Decoder */
#define STD_BETA_DSD2       0x69
#define STD_BETA_DTSSS      0x6a              /* DTS Surround Sensation */
#define STD_BETA_DVL258     0x6b              /* Dolby Volume 258 ASP */
#define STD_BETA_DVL        0x6b              /* Dolby Volume ASP */
#define STD_BETA_DVLPP      0x6b              /* Dobly Volume 258 + PP ASP */
#define STD_BETA_MLP        0x6c              /* MLP Decoder */
#define STD_BETA_MLP2       0x6d
#define STD_BETA_IPOD       0x6e              /* Used for iPOD*/
#define STD_BETA_AQ         0x6f              /* Used for DAQ */


#define STD_BETA_DDM        0x70              /* DDM ASP */
#define STD_BETA_BASS       0x71              /* TI Bass Boost */
#define STD_BETA_BWE        0x72              /* TI Bandwidth Extension */
#define STD_BETA_DRC        0x73              /* TI Dynamic Range Compression */
#define STD_BETA_DDE        0x74              /* DD creator */
#define STD_BETA_DTSTR      0x75              /* DTS transcoder */
#define STD_BETA_SD         0x76              /* Surround Delay */
#define STD_BETA_CPY        0x77              /* 2->5.1 channel copy */
#define STD_BETA_DTSHD      0x78              /* DTSHD Decoder */
#define STD_BETA_DTSHD2     0x79
#define STD_BETA_DDP        0x7a              /* Dolby Digital Plus Decoder */
#define STD_BETA_DDP2       0x7b
#define STD_BETA_THD        0x7c              /* Dolby TrueHD Decoder */
#define STD_BETA_THD2       0x7d
#define STD_BETA_DXP        0x7e              /* DTS Express (LBR) Decoder */
#define STD_BETA_DXP2       0x7f
#define STD_BETA_UAE        0x80              /* uSDK Audio Engine Registers */
#define STD_BETA_UPLM       0x81              /* uSDK PLM Registers */
#define STD_BETA_UFMP       0x82              /* uSDK VFMP Registers */
#define STD_BETA_NEOX       0x83              /* DTS NEO:X ASP */
#define STD_BETA_PRC        0x84              /* USDK Priority Conversion ASP */
#define STD_BETA_AMIX       0x85              /* Ztop mixer */
//#define STD_BETA_SINEPROBE  0x86              /* simple sine injector for testing */
//#define STD_BETA_MDFORMAT   0x87              /* MetaData Formatter */
//#define STD_BETA_MDDEFORMAT 0x88              /* MetaData DeFormatter */
#define STD_BETA_CAR        0x89              /* CAR */
#define STD_BETA_OAR        0x8A              /* OAR */
#define STD_BETA_BMDA       0x8B              /* Dolby BM */
#define STD_BETA_DAP        0x8C              /* Dolby DAP */
#define STD_BETA_DECOPCB    0x8D              /* Decoder Output Circular Buffer */
#define STD_BETA_DTSUHDA    0x8E              /* Decoder portion of DTS UHD */
#define STD_BETA_DTSUHDB    0x8F              /* ASP portion of DTS UHD */
#define STD_BETA_NEXT       0x96

#else /* VERSION_STD_BETA */

#error Unsupported value of STD_BETA version

#endif  /* VERSION_STD_BETA */

#define STD_BETA_PRIME_OFFSET (STD_BETA_NEXT-STD_BETA_PRIME_BASE)

#endif  /* STDBETA_ */
