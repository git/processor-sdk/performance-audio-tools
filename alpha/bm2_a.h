/******************************************************************************
 * Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Texas Instruments Incorporated nor the
 *         names of its contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 *   THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

#ifndef _BM2_A
#define _BM2_A

#include <acpbeta.h>

#define  readBMMode             0xc200+STD_BETA_BM,0x0400
#define writeBMModeDisable      0xca00+STD_BETA_BM,0x0400
#define writeBMModeLPF2ndOrder  0xca00+STD_BETA_BM,0x0401
#define writeBMModeLPF4thOrder  0xca00+STD_BETA_BM,0x0402
#define writeBMModeLPFAuto      0xca00+STD_BETA_BM,0x0403
/* Old BMMode command:*/
#define writeBMModeEnable       writeBMModeLPF4thOrder

#define  readBMOCAuto               0xc200+STD_BETA_BM,0x0500
#define writeBMOCAutoDisable        0xca00+STD_BETA_BM,0x0500
#define writeBMOCAutoProLogic       0xca00+STD_BETA_BM,0x0501
#define writeBMOCAutoCorrelation    0xca00+STD_BETA_BM,0x0502
#define writeBMOCAutoN(NN)          0xca00+STD_BETA_BM,0x0500+((NN)&0xff)

#define  readBMInactionReason              0xc200+STD_BETA_BM,0x0600
#define wroteBMInactionReasonNone          0xca00+STD_BETA_BM,0x0600
#define wroteBMInactionReasonModeDisabled  0xca00+STD_BETA_BM,0x0601
#define wroteBMInactionReasonBypassEnabled 0xca00+STD_BETA_BM,0x0602
#define wroteBMInactionReasonActiveFailed  0xca00+STD_BETA_BM,0x0603
#define wroteBMInactionReasonSetupFailed   0xca00+STD_BETA_BM,0x0604
#define wroteBMInactionReasonStreamUnknown 0xca00+STD_BETA_BM,0x0605
#define wroteBMInactionReasonStreamNone    0xca00+STD_BETA_BM,0x0606
#define wroteBMInactionReasonNoSubw        0xca00+STD_BETA_BM,0x0607
#define wroteBMInactionReasonSubwCount     0xca00+STD_BETA_BM,0x0608
#define wroteBMInactionReasonOCNONone      0xca00+STD_BETA_BM,0x0609
#define wroteBMInactionReasonBLKSIZE       0xca00+STD_BETA_BM,0x060a
#define wroteBMInactionReasonResetFailed   0xca00+STD_BETA_BM,0x060b

#define  readBMOCSelect 0xc400+STD_BETA_BM,0x0008

#define  readBMOCSelectChannelsLowFreq 0xc200+STD_BETA_BM,0x0800
#define writeBMOCSelectChannelsLowFreqMain 0xca00+STD_BETA_BM,0x0802
#define writeBMOCSelectChannelsLowFreqCntr 0xca00+STD_BETA_BM,0x0804
#define writeBMOCSelectChannelsLowFreqSurr 0xca00+STD_BETA_BM,0x0808
#define writeBMOCSelectChannelsLowFreqBack 0xca00+STD_BETA_BM,0x0810
#define writeBMOCSelectChannelsLowFreqWide 0xca00+STD_BETA_BM,0x0820
#define writeBMOCSelectChannelsLowFreqHigh 0xca00+STD_BETA_BM,0x0840
#define writeBMOCSelectChannelsLowFreqHead writeBMOCSelectChannelsLowFreqHigh
#define writeBMOCSelectChannelsLowFreqN(NN) \
 0xca00+STD_BETA_BM,0x0800+((NN)&0xff)

#define  readBMOCSelectAncillary 0xc200+STD_BETA_BM,0x0900
#define writeBMOCSelectAncillaryDgtlLBNoneStd 0xca00+STD_BETA_BM,0x0981
#define writeBMOCSelectAncillaryDgtlLBNoneAlt 0xca00+STD_BETA_BM,0x0983
#define writeBMOCSelectAncillaryDgtlLBFrntStd 0xca00+STD_BETA_BM,0x0995
#define writeBMOCSelectAncillaryDgtlLBFrntAlt 0xca00+STD_BETA_BM,0x0997
#define writeBMOCSelectAncillaryDgtlLBRearStd 0xca00+STD_BETA_BM,0x0999
#define writeBMOCSelectAncillaryDgtlLBRearAlt 0xca00+STD_BETA_BM,0x099b
#define writeBMOCSelectAncillaryDgtlLBBothStd 0xca00+STD_BETA_BM,0x099d
#define writeBMOCSelectAncillaryDgtlLBBothAlt 0xca00+STD_BETA_BM,0x099f
#define writeBMOCSelectAncillaryAnlgLBNoneSubwStd 0xca00+STD_BETA_BM,0x0900
#define writeBMOCSelectAncillaryAnlgLBNoneSubwFil 0xca00+STD_BETA_BM,0x0901
#define writeBMOCSelectAncillaryAnlgLBFrntSubwStd 0xca00+STD_BETA_BM,0x0914
#define writeBMOCSelectAncillaryAnlgLBFrntSubwFil 0xca00+STD_BETA_BM,0x0915
#define writeBMOCSelectAncillaryAnlgLBRearSubwStd 0xca00+STD_BETA_BM,0x0918
#define writeBMOCSelectAncillaryAnlgLBRearSubwFil 0xca00+STD_BETA_BM,0x0919
#define writeBMOCSelectAncillaryAnlgLBBothSubwStd 0xca00+STD_BETA_BM,0x091c
#define writeBMOCSelectAncillaryAnlgLBBothSubwFil 0xca00+STD_BETA_BM,0x091d
/* Following line is obselete, but in use:*/
#define writeBMOCSelectAncillaryN(NN) 0xca00+STD_BETA_BM,0x0900+((NN)&0xff)

#define  readBMOCSelectAuto 0xc200+STD_BETA_BM,0x0a00
#define writeBMOCSelectAutoDisable 0xca00+STD_BETA_BM,0x0a00
#define writeBMOCSelectAutoProLogic 0xca00+STD_BETA_BM,0x0a01
#define writeBMOCSelectAutoCorrelation 0xca00+STD_BETA_BM,0x0a02
#define writeBMOCSelectAutoN(NN) 0xca00+STD_BETA_BM,0x0a00+((NN)&0xff)

#define  readBMOCSelectOCNumber 0xc200+STD_BETA_BM,0x0b00
#define writeBMOCSelectOCNumberNone 0xca00+STD_BETA_BM,0x0b40
#define writeBMOCSelectOCNumberDOC0 0xca00+STD_BETA_BM,0x0b00
#define writeBMOCSelectOCNumberDOC1 0xca00+STD_BETA_BM,0x0b01
#define writeBMOCSelectOCNumberDOC2 0xca00+STD_BETA_BM,0x0b02
#define writeBMOCSelectOCNumberDOC3 0xca00+STD_BETA_BM,0x0b03
#define writeBMOCSelectOCNumberDOCAuto 0xca00+STD_BETA_BM,0x0b0f

/* Combination OCNumber and Auto*/
#define  readBMOCSelectOCAuto 0xc300+STD_BETA_BM,0x000a
#define writeBMOCSelectOCAutoNone          0xcb00+STD_BETA_BM,0x000a,0x4000
#define writeBMOCSelectOCAutoDOC0CorrelOff 0xcb00+STD_BETA_BM,0x000a,0x0000
#define writeBMOCSelectOCAutoDOC0CorrelOn  0xcb00+STD_BETA_BM,0x000a,0x0002
#define writeBMOCSelectOCAutoDPC0CorrelOff 0xcb00+STD_BETA_BM,0x000a,0x0001
#define writeBMOCSelectOCAutoDPC0CorrelOn  0xcb00+STD_BETA_BM,0x000a,0x0003
#define writeBMOCSelectOCAutoDOC1CorrelOff 0xcb00+STD_BETA_BM,0x000a,0x0100
#define writeBMOCSelectOCAutoDOC1CorrelOn  0xcb00+STD_BETA_BM,0x000a,0x0102
#define writeBMOCSelectOCAutoDPC1CorrelOff 0xcb00+STD_BETA_BM,0x000a,0x0101
#define writeBMOCSelectOCAutoDPC1CorrelOn  0xcb00+STD_BETA_BM,0x000a,0x0103
#define writeBMOCSelectOCAutoDOC2CorrelOff 0xcb00+STD_BETA_BM,0x000a,0x0200
#define writeBMOCSelectOCAutoDOC2CorrelOn  0xcb00+STD_BETA_BM,0x000a,0x0202
#define writeBMOCSelectOCAutoDPC2CorrelOff 0xcb00+STD_BETA_BM,0x000a,0x0201
#define writeBMOCSelectOCAutoDPC2CorrelOn  0xcb00+STD_BETA_BM,0x000a,0x0203
#define writeBMOCSelectOCAutoDOC3CorrelOff 0xcb00+STD_BETA_BM,0x000a,0x0300
#define writeBMOCSelectOCAutoDOC3CorrelOn  0xcb00+STD_BETA_BM,0x000a,0x0302
#define writeBMOCSelectOCAutoDOCAutoCorrelOff 0xcb00+STD_BETA_BM,0x000a,0x0f00
#define writeBMOCSelectOCAutoDOCAutoCorrelOn  0xcb00+STD_BETA_BM,0x000a,0x0f02

#define  readBMOCStatus 0xc400+STD_BETA_BM,0x000c
#define  readBMOutputConfigurationStatus readBMOCStatus

#define  readBMOCStatusChannelsLowFreq 0xc200+STD_BETA_BM,0x0c00
#define wroteBMOCStatusChannelsLowFreqMain 0xca00+STD_BETA_BM,0x0c02
#define wroteBMOCStatusChannelsLowFreqCntr 0xca00+STD_BETA_BM,0x0c04
#define wroteBMOCStatusChannelsLowFreqSurr 0xca00+STD_BETA_BM,0x0c08
#define wroteBMOCStatusChannelsLowFreqBack 0xca00+STD_BETA_BM,0x0c10
#define wroteBMOCStatusChannelsLowFreqWide 0xca00+STD_BETA_BM,0x0c20
#define wroteBMOCStatusChannelsLowFreqHigh 0xca00+STD_BETA_BM,0x0c40

#define  readBMOCStatusAncillary 0xc200+STD_BETA_BM,0x0d00
#define wroteBMOCStatusAncillaryDgtlLBNoneStd 0xca00+STD_BETA_BM,0x0d81
#define wroteBMOCStatusAncillaryDgtlLBNoneAlt 0xca00+STD_BETA_BM,0x0d83
#define wroteBMOCStatusAncillaryDgtlLBFrntStd 0xca00+STD_BETA_BM,0x0d95
#define wroteBMOCStatusAncillaryDgtlLBFrntAlt 0xca00+STD_BETA_BM,0x0d97
#define wroteBMOCStatusAncillaryDgtlLBRearStd 0xca00+STD_BETA_BM,0x0d99
#define wroteBMOCStatusAncillaryDgtlLBRearAlt 0xca00+STD_BETA_BM,0x0d9b
#define wroteBMOCStatusAncillaryDgtlLBBothStd 0xca00+STD_BETA_BM,0x0d9d
#define wroteBMOCStatusAncillaryDgtlLBBothAlt 0xca00+STD_BETA_BM,0x0d9f
#define wroteBMOCStatusAncillaryAnlgLBNoneSubwStd 0xca00+STD_BETA_BM,0x0d00
#define wroteBMOCStatusAncillaryAnlgLBNoneSubwFil 0xca00+STD_BETA_BM,0x0d01
#define wroteBMOCStatusAncillaryAnlgLBFrntSubwStd 0xca00+STD_BETA_BM,0x0d14
#define wroteBMOCStatusAncillaryAnlgLBFrntSubwFil 0xca00+STD_BETA_BM,0x0d15
#define wroteBMOCStatusAncillaryAnlgLBRearSubwStd 0xca00+STD_BETA_BM,0x0d18
#define wroteBMOCStatusAncillaryAnlgLBRearSubwFil 0xca00+STD_BETA_BM,0x0d19
#define wroteBMOCStatusAncillaryAnlgLBBothSubwStd 0xca00+STD_BETA_BM,0x0d1c
#define wroteBMOCStatusAncillaryAnlgLBBothSubwFil 0xca00+STD_BETA_BM,0x0d1d

#define  readBMOCStatusAuto 0xc200+STD_BETA_BM,0x0e00
#define wroteBMOCStatusAutoDisable 0xca00+STD_BETA_BM,0x0e00
#define wroteBMOCStatusAutoProLogic 0xca00+STD_BETA_BM,0x0e01
#define wroteBMOCStatusAutoCorrelation 0xca00+STD_BETA_BM,0x0e02

#define  readBMOCStatusOCNumber 0xc200+STD_BETA_BM,0x0f00
#define wroteBMOCStatusOCNumberNone 0xca00+STD_BETA_BM,0x0f40
#define wroteBMOCStatusOCNumberDOC0 0xca00+STD_BETA_BM,0x0f00
#define wroteBMOCStatusOCNumberDOC1 0xca00+STD_BETA_BM,0x0f01
#define wroteBMOCStatusOCNumberDOC2 0xca00+STD_BETA_BM,0x0f02
#define wroteBMOCStatusOCNumberDOC3 0xca00+STD_BETA_BM,0x0f03
#define wroteBMOCStatusOCNumberDOCAuto 0xca00+STD_BETA_BM,0x0f0f

/* Combination OCNumber and Auto*/
#define  readBMOCStatusOCAuto 0xc300+STD_BETA_BM,0x000e
#define wroteBMOCStatusOCAutoNoneCorrelOffPLOff 0xcb00+STD_BETA_BM,0x000e,0x4000
#define wroteBMOCStatusOCAutoNoneCorrelOffPLOn  0xcb00+STD_BETA_BM,0x000e,0x4001
#define wroteBMOCStatusOCAutoNoneCorrelOnPLOff  0xcb00+STD_BETA_BM,0x000e,0x4002
#define wroteBMOCStatusOCAutoNoneCorrelOnPLOn   0xcb00+STD_BETA_BM,0x000e,0x4003
#define wroteBMOCStatusOCAutoDOC0CorrelOff 0xcb00+STD_BETA_BM,0x000e,0x0000
#define wroteBMOCStatusOCAutoDOC0CorrelOn  0xcb00+STD_BETA_BM,0x000e,0x0002
#define wroteBMOCStatusOCAutoDPC0CorrelOff 0xcb00+STD_BETA_BM,0x000e,0x0001
#define wroteBMOCStatusOCAutoDPC0CorrelOn  0xcb00+STD_BETA_BM,0x000e,0x0003
#define wroteBMOCStatusOCAutoDOC1CorrelOff 0xcb00+STD_BETA_BM,0x000e,0x0100
#define wroteBMOCStatusOCAutoDOC1CorrelOn  0xcb00+STD_BETA_BM,0x000e,0x0102
#define wroteBMOCStatusOCAutoDPC1CorrelOff 0xcb00+STD_BETA_BM,0x000e,0x0101
#define wroteBMOCStatusOCAutoDPC1CorrelOn  0xcb00+STD_BETA_BM,0x000e,0x0103
#define wroteBMOCStatusOCAutoDOC2CorrelOff 0xcb00+STD_BETA_BM,0x000e,0x0200
#define wroteBMOCStatusOCAutoDOC2CorrelOn  0xcb00+STD_BETA_BM,0x000e,0x0202
#define wroteBMOCStatusOCAutoDPC2CorrelOff 0xcb00+STD_BETA_BM,0x000e,0x0201
#define wroteBMOCStatusOCAutoDPC2CorrelOn  0xcb00+STD_BETA_BM,0x000e,0x0203
#define wroteBMOCStatusOCAutoDOC3CorrelOff 0xcb00+STD_BETA_BM,0x000e,0x0300
#define wroteBMOCStatusOCAutoDOC3CorrelOn  0xcb00+STD_BETA_BM,0x000e,0x0302
#define wroteBMOCStatusOCAutoDOCAutoCorrelOff 0xcb00+STD_BETA_BM,0x000e,0x0f00
#define wroteBMOCStatusOCAutoDOCAutoCorrelOn  0xcb00+STD_BETA_BM,0x000e,0x0f02

#define  readBMOverrideBassCollectionMask           0xc200+STD_BETA_BM,0x1000
#define writeBMOverrideBassCollectionMaskMain       0xca00+STD_BETA_BM,0x1002
#define writeBMOverrideBassCollectionMaskCntr       0xca00+STD_BETA_BM,0x1004
#define writeBMOverrideBassCollectionMaskMainCntr   0xca00+STD_BETA_BM,0x1006
#define writeBMOverrideBassCollectionMaskSurr       0xca00+STD_BETA_BM,0x1008
#define writeBMOverrideBassCollectionMaskBack       0xca00+STD_BETA_BM,0x1010
#define writeBMOverrideBassCollectionMaskN(n)       \
0xca00+STD_BETA_BM,(0x1000 | (0x00ff & n))
#define writeBMOverrideBassCollectionMaskNone       \
writeBMOverrideBassCollectionMaskN(0)

#define  readBMFilterRateStatus         0xc200+STD_BETA_BM,0x1100
#define wroteBMFilterRateStatus32000Hz  0xca00+STD_BETA_BM,0x1102
#define wroteBMFilterRateStatus44100Hz  0xca00+STD_BETA_BM,0x1103
#define wroteBMFilterRateStatus48000Hz  0xca00+STD_BETA_BM,0x1104
#define wroteBMFilterRateStatus88200Hz  0xca00+STD_BETA_BM,0x1105
#define wroteBMFilterRateStatus96000Hz  0xca00+STD_BETA_BM,0x1106
#define wroteBMFilterRateStatus192000Hz 0xca00+STD_BETA_BM,0x1107
#define wroteBMFilterRateStatus64000Hz  0xca00+STD_BETA_BM,0x1108
#define wroteBMFilterRateStatus128000Hz 0xca00+STD_BETA_BM,0x1109
#define wroteBMFilterRateStatus176400Hz 0xca00+STD_BETA_BM,0x110a

#define  readBMBypass           0xc200+STD_BETA_BM,0x1200
#define writeBMBypassDisable    0xca00+STD_BETA_BM,0x1200
#define writeBMBypassEnable     0xca00+STD_BETA_BM,0x1201

#define  readBMLFEVolume        0xc200+STD_BETA_BM,0x1300
#define writeBMLFEVolumeN(NN)   0xca00+STD_BETA_BM,0x1300+((NN)&0xff)
/* See associated satellite volume control commands below */

#define  readBMModeStatus             0xc200+STD_BETA_BM,0x1500
#define wroteBMModeStatusDisabled     0xca00+STD_BETA_BM,0x1500
#define wroteBMModeStatusLPF2ndOrder  0xca00+STD_BETA_BM,0x1501
#define wroteBMModeStatusLPF4thOrder  0xca00+STD_BETA_BM,0x1502

#define  readBMFcSatAll                             0xc600+STD_BETA_BM,0x1808
#define writeBMFcSatAllN(FcMain,FcCntr,FcSurr,FcBack) \
0xce00+STD_BETA_BM,0x1808, \
            FcMain,FcCntr,FcSurr,FcBack
/*
  writeBMFilterCutoffSelectN() is obsolete.
  It is defined only for use by legacy applications.
  Do not use in new applications.
*/
#define writeBMFilterCutoffSelectN(N) writeBMFcSatAllN((N),(N),(N),(N))

#define  readBMFcMain                               0xc300+STD_BETA_BM,0x0018
#define writeBMFcMainN(NN)                          0xcb00+STD_BETA_BM,0x0018,NN
#define  readBMFcCntr                               0xc300+STD_BETA_BM,0x001a
#define writeBMFcCntrN(NN)                          0xcb00+STD_BETA_BM,0x001a,NN
#define  readBMFcSurr                               0xc300+STD_BETA_BM,0x001c
#define writeBMFcSurrN(NN)                          0xcb00+STD_BETA_BM,0x001c,NN
#define  readBMFcBack                               0xc300+STD_BETA_BM,0x001e
#define writeBMFcBackN(NN)                          0xcb00+STD_BETA_BM,0x001e,NN
#define  readBMFcWide                               0xc300+STD_BETA_BM,0x0034
#define writeBMFcWideN(NN)                          0xcb00+STD_BETA_BM,0x0034,NN
#define  readBMFcHigh                               0xc300+STD_BETA_BM,0x0036
#define writeBMFcHighN(NN)                          0xcb00+STD_BETA_BM,0x0036,NN
/* Old BMFc commands:*/
#define  readBMFcAll readBMFcSatAll
#define writeBMFcAllN(FcMain,FcCntr,FcSurr,FcBack) \
writeBMFcSatAllN(FcMain,FcCntr,FcSurr,FcBack)

#define  readBMFcStatusSatAll                       0xc600+STD_BETA_BM,0x2008
#define  readBMFcStatusMain                         0xc300+STD_BETA_BM,0x0020
#define  readBMFcStatusCntr                         0xc300+STD_BETA_BM,0x0022
#define  readBMFcStatusSurr                         0xc300+STD_BETA_BM,0x0024
#define  readBMFcStatusBack                         0xc300+STD_BETA_BM,0x0026
#define  readBMFcStatusWide                         0xc300+STD_BETA_BM,0x0038
#define  readBMFcStatusHigh                         0xc300+STD_BETA_BM,0x003a
/* Old BMFcStatus commands:*/
#define  readBMFcStatusAll readBMFcStatusSatAll

#define  readBMFcSubAll                             0xc600+STD_BETA_BM,0x2804
#define writeBMFcSubAllN(FcLFE,FcSubw)              \
0xce00+STD_BETA_BM,0x2804, FcLFE,FcSubw
#define  readBMFcLFE                                0xc300+STD_BETA_BM,0x0028
#define writeBMFcLFEN(NN)                           0xcb00+STD_BETA_BM,0x0028,NN
#define  readBMFcSubw                               0xc300+STD_BETA_BM,0x002a
#define writeBMFcSubwN(NN)                          0xcb00+STD_BETA_BM,0x002a,NN

#define  readBMFcStatusSubAll                       0xc600+STD_BETA_BM,0x2c04
#define  readBMFcStatusLFE                          0xc300+STD_BETA_BM,0x002c
#define  readBMFcStatusSubw                         0xc300+STD_BETA_BM,0x002e

#define  readBMMainVolume                           0xc200+STD_BETA_BM,0x3000
#define writeBMMainVolumeN(NN)                      \
0xca00+STD_BETA_BM,0x3000+((NN)&0x0ff)
#define  readBMCntrVolume                           0xc200+STD_BETA_BM,0x3100
#define writeBMCntrVolumeN(NN)                      \
0xca00+STD_BETA_BM,0x3100+((NN)&0x0ff)
#define  readBMSurrVolume                           0xc200+STD_BETA_BM,0x3200
#define writeBMSurrVolumeN(NN)                      \
0xca00+STD_BETA_BM,0x3200+((NN)&0x0ff)
#define  readBMBackVolume                           0xc200+STD_BETA_BM,0x3300
#define writeBMBackVolumeN(NN)                      \
0xca00+STD_BETA_BM,0x3300+((NN)&0x0ff)
#define  readBMWideVolume                           0xc200+STD_BETA_BM,0x3c00
#define writeBMWideVolumeN(NN)                      \
0xca00+STD_BETA_BM,0x3c00+((NN)&0x0ff)
#define  readBMHighVolume                           0xc200+STD_BETA_BM,0x3d00
#define writeBMHighVolumeN(NN)                      \
0xca00+STD_BETA_BM,0x3d00+((NN)&0x0ff)

#define  readBMOptions                              \
0xc200+STD_BETA_BM,0x3f00                           
#define writeBMOptionsN(NN)                         \
0xca00+STD_BETA_BM,0x3f00+((NN)&0x0ff)

#define readBMOCSelectExt             0xc200+STD_BETA_BM,0x4000   
#define writeBMOCSelectExtSpeakerNone 0xca00+STD_BETA_BM,0x0400
#define writeBMOCSelectExtSpeakerLRTF 0xca00+STD_BETA_BM,0x0401
#define writeBMOCSelectExtSpeakerLTMD 0xca00+STD_BETA_BM,0x0402

#define  readBMStatus 0xc508,STD_BETA_BM
#define  readBMControl \
         readBMMode, \
         readBMBypass, \
         readBMOCAuto, \
         readBMOCSelect, \
         readBMFcSatAll, \
         readBMFcWide, \
         readBMFcHigh, \
         readBMFcSubAll, \
         readBMMainVolume, \
         readBMCntrVolume, \
         readBMSurrVolume, \
         readBMBackVolume, \
         readBMLFEVolume, \
         readBMWideVolume, \
         readBMHighVolume, \
         readBMOverrideBassCollectionMask, \
         readBMOptions,\
         readBMOCSelectExt
/*
 Digital-only implementation test setup for direct mode
 selection of Dolby "Speaker setup"

 Requires speaker setup in auto mode:

     writeSYSRecreationModeAuto
     writeSYSSpeakerCntr[None][Small1]
     writeSYSSpeakerSurr[None][Small2]
     writeSYSSpeakerBack[None][Small1][Small2]
     writeSYSSpeakerSubw[None][Bass1]

     Note: "None, 1 and 2" are significant above;
           "Small" is ignored for the following.

 Then switch to direct mode:

     writeSYSRecreationModeDirect

 Then send appropriate alpha code for Dolby reference
 "Speaker setup" configuration using convention:

     X X X X Y
      0 1 2 3 0

 where:

     X  refers to Main channels:   Small/Large (S/L)
      0

     X  refers to Cntr channel:    None/Small/Large (0/S/L)
      1

     X  refers to Surr channels:   None/Small/Large (0/S/L)
      2

     X  refers to Back channel(s): None/Small/Large (0/S/L)
      3

     Y  refers to Subw channel:    None/Bass1 (0/1)
      0

 Example sequence:

     Establish which channels may be active:

     writeSYSRecreationModeAuto
     writeSYSSpeakerCntrSmall1
     writeSYSSpeakerSurrSmall2
     writeSYSSpeakerBackNone
     writeSYSSpeakerSubwBass1
     writeSYSRecreationModeDirect

     Then select any of the following to apply:

     writeBMOCSelect_LLL01
     writeBMOCSelect_LLS01
     writeBMOCSelect_LSL01
     writeBMOCSelect_LSS01
     writeBMOCSelect_SLL01
     writeBMOCSelect_SLS01
     writeBMOCSelect_SSL01
     writeBMOCSelect_SSS01

 Note that these are "Digital-Only" selections.
 If Digital/Analog implementation is desired, or other
 ancillary feature, it is required to write the
 ancillary register exclusively afterward. Such as,
 select DOC2 "Alternate" configuration by:

     writeSYSRecreationModeAuto
     writeSYSSpeakerCntrSmall1
     writeSYSSpeakerSurrSmall2
     writeSYSSpeakerBackNone
     writeSYSSpeakerSubwNone
     writeSYSRecreationModeDirect
     writeBMOCSelect_LSS00
     writeBMOCSelectAncillaryDgtlLBNoneAlt
*/
#define  writeBMOCSelect_LLLS1 0xcc00+STD_BETA_BM,0x0008,0x810e,0x0302
#define  writeBMOCSelect_LLLS0 writeBMOCSelect_LLLS1
#define  writeBMOCSelect_LLSS1 0xcc00+STD_BETA_BM,0x0008,0x8106,0x0102
#define  writeBMOCSelect_LLSS0 0xcc00+STD_BETA_BM,0x0008,0x8106,0x0202
#define  writeBMOCSelect_LL0S1 0xcc00+STD_BETA_BM,0x0008,0x8106,0x0302
#define  writeBMOCSelect_LL0S0 writeBMOCSelect_LLSS0
#define  writeBMOCSelect_LSLS1 0xcc00+STD_BETA_BM,0x0008,0x810a,0x0302
#define  writeBMOCSelect_LSLS0 writeBMOCSelect_LSLS1
#define  writeBMOCSelect_LSSS1 0xcc00+STD_BETA_BM,0x0008,0x8102,0x0102
#define  writeBMOCSelect_LSSS0 0xcc00+STD_BETA_BM,0x0008,0x8102,0x0202
#define  writeBMOCSelect_LS0S1 writeBMOCSelect_LSSS1
#define  writeBMOCSelect_LS0S0 writeBMOCSelect_LSSS0
#define  writeBMOCSelect_L0LS1 writeBMOCSelect_LSLS1
#define  writeBMOCSelect_L0LS0 writeBMOCSelect_LSLS1
#define  writeBMOCSelect_L0SS1 writeBMOCSelect_LSSS1
#define  writeBMOCSelect_L0SS0 writeBMOCSelect_LSSS0
#define  writeBMOCSelect_L00S1 writeBMOCSelect_LSSS1
#define  writeBMOCSelect_L00S0 writeBMOCSelect_LSSS0

#define  writeBMOCSelect_SLLS1 0xcc00+STD_BETA_BM,0x0008,0x810c,0x0102
#define  writeBMOCSelect_SLLS0 0xcc00+STD_BETA_BM,0x0008,0x810c,0x0202
#define  writeBMOCSelect_SLSS1 0xcc00+STD_BETA_BM,0x0008,0x8104,0x0102
#define  writeBMOCSelect_SL0S1 writeBMOCSelect_SLSS1
#define  writeBMOCSelect_SSLS1 0xcc00+STD_BETA_BM,0x0008,0x8108,0x0102
#define  writeBMOCSelect_SSLS0 0xcc00+STD_BETA_BM,0x0008,0x8108,0x0202
#define  writeBMOCSelect_SSSS1 0xcc00+STD_BETA_BM,0x0008,0x8100,0x0102
#define  writeBMOCSelect_SSSS0 writeBMOCSelect_SSSS1
#define  writeBMOCSelect_SS0S1 writeBMOCSelect_SSSS1
#define  writeBMOCSelect_SS0S0 writeBMOCSelect_SSSS1
#define  writeBMOCSelect_S0LS1 writeBMOCSelect_SSLS1
#define  writeBMOCSelect_S0LS0 writeBMOCSelect_SSLS0
#define  writeBMOCSelect_S0SS1 writeBMOCSelect_SSSS1
#define  writeBMOCSelect_S0SS0 writeBMOCSelect_SSSS1
#define  writeBMOCSelect_S00S1 writeBMOCSelect_SSSS1
#define  writeBMOCSelect_S00S0 writeBMOCSelect_SSSS1

#define  writeBMOCSelect_LLLL1 0xcc00+STD_BETA_BM,0x0008,0x811e,0x0302
#define  writeBMOCSelect_LLLL0 writeBMOCSelect_LLLL1
#define  writeBMOCSelect_LLSL1 0xcc00+STD_BETA_BM,0x0008,0x8116,0x0102
#define  writeBMOCSelect_LLSL0 0xcc00+STD_BETA_BM,0x0008,0x8116,0x0202
#define  writeBMOCSelect_LL0L1 0xcc00+STD_BETA_BM,0x0008,0x8116,0x0302
#define  writeBMOCSelect_LL0L0 writeBMOCSelect_LLSL0
#define  writeBMOCSelect_LSLL1 0xcc00+STD_BETA_BM,0x0008,0x811a,0x0302
#define  writeBMOCSelect_LSLL0 writeBMOCSelect_LSLL1
#define  writeBMOCSelect_LSSL1 0xcc00+STD_BETA_BM,0x0008,0x8112,0x0102
#define  writeBMOCSelect_LSSL0 0xcc00+STD_BETA_BM,0x0008,0x8112,0x0202
#define  writeBMOCSelect_LS0L1 writeBMOCSelect_LSSL1
#define  writeBMOCSelect_LS0L0 writeBMOCSelect_LSSL0
#define  writeBMOCSelect_L0LL1 writeBMOCSelect_LSLL1
#define  writeBMOCSelect_L0LL0 writeBMOCSelect_LSLL1
#define  writeBMOCSelect_L0SL1 writeBMOCSelect_LSSL1
#define  writeBMOCSelect_L0SL0 writeBMOCSelect_LSSL0
#define  writeBMOCSelect_L00L1 writeBMOCSelect_LSSL1
#define  writeBMOCSelect_L00L0 writeBMOCSelect_LSSL0

#define  writeBMOCSelect_SLLL1 0xcc00+STD_BETA_BM,0x0008,0x811c,0x0102
#define  writeBMOCSelect_SLLL0 0xcc00+STD_BETA_BM,0x0008,0x811c,0x0202
#define  writeBMOCSelect_SLSL1 0xcc00+STD_BETA_BM,0x0008,0x8114,0x0102
#define  writeBMOCSelect_SL0L1 writeBMOCSelect_SLSL1
#define  writeBMOCSelect_SSLL1 0xcc00+STD_BETA_BM,0x0008,0x8118,0x0102
#define  writeBMOCSelect_SSLL0 0xcc00+STD_BETA_BM,0x0008,0x8118,0x0202
#define  writeBMOCSelect_SSSL1 0xcc00+STD_BETA_BM,0x0008,0x8110,0x0102
#define  writeBMOCSelect_SSSL0 writeBMOCSelect_SSSL1
#define  writeBMOCSelect_SS0L1 writeBMOCSelect_SSSL1
#define  writeBMOCSelect_SS0L0 writeBMOCSelect_SSSL1
#define  writeBMOCSelect_S0LL1 writeBMOCSelect_SSLL1
#define  writeBMOCSelect_S0LL0 writeBMOCSelect_SSLL0
#define  writeBMOCSelect_S0SL1 writeBMOCSelect_SSSL1
#define  writeBMOCSelect_S0SL0 writeBMOCSelect_SSSL1
#define  writeBMOCSelect_S00L1 writeBMOCSelect_SSSL1
#define  writeBMOCSelect_S00L0 writeBMOCSelect_SSSL1

#define  writeBMOCSelect_LLL01 writeBMOCSelect_LLLS1
#define  writeBMOCSelect_LLL00 writeBMOCSelect_LLLS0
#define  writeBMOCSelect_LLS01 writeBMOCSelect_LLSS1
#define  writeBMOCSelect_LLS00 writeBMOCSelect_LLSS0
#define  writeBMOCSelect_LL001 writeBMOCSelect_LLSS1
#define  writeBMOCSelect_LL000 writeBMOCSelect_LLSS0
#define  writeBMOCSelect_LSL01 writeBMOCSelect_LSLS1
#define  writeBMOCSelect_LSL00 writeBMOCSelect_LSLS0
#define  writeBMOCSelect_LSS01 writeBMOCSelect_LSSS1
#define  writeBMOCSelect_LSS00 writeBMOCSelect_LSSS0
#define  writeBMOCSelect_LS001 writeBMOCSelect_LSSS1
#define  writeBMOCSelect_LS000 writeBMOCSelect_LSSS0
#define  writeBMOCSelect_L0L01 writeBMOCSelect_LSLS1
#define  writeBMOCSelect_L0L00 writeBMOCSelect_LSLS0
#define  writeBMOCSelect_L0S01 writeBMOCSelect_LSSS1
#define  writeBMOCSelect_L0S00 writeBMOCSelect_LSSS0
#define  writeBMOCSelect_L0001 writeBMOCSelect_LSSS1
#define  writeBMOCSelect_L0000 writeBMOCSelect_LSSS0

#define  writeBMOCSelect_SLL01 writeBMOCSelect_SLLS1
#define  writeBMOCSelect_SLL00 writeBMOCSelect_SLLS0
#define  writeBMOCSelect_SLS01 writeBMOCSelect_SLSS1
#define  writeBMOCSelect_SL001 writeBMOCSelect_SLSS1
#define  writeBMOCSelect_SSL01 writeBMOCSelect_SSLS1
#define  writeBMOCSelect_SSL00 writeBMOCSelect_SSLS0
#define  writeBMOCSelect_SSS01 writeBMOCSelect_SSSS1
#define  writeBMOCSelect_SSS00 writeBMOCSelect_SSSS0
#define  writeBMOCSelect_SS001 writeBMOCSelect_SSSS1
#define  writeBMOCSelect_SS000 writeBMOCSelect_SSSS0
#define  writeBMOCSelect_S0L01 writeBMOCSelect_SSLS1
#define  writeBMOCSelect_S0L00 writeBMOCSelect_SSLS0
#define  writeBMOCSelect_S0S01 writeBMOCSelect_SSSS1
#define  writeBMOCSelect_S0S00 writeBMOCSelect_SSSS0
#define  writeBMOCSelect_S0001 writeBMOCSelect_SSSS1
#define  writeBMOCSelect_S0000 writeBMOCSelect_SSSS0

#define  readBMCcrSat           0xc200+STD_BETA_BM,0x1600
#define  readBMCcrSub           0xc200+STD_BETA_BM,0x1700
#define  readBMCcrExt           0xc200+STD_BETA_BM,0x3e00

#endif /* _BM2_A */
