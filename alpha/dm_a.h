/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// Downmix alpha codes
//
//
//

#ifndef _DM_A
#define _DM_A

#include <acpbeta.h>

#define  readDMMode  \ 
0xc200+STD_BETA_DM,0x0400
#define writeDMModeDisable  \ 
0xca00+STD_BETA_DM,0x0400
#define writeDMModeEnable  \ 
0xca00+STD_BETA_DM,0x0401

#define  readDMLFEDownmixVolume  \ 
0xc200+STD_BETA_DM,0x0500
#define writeDMLFEDownmixVolumeN(NN)  \ 
0xca00+STD_BETA_DM,0x0500+(0xff&(NN))
/* in support of inverse compilation only */
#define writeDMLFEDownmixVolumeN__20__ writeDMLFEDownmixVolumeN(20)

#define  readDMLFEDownmixInclude  \ 
0xc200+STD_BETA_DM,0x0600
#define writeDMLFEDownmixIncludeNo  \ 
0xca00+STD_BETA_DM,0x0600
#define writeDMLFEDownmixIncludeYes  \ 
0xca00+STD_BETA_DM,0x0601

#define  readDMCenterMixLevel  \ 
0xc300+STD_BETA_DM,0x0008
#define writeDMCenterMixLevelN(NN)  \ 
0xcb00+STD_BETA_DM,0x0008,(0xFFFF&(NN))
#define wroteDMCenterMixLevel  \ 
0xcb00+STD_BETA_DM,0x0008

#define  readDMSurroundMixLevel  \ 
0xc300+STD_BETA_DM,0x000a
#define writeDMSurroundMixLevelN(NN)  \ 
0xcb00+STD_BETA_DM,0x000a,(0xFFFF&(NN))
#define wroteDMSurroundMixLevel  \ 
0xcb00+STD_BETA_DM,0x000a

#define  readDMChannelConfigurationRequest  \ 
0xc400+STD_BETA_DM,0x000c
#define writeDMChannelConfigurationRequestUnknown \
0xcc00+STD_BETA_DM,0x000c,0x0000,0x0000

#define writeDMChannelConfigurationRequestNone  \ 
0xcc00+STD_BETA_DM,0x000c,0x0001,0x0000
#define writeDMChannelConfigurationRequestMono  \ 
0xcc00+STD_BETA_DM,0x000c,0x0002,0x0000
#define writeDMChannelConfigurationRequestStereo  \ 
0xcc00+STD_BETA_DM,0x000c,0x0003,0x0000
#define writeDMChannelConfigurationRequestStereoLtRt  \ 
0xcc00+STD_BETA_DM,0x000c,0x0003,0x0002
#define writeDMChannelConfigurationRequestStereoMono  \ 
0xcc00+STD_BETA_DM,0x000c,0x0003,0x0003
#define writeDMChannelConfigurationRequest3Stereo  \ 
0xcc00+STD_BETA_DM,0x000c,0x0108,0x0000
#define writeDMChannelConfigurationRequestPhantom  \ 
0xcc00+STD_BETA_DM,0x000c,0x0105,0x0000
#define writeDMChannelConfigurationRequestSurround  \ 
0xcc00+STD_BETA_DM,0x000c,0x010a,0x0000

#define writeDMChannelConfigurationRequestNone_0  \ 
0xcc00+STD_BETA_DM,0x000c,0x0001,0x0000
#define writeDMChannelConfigurationRequestMono_0  \ 
0xcc00+STD_BETA_DM,0x000c,0x0002,0x0000
#define writeDMChannelConfigurationRequestPhantom0_0  \ 
0xcc00+STD_BETA_DM,0x000c,0x0003,0x0000
#define writeDMChannelConfigurationRequestPhantom0Stereo_0  \ 
0xcc00+STD_BETA_DM,0x000c,0x0003,0x0001
#define writeDMChannelConfigurationRequestPhantom0LtRt_0  \ 
0xcc00+STD_BETA_DM,0x000c,0x0003,0x0002
#define writeDMChannelConfigurationRequestPhantom0Mono_0  \ 
0xcc00+STD_BETA_DM,0x000c,0x0003,0x0003

#define wroteDMChannelConfigurationRequestPhantom0Dual_0  \ 
0xcc00+STD_BETA_DM,0x000c,0x0003,0x0004

#define writeDMChannelConfigurationRequestPhantom1_0  \ 
0xcc00+STD_BETA_DM,0x000c,0x0004,0x0000
#define writeDMChannelConfigurationRequestPhantom2_0  \ 
0xcc00+STD_BETA_DM,0x000c,0x0005,0x0000

#define wroteDMChannelConfigurationRequestPhantom2Stereo_0  \ 
0xcc00+STD_BETA_DM,0x000c,0x0005,0x0001

#define writeDMChannelConfigurationRequestPhantom2LtRt_0  \ 
0xcc00+STD_BETA_DM,0x000c,0x0005,0x0002

#define wroteDMChannelConfigurationRequestPhantom2Mono_0  \ 
0xcc00+STD_BETA_DM,0x000c,0x0005,0x0003

#define writeDMChannelConfigurationRequestPhantom3_0  \ 
0xcc00+STD_BETA_DM,0x000c,0x0006,0x0000
#define writeDMChannelConfigurationRequestPhantom4_0  \ 
0xcc00+STD_BETA_DM,0x000c,0x0007,0x0000
#define writeDMChannelConfigurationRequestSurround0_0  \ 
0xcc00+STD_BETA_DM,0x000c,0x0008,0x0000
#define writeDMChannelConfigurationRequestSurround1_0  \ 
0xcc00+STD_BETA_DM,0x000c,0x0009,0x0000
#define writeDMChannelConfigurationRequestSurround2_0  \ 
0xcc00+STD_BETA_DM,0x000c,0x000a,0x0000

#define wroteDMChannelConfigurationRequestSurround2Stereo_0  \ 
0xcc00+STD_BETA_DM,0x000c,0x000a,0x0001

#define writeDMChannelConfigurationRequestSurround2LtRt_0  \ 
0xcc00+STD_BETA_DM,0x000c,0x000a,0x0002

#define wroteDMChannelConfigurationRequestSurround2Mono_0  \ 
0xcc00+STD_BETA_DM,0x000c,0x000a,0x0003

#define writeDMChannelConfigurationRequestSurround3_0  \ 
0xcc00+STD_BETA_DM,0x000c,0x000b,0x0000
#define writeDMChannelConfigurationRequestSurround4_0  \ 
0xcc00+STD_BETA_DM,0x000c,0x000c,0x0000

#define writeDMChannelConfigurationRequestNone_1  \ 
0xcc00+STD_BETA_DM,0x000c,0x0101,0x0000
#define writeDMChannelConfigurationRequestMono_1  \ 
0xcc00+STD_BETA_DM,0x000c,0x0102,0x0000
#define writeDMChannelConfigurationRequestPhantom0_1  \ 
0xcc00+STD_BETA_DM,0x000c,0x0103,0x0000
#define writeDMChannelConfigurationRequestPhantom0Stereo_1  \ 
0xcc00+STD_BETA_DM,0x000c,0x0103,0x0001
#define writeDMChannelConfigurationRequestPhantom0LtRt_1  \ 
0xcc00+STD_BETA_DM,0x000c,0x0103,0x0002
#define writeDMChannelConfigurationRequestPhantom0Mono_1  \ 
0xcc00+STD_BETA_DM,0x000c,0x0103,0x0003

#define wroteDMChannelConfigurationRequestPhantom0Dual_1  \ 
0xcc00+STD_BETA_DM,0x000c,0x0103,0x0004

#define writeDMChannelConfigurationRequestPhantom1_1  \ 
0xcc00+STD_BETA_DM,0x000c,0x0104,0x0000
#define writeDMChannelConfigurationRequestPhantom2_1  \ 
0xcc00+STD_BETA_DM,0x000c,0x0105,0x0000

#define wroteDMChannelConfigurationRequestPhantom2Stereo_1  \ 
0xcc00+STD_BETA_DM,0x000c,0x0105,0x0001

#define writeDMChannelConfigurationRequestPhantom2LtRt_1  \ 
0xcc00+STD_BETA_DM,0x000c,0x0105,0x0002

#define wroteDMChannelConfigurationRequestPhantom2Mono_1  \ 
0xcc00+STD_BETA_DM,0x000c,0x0105,0x0003

#define writeDMChannelConfigurationRequestPhantom3_1  \ 
0xcc00+STD_BETA_DM,0x000c,0x0106,0x0000
#define writeDMChannelConfigurationRequestPhantom4_1  \ 
0xcc00+STD_BETA_DM,0x000c,0x0107,0x0000
#define writeDMChannelConfigurationRequestSurround0_1  \ 
0xcc00+STD_BETA_DM,0x000c,0x0108,0x0000
#define writeDMChannelConfigurationRequestSurround1_1  \ 
0xcc00+STD_BETA_DM,0x000c,0x0109,0x0000
#define writeDMChannelConfigurationRequestSurround2_1  \ 
0xcc00+STD_BETA_DM,0x000c,0x010a,0x0000

#define wroteDMChannelConfigurationRequestSurround2Stereo_1  \ 
0xcc00+STD_BETA_DM,0x000c,0x010a,0x0001

#define writeDMChannelConfigurationRequestSurround2LtRt_1  \ 
0xcc00+STD_BETA_DM,0x000c,0x010a,0x0002

#define wroteDMChannelConfigurationRequestSurround2Mono_1  \ 
0xcc00+STD_BETA_DM,0x000c,0x010a,0x0003

#define writeDMChannelConfigurationRequestSurround3_1  \ 
0xcc00+STD_BETA_DM,0x000c,0x010b,0x0000
#define writeDMChannelConfigurationRequestSurround4_1  \ 
0xcc00+STD_BETA_DM,0x000c,0x010c,0x0000

#define writeDMChannelConfigurationRequestHL(HH,LL)  \ 
0xcc00+STD_BETA_DM,0x000c,LL,HH

#define  readDMStatus  \ 
0xc508,STD_BETA_DM
#define  readDMControl \
         readDMMode, \
         readDMLFEDownmixVolume, \
         readDMLFEDownmixInclude, \
         readDMCenterMixLevel, \
         readDMSurroundMixLevel, \
         readDMChannelConfigurationRequest

#endif /* _DM_A */

