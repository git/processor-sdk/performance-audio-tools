
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Volume Declarations
//
//
//

#ifndef _PAFVOL_A
#define _PAFVOL_A

#include <paftyp_a.h>
#include <acpbeta.h>

#define  readVOLMode 0xc200+STD_BETA_VOLUME,0x0400
#define writeVOLModeDisable 0xca00+STD_BETA_VOLUME,0x0400
#define writeVOLModeEnable 0xca00+STD_BETA_VOLUME,0x0401

#define  readVOLChannelCount 0xc200+STD_BETA_VOLUME,0x0500
#define writeVOLChannelCountN(NN) 0xca00+STD_BETA_VOLUME,0x0500+((NN)&0xff)

#define  readVOLImplementation 0xc200+STD_BETA_VOLUME,0x0600
#define writeVOLImplementationInactive 0xca00+STD_BETA_VOLUME,0x0600
#define writeVOLImplementationInternal 0xca00+STD_BETA_VOLUME,0x060f
#define writeVOLImplementationInternalDirect 0xca00+STD_BETA_VOLUME,0x060e
#define writeVOLImplementationExternal 0xca00+STD_BETA_VOLUME,0x060d
#define writeVOLImplementationExternalDirect 0xca00+STD_BETA_VOLUME,0x060c
#define writeVOLImplementationN(NN) 0xca00+STD_BETA_VOLUME,0x0600+((NN)&0xff)
    // ... and more could be named, but N will do.

#define  readVOLRampTime 0xc300+STD_BETA_VOLUME,0x0008
#define writeVOLRampTimeN(NN) 0xcb00+STD_BETA_VOLUME,0x0008,NN
#define wroteVOLRampTime 0xcb00+STD_BETA_VOLUME,0x0008

#define  readVOLControlMaster 0xc300+STD_BETA_VOLUME,0x0010
#define  readVOLOffsetMaster 0xc300+STD_BETA_VOLUME,0x0012
#define  readVOLInternalStatusMaster 0xc300+STD_BETA_VOLUME,0x0014
#define  readVOLExternalStatusMaster 0xc300+STD_BETA_VOLUME,0x0016

#define writeVOLControlMasterN(NN) 0xcb00+STD_BETA_VOLUME,0x0010,NN
#define writeVOLOffsetMasterN(NN) 0xcb00+STD_BETA_VOLUME,0x0012,NN

#if PAF_MAXNUMCHAN_AF==8

#define  readVOLTrimLeft 0xc300+STD_BETA_VOLUME,0x0018
#define  readVOLOffsetLeft 0xc300+STD_BETA_VOLUME,0x001a
#define  readVOLInternalStatusLeft 0xc300+STD_BETA_VOLUME,0x001c
#define  readVOLExternalStatusLeft 0xc300+STD_BETA_VOLUME,0x001e
#define  readVOLTrimRght 0xc300+STD_BETA_VOLUME,0x0020
#define  readVOLOffsetRght 0xc300+STD_BETA_VOLUME,0x0022
#define  readVOLInternalStatusRght 0xc300+STD_BETA_VOLUME,0x0024
#define  readVOLExternalStatusRght 0xc300+STD_BETA_VOLUME,0x0026
#define  readVOLTrimCntr 0xc300+STD_BETA_VOLUME,0x0028
#define  readVOLOffsetCntr 0xc300+STD_BETA_VOLUME,0x002a
#define  readVOLInternalStatusCntr 0xc300+STD_BETA_VOLUME,0x002c
#define  readVOLExternalStatusCntr 0xc300+STD_BETA_VOLUME,0x002e
#define  readVOLTrimSurr 0xc300+STD_BETA_VOLUME,0x0030
#define  readVOLOffsetSurr 0xc300+STD_BETA_VOLUME,0x0032
#define  readVOLInternalStatusSurr 0xc300+STD_BETA_VOLUME,0x0034
#define  readVOLExternalStatusSurr 0xc300+STD_BETA_VOLUME,0x0036
#define  readVOLTrimLSur 0xc300+STD_BETA_VOLUME,0x0030
#define  readVOLOffsetLSur 0xc300+STD_BETA_VOLUME,0x0032
#define  readVOLInternalStatusLSur 0xc300+STD_BETA_VOLUME,0x0034
#define  readVOLExternalStatusLSur 0xc300+STD_BETA_VOLUME,0x0036
#define  readVOLTrimRSur 0xc300+STD_BETA_VOLUME,0x0038
#define  readVOLOffsetRSur 0xc300+STD_BETA_VOLUME,0x003a
#define  readVOLInternalStatusRSur 0xc300+STD_BETA_VOLUME,0x003c
#define  readVOLExternalStatusRSur 0xc300+STD_BETA_VOLUME,0x003e
#define  readVOLTrimSubw 0xc300+STD_BETA_VOLUME,0x0040
#define  readVOLOffsetSubw 0xc300+STD_BETA_VOLUME,0x0042
#define  readVOLInternalStatusSubw 0xc300+STD_BETA_VOLUME,0x0044
#define  readVOLExternalStatusSubw 0xc300+STD_BETA_VOLUME,0x0046
#define  readVOLTrimBack 0xc300+STD_BETA_VOLUME,0x0048
#define  readVOLOffsetBack 0xc300+STD_BETA_VOLUME,0x004a
#define  readVOLInternalStatusBack 0xc300+STD_BETA_VOLUME,0x004c
#define  readVOLExternalStatusBack 0xc300+STD_BETA_VOLUME,0x004e
#define  readVOLTrimLBak 0xc300+STD_BETA_VOLUME,0x0048
#define  readVOLOffsetLBak 0xc300+STD_BETA_VOLUME,0x004a
#define  readVOLInternalStatusLBak 0xc300+STD_BETA_VOLUME,0x004c
#define  readVOLExternalStatusLBak 0xc300+STD_BETA_VOLUME,0x004e
#define  readVOLTrimRBak 0xc300+STD_BETA_VOLUME,0x0050
#define  readVOLOffsetRBak 0xc300+STD_BETA_VOLUME,0x0052
#define  readVOLInternalStatusRBak 0xc300+STD_BETA_VOLUME,0x0054
#define  readVOLExternalStatusRBak 0xc300+STD_BETA_VOLUME,0x0056

#define writeVOLTrimLeftN(NN) 0xcb00+STD_BETA_VOLUME,0x0018,NN
#define writeVOLOffsetLeftN(NN) 0xcb00+STD_BETA_VOLUME,0x001a,NN
#define writeVOLTrimRghtN(NN) 0xcb00+STD_BETA_VOLUME,0x0020,NN
#define writeVOLOffsetRghtN(NN) 0xcb00+STD_BETA_VOLUME,0x0022,NN
#define writeVOLTrimCntrN(NN) 0xcb00+STD_BETA_VOLUME,0x0028,NN
#define writeVOLOffsetCntrN(NN) 0xcb00+STD_BETA_VOLUME,0x002a,NN
#define writeVOLTrimSurrN(NN) 0xcb00+STD_BETA_VOLUME,0x0030,NN
#define writeVOLOffsetSurrN(NN) 0xcb00+STD_BETA_VOLUME,0x0032,NN
#define writeVOLTrimLSurN(NN) 0xcb00+STD_BETA_VOLUME,0x0030,NN
#define writeVOLOffsetLSurN(NN) 0xcb00+STD_BETA_VOLUME,0x0032,NN
#define writeVOLTrimRSurN(NN) 0xcb00+STD_BETA_VOLUME,0x0038,NN
#define writeVOLOffsetRSurN(NN) 0xcb00+STD_BETA_VOLUME,0x003a,NN
#define writeVOLTrimSubwN(NN) 0xcb00+STD_BETA_VOLUME,0x0040,NN
#define writeVOLOffsetSubwN(NN) 0xcb00+STD_BETA_VOLUME,0x0042,NN
#define writeVOLTrimBackN(NN) 0xcb00+STD_BETA_VOLUME,0x0048,NN
#define writeVOLOffsetBackN(NN) 0xcb00+STD_BETA_VOLUME,0x004a,NN
#define writeVOLTrimLBakN(NN) 0xcb00+STD_BETA_VOLUME,0x0048,NN
#define writeVOLOffsetLBakN(NN) 0xcb00+STD_BETA_VOLUME,0x004a,NN
#define writeVOLTrimRBakN(NN) 0xcb00+STD_BETA_VOLUME,0x0050,NN
#define writeVOLOffsetRBakN(NN) 0xcb00+STD_BETA_VOLUME,0x0052,NN

#define wroteVOLTrimLeft 0x0800+readVOLTrimLeft
#define wroteVOLOffsetLeft 0x0800+readVOLOffsetLeft
#define wroteVOLInternalStatusLeft 0x0800+readVOLInternalStatusLeft
#define wroteVOLExternalStatusLeft 0x0800+readVOLExternalStatusLeft
#define wroteVOLTrimRght 0x0800+readVOLTrimRght
#define wroteVOLOffsetRght 0x0800+readVOLOffsetRght
#define wroteVOLInternalStatusRght 0x0800+readVOLInternalStatusRght
#define wroteVOLExternalStatusRght 0x0800+readVOLExternalStatusRght
#define wroteVOLTrimCntr 0x0800+readVOLTrimCntr
#define wroteVOLOffsetCntr 0x0800+readVOLOffsetCntr
#define wroteVOLInternalStatusCntr 0x0800+readVOLInternalStatusCntr
#define wroteVOLExternalStatusCntr 0x0800+readVOLExternalStatusCntr
#define wroteVOLTrimSurr 0x0800+readVOLTrimSurr
#define wroteVOLOffsetSurr 0x0800+readVOLOffsetSurr
#define wroteVOLInternalStatusSurr 0x0800+readVOLInternalStatusSurr
#define wroteVOLExternalStatusSurr 0x0800+readVOLExternalStatusSurr
#define wroteVOLTrimLSur 0x0800+readVOLTrimLSur
#define wroteVOLOffsetLSur 0x0800+readVOLOffsetLSur
#define wroteVOLInternalStatusLSur 0x0800+readVOLInternalStatusLSur
#define wroteVOLExternalStatusLSur 0x0800+readVOLExternalStatusLSur
#define wroteVOLTrimRSur 0x0800+readVOLTrimRSur
#define wroteVOLOffsetRSur 0x0800+readVOLOffsetRSur
#define wroteVOLInternalStatusRSur 0x0800+readVOLInternalStatusRSur
#define wroteVOLExternalStatusRSur 0x0800+readVOLExternalStatusRSur
#define wroteVOLTrimSubw 0x0800+readVOLTrimSubw
#define wroteVOLOffsetSubw 0x0800+readVOLOffsetSubw
#define wroteVOLInternalStatusSubw 0x0800+readVOLInternalStatusSubw
#define wroteVOLExternalStatusSubw 0x0800+readVOLExternalStatusSubw
#define wroteVOLTrimBack 0x0800+readVOLTrimBack
#define wroteVOLOffsetBack 0x0800+readVOLOffsetBack
#define wroteVOLInternalStatusBack 0x0800+readVOLInternalStatusBack
#define wroteVOLExternalStatusBack 0x0800+readVOLExternalStatusBack
#define wroteVOLTrimLBak 0x0800+readVOLTrimLBak
#define wroteVOLOffsetLBak 0x0800+readVOLOffsetLBak
#define wroteVOLInternalStatusLBak 0x0800+readVOLInternalStatusLBak
#define wroteVOLExternalStatusLBak 0x0800+readVOLExternalStatusLBak
#define wroteVOLTrimRBak 0x0800+readVOLTrimRBak
#define wroteVOLOffsetRBak 0x0800+readVOLOffsetRBak
#define wroteVOLInternalStatusRBak 0x0800+readVOLInternalStatusRBak
#define wroteVOLExternalStatusRBak 0x0800+readVOLExternalStatusRBak

#elif PAF_MAXNUMCHAN_AF==16

#define  readVOLControlMaster 0xc300+STD_BETA_VOLUME,0x0010
#define  readVOLOffsetMaster 0xc300+STD_BETA_VOLUME,0x0012
#define  readVOLInternalStatusMaster 0xc300+STD_BETA_VOLUME,0x0014
#define  readVOLExternalStatusMaster 0xc300+STD_BETA_VOLUME,0x0016
#define  readVOLTrimLeft 0xc300+STD_BETA_VOLUME,0x0018
#define  readVOLOffsetLeft 0xc300+STD_BETA_VOLUME,0x001a
#define  readVOLInternalStatusLeft 0xc300+STD_BETA_VOLUME,0x001c
#define  readVOLExternalStatusLeft 0xc300+STD_BETA_VOLUME,0x001e
#define  readVOLTrimRght 0xc300+STD_BETA_VOLUME,0x0020
#define  readVOLOffsetRght 0xc300+STD_BETA_VOLUME,0x0022
#define  readVOLInternalStatusRght 0xc300+STD_BETA_VOLUME,0x0024
#define  readVOLExternalStatusRght 0xc300+STD_BETA_VOLUME,0x0026
#define  readVOLTrimCntr 0xc300+STD_BETA_VOLUME,0x0028
#define  readVOLOffsetCntr 0xc300+STD_BETA_VOLUME,0x002a
#define  readVOLInternalStatusCntr 0xc300+STD_BETA_VOLUME,0x002c
#define  readVOLExternalStatusCntr 0xc300+STD_BETA_VOLUME,0x002e
#define  readVOLTrimLCtr 0xc300+STD_BETA_VOLUME,0x0028
#define  readVOLOffsetLCtr 0xc300+STD_BETA_VOLUME,0x002a
#define  readVOLInternalStatusLCtr 0xc300+STD_BETA_VOLUME,0x002c
#define  readVOLExternalStatusLCtr 0xc300+STD_BETA_VOLUME,0x002e
#define  readVOLTrimRCtr 0xc300+STD_BETA_VOLUME,0x0030
#define  readVOLOffsetRCtr 0xc300+STD_BETA_VOLUME,0x0032
#define  readVOLInternalStatusRCtr 0xc300+STD_BETA_VOLUME,0x0034
#define  readVOLExternalStatusRCtr 0xc300+STD_BETA_VOLUME,0x0036
#define  readVOLTrimWide 0xc300+STD_BETA_VOLUME,0x0038
#define  readVOLOffsetWide 0xc300+STD_BETA_VOLUME,0x003a
#define  readVOLInternalStatusWide 0xc300+STD_BETA_VOLUME,0x003c
#define  readVOLExternalStatusWide 0xc300+STD_BETA_VOLUME,0x003e
#define  readVOLTrimLWid 0xc300+STD_BETA_VOLUME,0x0038
#define  readVOLOffsetLWid 0xc300+STD_BETA_VOLUME,0x003a
#define  readVOLInternalStatusLWid 0xc300+STD_BETA_VOLUME,0x003c
#define  readVOLExternalStatusLWid 0xc300+STD_BETA_VOLUME,0x003e
#define  readVOLTrimRWid 0xc300+STD_BETA_VOLUME,0x0040
#define  readVOLOffsetRWid 0xc300+STD_BETA_VOLUME,0x0042
#define  readVOLInternalStatusRWid 0xc300+STD_BETA_VOLUME,0x0044
#define  readVOLExternalStatusRWid 0xc300+STD_BETA_VOLUME,0x0046
#define  readVOLTrimOver 0xc300+STD_BETA_VOLUME,0x0048
#define  readVOLOffsetOver 0xc300+STD_BETA_VOLUME,0x004a
#define  readVOLInternalStatusOver 0xc300+STD_BETA_VOLUME,0x004c
#define  readVOLExternalStatusOver 0xc300+STD_BETA_VOLUME,0x004e
#define  readVOLTrimLOvr 0xc300+STD_BETA_VOLUME,0x0048
#define  readVOLOffsetLOvr 0xc300+STD_BETA_VOLUME,0x004a
#define  readVOLInternalStatusLOvr 0xc300+STD_BETA_VOLUME,0x004c
#define  readVOLExternalStatusLOvr 0xc300+STD_BETA_VOLUME,0x004e
#define  readVOLTrimROvr 0xc300+STD_BETA_VOLUME,0x0050
#define  readVOLOffsetROvr 0xc300+STD_BETA_VOLUME,0x0052
#define  readVOLInternalStatusROvr 0xc300+STD_BETA_VOLUME,0x0054
#define  readVOLExternalStatusROvr 0xc300+STD_BETA_VOLUME,0x0056
#define  readVOLTrimSurr 0xc300+STD_BETA_VOLUME,0x0058
#define  readVOLOffsetSurr 0xc300+STD_BETA_VOLUME,0x005a
#define  readVOLInternalStatusSurr 0xc300+STD_BETA_VOLUME,0x005c
#define  readVOLExternalStatusSurr 0xc300+STD_BETA_VOLUME,0x005e
#define  readVOLTrimLSur 0xc300+STD_BETA_VOLUME,0x0058
#define  readVOLOffsetLSur 0xc300+STD_BETA_VOLUME,0x005a
#define  readVOLInternalStatusLSur 0xc300+STD_BETA_VOLUME,0x005c
#define  readVOLExternalStatusLSur 0xc300+STD_BETA_VOLUME,0x005e
#define  readVOLTrimRSur 0xc300+STD_BETA_VOLUME,0x0060
#define  readVOLOffsetRSur 0xc300+STD_BETA_VOLUME,0x0062
#define  readVOLInternalStatusRSur 0xc300+STD_BETA_VOLUME,0x0064
#define  readVOLExternalStatusRSur 0xc300+STD_BETA_VOLUME,0x0066
#define  readVOLTrimBack 0xc300+STD_BETA_VOLUME,0x0068
#define  readVOLOffsetBack 0xc300+STD_BETA_VOLUME,0x006a
#define  readVOLInternalStatusBack 0xc300+STD_BETA_VOLUME,0x006c
#define  readVOLExternalStatusBack 0xc300+STD_BETA_VOLUME,0x006e
#define  readVOLTrimLBak 0xc300+STD_BETA_VOLUME,0x0068
#define  readVOLOffsetLBak 0xc300+STD_BETA_VOLUME,0x006a
#define  readVOLInternalStatusLBak 0xc300+STD_BETA_VOLUME,0x006c
#define  readVOLExternalStatusLBak 0xc300+STD_BETA_VOLUME,0x006e
#define  readVOLTrimRBak 0xc300+STD_BETA_VOLUME,0x0070
#define  readVOLOffsetRBak 0xc300+STD_BETA_VOLUME,0x0072
#define  readVOLInternalStatusRBak 0xc300+STD_BETA_VOLUME,0x0074
#define  readVOLExternalStatusRBak 0xc300+STD_BETA_VOLUME,0x0076
#define  readVOLTrimSubw 0xc300+STD_BETA_VOLUME,0x0078
#define  readVOLOffsetSubw 0xc300+STD_BETA_VOLUME,0x007a
#define  readVOLInternalStatusSubw 0xc300+STD_BETA_VOLUME,0x007c
#define  readVOLExternalStatusSubw 0xc300+STD_BETA_VOLUME,0x007e
#define  readVOLTrimLSub 0xc300+STD_BETA_VOLUME,0x0078
#define  readVOLOffsetLSub 0xc300+STD_BETA_VOLUME,0x007a
#define  readVOLInternalStatusLSub 0xc300+STD_BETA_VOLUME,0x007c
#define  readVOLExternalStatusLSub 0xc300+STD_BETA_VOLUME,0x007e
#define  readVOLTrimRSub 0xc300+STD_BETA_VOLUME,0x0080
#define  readVOLOffsetRSub 0xc300+STD_BETA_VOLUME,0x0082
#define  readVOLInternalStatusRSub 0xc300+STD_BETA_VOLUME,0x0084
#define  readVOLExternalStatusRSub 0xc300+STD_BETA_VOLUME,0x0086
#define  readVOLTrimHead 0xc300+STD_BETA_VOLUME,0x0088
#define  readVOLOffsetHead 0xc300+STD_BETA_VOLUME,0x008a
#define  readVOLInternalStatusHead 0xc300+STD_BETA_VOLUME,0x008c
#define  readVOLExternalStatusHead 0xc300+STD_BETA_VOLUME,0x008e
#define  readVOLTrimLHed 0xc300+STD_BETA_VOLUME,0x0088
#define  readVOLOffsetLHed 0xc300+STD_BETA_VOLUME,0x008a
#define  readVOLInternalStatusLHed 0xc300+STD_BETA_VOLUME,0x008c
#define  readVOLExternalStatusLHed 0xc300+STD_BETA_VOLUME,0x008e
#define  readVOLTrimRHed 0xc300+STD_BETA_VOLUME,0x0090
#define  readVOLOffsetRHed 0xc300+STD_BETA_VOLUME,0x0092
#define  readVOLInternalStatusRHed 0xc300+STD_BETA_VOLUME,0x0094
#define  readVOLExternalStatusRHed 0xc300+STD_BETA_VOLUME,0x0096

#define writeVOLControlMasterN(NN) 0xcb00+STD_BETA_VOLUME,0x0010,NN
#define writeVOLOffsetMasterN(NN) 0xcb00+STD_BETA_VOLUME,0x0012,NN
#define writeVOLTrimLeftN(NN) 0xcb00+STD_BETA_VOLUME,0x0018,NN
#define writeVOLOffsetLeftN(NN) 0xcb00+STD_BETA_VOLUME,0x001a,NN
#define writeVOLTrimRghtN(NN) 0xcb00+STD_BETA_VOLUME,0x0020,NN
#define writeVOLOffsetRghtN(NN) 0xcb00+STD_BETA_VOLUME,0x0022,NN
#define writeVOLTrimCntrN(NN) 0xcb00+STD_BETA_VOLUME,0x0028,NN
#define writeVOLOffsetCntrN(NN) 0xcb00+STD_BETA_VOLUME,0x002a,NN
#define writeVOLTrimLCtrN(NN) 0xcb00+STD_BETA_VOLUME,0x0028,NN
#define writeVOLOffsetLCtrN(NN) 0xcb00+STD_BETA_VOLUME,0x002a,NN
#define writeVOLTrimRCtrN(NN) 0xcb00+STD_BETA_VOLUME,0x0030,NN
#define writeVOLOffsetRCtrN(NN) 0xcb00+STD_BETA_VOLUME,0x0032,NN
#define writeVOLTrimWideN(NN) 0xcb00+STD_BETA_VOLUME,0x0038,NN
#define writeVOLOffsetWideN(NN) 0xcb00+STD_BETA_VOLUME,0x003a,NN
#define writeVOLTrimLWidN(NN) 0xcb00+STD_BETA_VOLUME,0x0038,NN
#define writeVOLOffsetLWidN(NN) 0xcb00+STD_BETA_VOLUME,0x003a,NN
#define writeVOLTrimRWidN(NN) 0xcb00+STD_BETA_VOLUME,0x0040,NN
#define writeVOLOffsetRWidN(NN) 0xcb00+STD_BETA_VOLUME,0x0042,NN
#define writeVOLTrimOverN(NN) 0xcb00+STD_BETA_VOLUME,0x0048,NN
#define writeVOLOffsetOverN(NN) 0xcb00+STD_BETA_VOLUME,0x004a,NN
#define writeVOLTrimLOvrN(NN) 0xcb00+STD_BETA_VOLUME,0x0048,NN
#define writeVOLOffsetLOvrN(NN) 0xcb00+STD_BETA_VOLUME,0x004a,NN
#define writeVOLTrimROvrN(NN) 0xcb00+STD_BETA_VOLUME,0x0050,NN
#define writeVOLOffsetROvrN(NN) 0xcb00+STD_BETA_VOLUME,0x0052,NN
#define writeVOLTrimSurrN(NN) 0xcb00+STD_BETA_VOLUME,0x0058,NN
#define writeVOLOffsetSurrN(NN) 0xcb00+STD_BETA_VOLUME,0x005a,NN
#define writeVOLTrimLSurN(NN) 0xcb00+STD_BETA_VOLUME,0x0058,NN
#define writeVOLOffsetLSurN(NN) 0xcb00+STD_BETA_VOLUME,0x005a,NN
#define writeVOLTrimRSurN(NN) 0xcb00+STD_BETA_VOLUME,0x0060,NN
#define writeVOLOffsetRSurN(NN) 0xcb00+STD_BETA_VOLUME,0x0062,NN
#define writeVOLTrimBackN(NN) 0xcb00+STD_BETA_VOLUME,0x0068,NN
#define writeVOLOffsetBackN(NN) 0xcb00+STD_BETA_VOLUME,0x006a,NN
#define writeVOLTrimLBakN(NN) 0xcb00+STD_BETA_VOLUME,0x0068,NN
#define writeVOLOffsetLBakN(NN) 0xcb00+STD_BETA_VOLUME,0x006a,NN
#define writeVOLTrimRBakN(NN) 0xcb00+STD_BETA_VOLUME,0x0070,NN
#define writeVOLOffsetRBakN(NN) 0xcb00+STD_BETA_VOLUME,0x0072,NN
#define writeVOLTrimSubwN(NN) 0xcb00+STD_BETA_VOLUME,0x0078,NN
#define writeVOLOffsetSubwN(NN) 0xcb00+STD_BETA_VOLUME,0x007a,NN
#define writeVOLTrimLSubN(NN) 0xcb00+STD_BETA_VOLUME,0x0078,NN
#define writeVOLOffsetLSubN(NN) 0xcb00+STD_BETA_VOLUME,0x007a,NN
#define writeVOLTrimRSubN(NN) 0xcb00+STD_BETA_VOLUME,0x0080,NN
#define writeVOLOffsetRSubN(NN) 0xcb00+STD_BETA_VOLUME,0x0082,NN
#define writeVOLTrimHeadN(NN) 0xcb00+STD_BETA_VOLUME,0x0088,NN
#define writeVOLOffsetHeadN(NN) 0xcb00+STD_BETA_VOLUME,0x008a,NN
#define writeVOLTrimLHedN(NN) 0xcb00+STD_BETA_VOLUME,0x0088,NN
#define writeVOLOffsetLHedN(NN) 0xcb00+STD_BETA_VOLUME,0x008a,NN
#define writeVOLTrimRHedN(NN) 0xcb00+STD_BETA_VOLUME,0x0090,NN
#define writeVOLOffsetRHedN(NN) 0xcb00+STD_BETA_VOLUME,0x0092,NN

//ATMOS
#define  readVOLTrimLtrr 0xc300+STD_BETA_VOLUME,0x0048
#define  readVOLOffsetLtrr 0xc300+STD_BETA_VOLUME,0x004a
#define  readVOLInternalStatusLtrr 0xc300+STD_BETA_VOLUME,0x004c
#define  readVOLExternalStatusLtrr 0xc300+STD_BETA_VOLUME,0x004e
#define  readVOLTrimRtrr 0xc300+STD_BETA_VOLUME,0x0050
#define  readVOLOffsetRtrr 0xc300+STD_BETA_VOLUME,0x0052
#define  readVOLInternalStatusRtrr 0xc300+STD_BETA_VOLUME,0x0054
#define  readVOLExternalStatusRtrr 0xc300+STD_BETA_VOLUME,0x0056
#define  readVOLTrimLtrh 0xc300+STD_BETA_VOLUME,0x0048
#define  readVOLOffsetLtrh 0xc300+STD_BETA_VOLUME,0x004a
#define  readVOLInternalStatusLtrh 0xc300+STD_BETA_VOLUME,0x004c
#define  readVOLExternalStatusLtrh 0xc300+STD_BETA_VOLUME,0x004e
#define  readVOLTrimRtrh 0xc300+STD_BETA_VOLUME,0x0050
#define  readVOLOffsetRtrh 0xc300+STD_BETA_VOLUME,0x0052
#define  readVOLInternalStatusRtrh 0xc300+STD_BETA_VOLUME,0x0054
#define  readVOLExternalStatusRtrh 0xc300+STD_BETA_VOLUME,0x0056
#define  readVOLTrimLtmd 0xc300+STD_BETA_VOLUME,0x0088
#define  readVOLOffsetLtmd 0xc300+STD_BETA_VOLUME,0x008a
#define  readVOLInternalStatusLtmd 0xc300+STD_BETA_VOLUME,0x008c
#define  readVOLExternalStatusLtmd 0xc300+STD_BETA_VOLUME,0x008e
#define  readVOLTrimRtmd 0xc300+STD_BETA_VOLUME,0x0090
#define  readVOLOffsetRtmd 0xc300+STD_BETA_VOLUME,0x0092
#define  readVOLInternalStatusRtmd 0xc300+STD_BETA_VOLUME,0x0094
#define  readVOLExternalStatusRtmd 0xc300+STD_BETA_VOLUME,0x0096
#define  readVOLTrimLtft 0xc300+STD_BETA_VOLUME,0x0088
#define  readVOLOffsetLtft 0xc300+STD_BETA_VOLUME,0x008a
#define  readVOLInternalStatusLtft 0xc300+STD_BETA_VOLUME,0x008c
#define  readVOLExternalStatusLtft 0xc300+STD_BETA_VOLUME,0x008e
#define  readVOLTrimRtft 0xc300+STD_BETA_VOLUME,0x0090
#define  readVOLOffsetRtft 0xc300+STD_BETA_VOLUME,0x0092
#define  readVOLInternalStatusRtft 0xc300+STD_BETA_VOLUME,0x0094
#define  readVOLExternalStatusRtft 0xc300+STD_BETA_VOLUME,0x0096
#define  readVOLTrimLtfh 0xc300+STD_BETA_VOLUME,0x0088
#define  readVOLOffsetLtfh 0xc300+STD_BETA_VOLUME,0x008a
#define  readVOLInternalStatusLtfh 0xc300+STD_BETA_VOLUME,0x008c
#define  readVOLExternalStatusLtfh 0xc300+STD_BETA_VOLUME,0x008e
#define  readVOLTrimRtfh 0xc300+STD_BETA_VOLUME,0x0090
#define  readVOLOffsetRtfh 0xc300+STD_BETA_VOLUME,0x0092
#define  readVOLInternalStatusRtfh 0xc300+STD_BETA_VOLUME,0x0094
#define  readVOLExternalStatusRtfh 0xc300+STD_BETA_VOLUME,0x0096

#define wroteVOLControlMaster 0x0800+readVOLControlMaster
#define wroteVOLOffsetMaster 0x0800+readVOLOffsetMaster
#define wroteVOLInternalStatusMaster 0x0800+readVOLInternalStatusMaster
#define wroteVOLExternalStatusMaster 0x0800+readVOLExternalStatusMaster
#define wroteVOLTrimLeft 0x0800+readVOLTrimLeft
#define wroteVOLOffsetLeft 0x0800+readVOLOffsetLeft
#define wroteVOLInternalStatusLeft 0x0800+readVOLInternalStatusLeft
#define wroteVOLExternalStatusLeft 0x0800+readVOLExternalStatusLeft
#define wroteVOLTrimRght 0x0800+readVOLTrimRght
#define wroteVOLOffsetRght 0x0800+readVOLOffsetRght
#define wroteVOLInternalStatusRght 0x0800+readVOLInternalStatusRght
#define wroteVOLExternalStatusRght 0x0800+readVOLExternalStatusRght
#define wroteVOLTrimCntr 0x0800+readVOLTrimCntr
#define wroteVOLOffsetCntr 0x0800+readVOLOffsetCntr
#define wroteVOLInternalStatusCntr 0x0800+readVOLInternalStatusCntr
#define wroteVOLExternalStatusCntr 0x0800+readVOLExternalStatusCntr
#define wroteVOLTrimLCtr 0x0800+readVOLTrimLCtr
#define wroteVOLOffsetLCtr 0x0800+readVOLOffsetLCtr
#define wroteVOLInternalStatusLCtr 0x0800+readVOLInternalStatusLCtr
#define wroteVOLExternalStatusLCtr 0x0800+readVOLExternalStatusLCtr
#define wroteVOLTrimRCtr 0x0800+readVOLTrimRCtr
#define wroteVOLOffsetRCtr 0x0800+readVOLOffsetRCtr
#define wroteVOLInternalStatusRCtr 0x0800+readVOLInternalStatusRCtr
#define wroteVOLExternalStatusRCtr 0x0800+readVOLExternalStatusRCtr
#define wroteVOLTrimWide 0x0800+readVOLTrimWide
#define wroteVOLOffsetWide 0x0800+readVOLOffsetWide
#define wroteVOLInternalStatusWide 0x0800+readVOLInternalStatusWide
#define wroteVOLExternalStatusWide 0x0800+readVOLExternalStatusWide
#define wroteVOLTrimLWid 0x0800+readVOLTrimLWid
#define wroteVOLOffsetLWid 0x0800+readVOLOffsetLWid
#define wroteVOLInternalStatusLWid 0x0800+readVOLInternalStatusLWid
#define wroteVOLExternalStatusLWid 0x0800+readVOLExternalStatusLWid
#define wroteVOLTrimRWid 0x0800+readVOLTrimRWid
#define wroteVOLOffsetRWid 0x0800+readVOLOffsetRWid
#define wroteVOLInternalStatusRWid 0x0800+readVOLInternalStatusRWid
#define wroteVOLExternalStatusRWid 0x0800+readVOLExternalStatusRWid
#define wroteVOLTrimOver 0x0800+readVOLTrimOver
#define wroteVOLOffsetOver 0x0800+readVOLOffsetOver
#define wroteVOLInternalStatusOver 0x0800+readVOLInternalStatusOver
#define wroteVOLExternalStatusOver 0x0800+readVOLExternalStatusOver
#define wroteVOLTrimLOvr 0x0800+readVOLTrimLOvr
#define wroteVOLOffsetLOvr 0x0800+readVOLOffsetLOvr
#define wroteVOLInternalStatusLOvr 0x0800+readVOLInternalStatusLOvr
#define wroteVOLExternalStatusLOvr 0x0800+readVOLExternalStatusLOvr
#define wroteVOLTrimROvr 0x0800+readVOLTrimROvr
#define wroteVOLOffsetROvr 0x0800+readVOLOffsetROvr
#define wroteVOLInternalStatusROvr 0x0800+readVOLInternalStatusROvr
#define wroteVOLExternalStatusROvr 0x0800+readVOLExternalStatusROvr
#define wroteVOLTrimSurr 0x0800+readVOLTrimSurr
#define wroteVOLOffsetSurr 0x0800+readVOLOffsetSurr
#define wroteVOLInternalStatusSurr 0x0800+readVOLInternalStatusSurr
#define wroteVOLExternalStatusSurr 0x0800+readVOLExternalStatusSurr
#define wroteVOLTrimLSur 0x0800+readVOLTrimLSur
#define wroteVOLOffsetLSur 0x0800+readVOLOffsetLSur
#define wroteVOLInternalStatusLSur 0x0800+readVOLInternalStatusLSur
#define wroteVOLExternalStatusLSur 0x0800+readVOLExternalStatusLSur
#define wroteVOLTrimRSur 0x0800+readVOLTrimRSur
#define wroteVOLOffsetRSur 0x0800+readVOLOffsetRSur
#define wroteVOLInternalStatusRSur 0x0800+readVOLInternalStatusRSur
#define wroteVOLExternalStatusRSur 0x0800+readVOLExternalStatusRSur
#define wroteVOLTrimBack 0x0800+readVOLTrimBack
#define wroteVOLOffsetBack 0x0800+readVOLOffsetBack
#define wroteVOLInternalStatusBack 0x0800+readVOLInternalStatusBack
#define wroteVOLExternalStatusBack 0x0800+readVOLExternalStatusBack
#define wroteVOLTrimLBak 0x0800+readVOLTrimLBak
#define wroteVOLOffsetLBak 0x0800+readVOLOffsetLBak
#define wroteVOLInternalStatusLBak 0x0800+readVOLInternalStatusLBak
#define wroteVOLExternalStatusLBak 0x0800+readVOLExternalStatusLBak
#define wroteVOLTrimRBak 0x0800+readVOLTrimRBak
#define wroteVOLOffsetRBak 0x0800+readVOLOffsetRBak
#define wroteVOLInternalStatusRBak 0x0800+readVOLInternalStatusRBak
#define wroteVOLExternalStatusRBak 0x0800+readVOLExternalStatusRBak
#define wroteVOLTrimSubw 0x0800+readVOLTrimSubw
#define wroteVOLOffsetSubw 0x0800+readVOLOffsetSubw
#define wroteVOLInternalStatusSubw 0x0800+readVOLInternalStatusSubw
#define wroteVOLExternalStatusSubw 0x0800+readVOLExternalStatusSubw
#define wroteVOLTrimLSub 0x0800+readVOLTrimLSub
#define wroteVOLOffsetLSub 0x0800+readVOLOffsetLSub
#define wroteVOLInternalStatusLSub 0x0800+readVOLInternalStatusLSub
#define wroteVOLExternalStatusLSub 0x0800+readVOLExternalStatusLSub
#define wroteVOLTrimRSub 0x0800+readVOLTrimRSub
#define wroteVOLOffsetRSub 0x0800+readVOLOffsetRSub
#define wroteVOLInternalStatusRSub 0x0800+readVOLInternalStatusRSub
#define wroteVOLExternalStatusRSub 0x0800+readVOLExternalStatusRSub
#define wroteVOLTrimHead 0x0800+readVOLTrimHead
#define wroteVOLOffsetHead 0x0800+readVOLOffsetHead
#define wroteVOLInternalStatusHead 0x0800+readVOLInternalStatusHead
#define wroteVOLExternalStatusHead 0x0800+readVOLExternalStatusHead
#define wroteVOLTrimLHed 0x0800+readVOLTrimLHed
#define wroteVOLOffsetLHed 0x0800+readVOLOffsetLHed
#define wroteVOLInternalStatusLHed 0x0800+readVOLInternalStatusLHed
#define wroteVOLExternalStatusLHed 0x0800+readVOLExternalStatusLHed
#define wroteVOLTrimRHed 0x0800+readVOLTrimRHed
#define wroteVOLOffsetRHed 0x0800+readVOLOffsetRHed
#define wroteVOLInternalStatusRHed 0x0800+readVOLInternalStatusRHed
#define wroteVOLExternalStatusRHed 0x0800+readVOLExternalStatusRHed

//ATMOS

#define wroteVOLTrimLtrr 0x0800+readVOLTrimLtrr
#define wroteVOLOffsetLtrr 0x0800+readVOLOffsetLtrr
#define wroteVOLInternalStatusLtrr 0x0800+readVOLInternalStatusLtrr
#define wroteVOLExternalStatusLtrr 0x0800+readVOLExternalStatusLtrr
#define wroteVOLTrimRtrr 0x0800+readVOLTrimRtrr
#define wroteVOLOffsetRtrr 0x0800+readVOLOffsetRtrr
#define wroteVOLInternalStatusRtrr 0x0800+readVOLInternalStatusRtrr
#define wroteVOLExternalStatusRtrr 0x0800+readVOLExternalStatusRtrr
#define wroteVOLTrimLtrh 0x0800+readVOLTrimLtrh
#define wroteVOLOffsetLtrh 0x0800+readVOLOffsetLtrh
#define wroteVOLInternalStatusLtrh 0x0800+readVOLInternalStatusLtrh
#define wroteVOLExternalStatusLtrh 0x0800+readVOLExternalStatusLtrh
#define wroteVOLTrimRtrh 0x0800+readVOLTrimRtrh
#define wroteVOLOffsetRtrh 0x0800+readVOLOffsetRtrh
#define wroteVOLInternalStatusRtrh 0x0800+readVOLInternalStatusRtrh
#define wroteVOLExternalStatusRtrh 0x0800+readVOLExternalStatusRtrh
#define wroteVOLTrimLtmd 0x0800+readVOLTrimLtmd
#define wroteVOLOffsetLtmd 0x0800+readVOLOffsetLtmd
#define wroteVOLInternalStatusLtmd 0x0800+readVOLInternalStatusLtmd
#define wroteVOLExternalStatusLtmd 0x0800+readVOLExternalStatusLtmd
#define wroteVOLTrimRtmd 0x0800+readVOLTrimRtmd
#define wroteVOLOffsetRtmd 0x0800+readVOLOffsetRtmd
#define wroteVOLInternalStatusRtmd 0x0800+readVOLInternalStatusRtmd
#define wroteVOLExternalStatusRtmd 0x0800+readVOLExternalStatusRtmd
#define wroteVOLTrimLtft 0x0800+readVOLTrimLtft
#define wroteVOLOffsetLtft 0x0800+readVOLOffsetLtft
#define wroteVOLInternalStatusLtft 0x0800+readVOLInternalStatusLtft
#define wroteVOLExternalStatusLtft 0x0800+readVOLExternalStatusLtft
#define wroteVOLTrimRtft 0x0800+readVOLTrimRtft
#define wroteVOLOffsetRtft 0x0800+readVOLOffsetRtft
#define wroteVOLInternalStatusRtft 0x0800+readVOLInternalStatusRtft
#define wroteVOLExternalStatusRtft 0x0800+readVOLExternalStatusRtft
#define wroteVOLTrimLtfh 0x0800+readVOLTrimLtfh
#define wroteVOLOffsetLtfh 0x0800+readVOLOffsetLtfh
#define wroteVOLInternalStatusLtfh 0x0800+readVOLInternalStatusLtfh
#define wroteVOLExternalStatusLtfh 0x0800+readVOLExternalStatusLtfh
#define wroteVOLTrimRtfh 0x0800+readVOLTrimRtfh
#define wroteVOLOffsetRtfh 0x0800+readVOLOffsetRtfh
#define wroteVOLInternalStatusRtfh 0x0800+readVOLInternalStatusRtfh
#define wroteVOLExternalStatusRtfh 0x0800+readVOLExternalStatusRtfh

//==========================================================================================
#elif PAF_MAXNUMCHAN_AF==32
//==========================================================================================

#define PAF_VOL_RAMPTIME_BASE 0x08
#define PAF_VOL_RAMPTIME_LENGTH 2

#define PAF_VOL_UNUSED2_BASE PAF_VOL_RAMPTIME_BASE+PAF_VOL_RAMPTIME_LENGTH //10
#define PAF_VOL_UNUSED2_LENGTH 2

#define PAF_VOL_UNUSED3_BASE PAF_VOL_UNUSED2_BASE+PAF_VOL_UNUSED2_LENGTH //12
#define PAF_VOL_UNUSED3_LENGTH 4

#define PAF_VOL_MASTER_BASE PAF_VOL_UNUSED3_BASE+PAF_VOL_UNUSED3_LENGTH//16=0x10
#define PAF_VOL_MASTER_LENGTH  4*2

#define PAF_VOL_TRIM_BASE PAF_VOL_MASTER_BASE+PAF_VOL_MASTER_LENGTH//24 
#define PAF_VOL_TRIM_LENGTH  PAF_MAXNUMCHAN_AF*4*2 

//==========================================================================================

//
//PAF_VOL_TRIM_BASE => PAF_VolumeQuad :: control
//
#define writeVOLTrimLeftN(NN) 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LEFT*8,NN //0
//
#define writeVOLTrimRghtN(NN) 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RGHT*8,NN //1
//
#define writeVOLTrimCntrN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_CNTR*8,NN //2
//
#define writeVOLTrimLCtrN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LCTR*8,NN //2
#define writeVOLTrimRCtrN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RCTR*8,NN //3
//
#define writeVOLTrimWideN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_WIDE*8,NN //4
#define writeVOLTrimLWidN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LWID*8,NN //4
//
#define writeVOLTrimRWidN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RWID*8,NN //5
//
//PAF_OVER	6 : Undefined
//PAF_LOVR	6
//#define writeVOLTrimOverN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_OVER*8,NN //6
//#define writeVOLTrimLOvrN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LOVR*8,NN //6
//
#define writeVOLTrimROvrN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_ROVR*8,NN //7
//
#define writeVOLTrimLtmdN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LTMD*8,NN //6
#define writeVOLTrimRtmdN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RTMD*8,NN //7

#define writeVOLTrimSurrN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_SURR*8,NN //8
#define writeVOLTrimLSurN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LSUR*8,NN //8
//
#define writeVOLTrimRSurN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RSUR*8,NN //9
#define writeVOLTrimBackN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_BACK*8,NN //10
#define writeVOLTrimLBakN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LBAK*8,NN //10
//
#define writeVOLTrimRBakN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RBAK*8,NN //11
#define writeVOLTrimSubwN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_SUBW*8,NN //12
#define writeVOLTrimLSubN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LSUB*8,NN //12
//
#define writeVOLTrimRSubN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RSUB*8,NN //13
#define writeVOLTrimLHedN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LHED*8,NN //14
#define writeVOLTrimRHedN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RHED*8,NN //15
//
#define writeVOLTrimCHedN(NN)  	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_CHED*8,NN //16
#define writeVOLTrimTOverHdN(NN) 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_OVER_HD*8,NN //17
#define writeVOLTrimLHSiN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LHSI*8,NN //18
#define writeVOLTrimRHSiN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RHSI*8,NN //19
#define writeVOLTrimLHBkN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LHBK*8,NN //20
#define writeVOLTrimRHBkN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RHBK*8,NN //21
#define writeVOLTrimCHBkN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_CHBK*8,NN //22
//..
// Three channels -unassigned viz 23,24,25
//..
#define writeVOLTrimLCtrHdN(NN)	0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LCTR_HD*8,NN  //26
#define writeVOLTrimRCtrHdN(NN)	0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RCTR_HD*8,NN //27
//..
// Four channels 28,29,30,31 -unassigned ie till PAF_VOL_TRIM_BASE+PAF_MAXNUMCHAN_AF*8
//

//=============================================================================================

//
//PAF_VOL_TRIM_BASE => PAF_VolumeQuad :: control
//
#define readVOLTrimLeft  0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LEFT*8  //0
//
#define readVOLTrimRght  0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RGHT*8  //1
//
#define readVOLTrimCntr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_CNTR*8  //2
//
#define readVOLTrimLCtr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LCTR*8  //2
#define readVOLTrimRCtr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RCTR*8  //3
//
#define readVOLTrimWide 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_WIDE*8  //4
#define readVOLTrimLWid 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LWID*8  //4
//
#define readVOLTrimRWid 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RWID*8  //5
//
//PAF_OVER	6 : Undefined
//PAF_LOVR	6
//#define readVOLTrimOver 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_OVER*8  //6
//#define readVOLTrimLOvr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LOVR*8  //6
//
#define readVOLTrimROvr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_ROVR*8  //7
//
#define readVOLTrimLtmd 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LTMD*8  //6
#define readVOLTrimRtmd 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RTMD*8  //7

#define readVOLTrimSurr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_SURR*8  //8
#define readVOLTrimLSur 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LSUR*8  //8
//
#define readVOLTrimRSur 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RSUR*8  //9
#define readVOLTrimBack 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_BACK*8  //10
#define readVOLTrimLBak 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LBAK*8  //10
//
#define readVOLTrimRBak 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RBAK*8  //11
#define readVOLTrimSubw 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_SUBW*8  //12
#define readVOLTrimLSub 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LSUB*8  //12
//
#define readVOLTrimRSub 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RSUB*8  //13
#define readVOLTrimLHed 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LHED*8  //14
#define readVOLTrimRHed 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RHED*8  //15
//
#define readVOLTrimCHed   	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_CHED*8  //16
#define readVOLTrimTOverHd  0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_OVER_HD*8  //17
#define readVOLTrimLHSi 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LHSI*8  //18
#define readVOLTrimRHSi 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RHSI*8  //19
#define readVOLTrimLHBk 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LHBK*8  //20
#define readVOLTrimRHBk 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RHBK*8  //21
#define readVOLTrimCHBk 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_CHBK*8  //22
//..
// Three channels -unassigned viz 23,24,25
//..
#define readVOLTrimLCtrHd 	0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LCTR_HD*8   //26
#define readVOLTrimRCtrHd 	0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RCTR_HD*8  //27
//..
// Four channels 28,29,30,31 -unassigned ie till PAF_VOL_TRIM_BASE+PAF_MAXNUMCHAN_AF*8
//

//==========================================================================================

//
//PAF_VOL_TRIM_BASE+2 => PAF_VolumeQuad :: offset
//
#define writeVOLOffsetLeftN(NN) 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LEFT*8+2,NN //0
//
#define writeVOLOffsetRghtN(NN) 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RGHT*8+2,NN //1
//
#define writeVOLOffsetCntrN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_CNTR*8+2,NN //2
//
#define writeVOLOffsetLCtrN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LCTR*8+2,NN //2
#define writeVOLOffsetRCtrN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RCTR*8+2,NN //3
//
#define writeVOLOffsetWideN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_WIDE*8+2,NN //4
#define writeVOLOffsetLWidN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LWID*8+2,NN //4
//
#define writeVOLOffsetRWidN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RWID*8+2,NN //5
//
//PAF_OVER	6 : Undefined
//PAF_LOVR	6
//#define writeVOLOffsetOverN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_OVER*8+2,NN //6
//#define writeVOLOffsetLOvrN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LOVR*8+2,NN //6
//
#define writeVOLOffsetROvrN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_ROVR*8+2,NN //7
//
#define writeVOLOffsetLtmdN(NN)  0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LTMD*8+2,NN //6
#define writeVOLOffsetRtmdN(NN)  0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RTMD*8+2,NN //7

#define writeVOLOffsetSurrN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_SURR*8+2,NN //8
#define writeVOLOffsetLSurN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LSUR*8+2,NN //8
//
#define writeVOLOffsetRSurN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RSUR*8+2,NN //9
#define writeVOLOffsetBackN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_BACK*8+2,NN //10
#define writeVOLOffsetLBakN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LBAK*8+2,NN //10
//
#define writeVOLOffsetRBakN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RBAK*8+2,NN //11
#define writeVOLOffsetSubwN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_SUBW*8+2,NN //12
#define writeVOLOffsetLSubN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LSUB*8+2,NN //12
//
#define writeVOLOffsetRSubN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RSUB*8+2,NN //13
#define writeVOLOffsetLHedN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LHED*8+2,NN //14
#define writeVOLOffsetRHedN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RHED*8+2,NN //15
//
#define writeVOLOffsetCHedN(NN)  	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_CHED*8+2,NN //16
#define writeVOLOffsetTOverHdN(NN) 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_OVER_HD*8+2,NN //17
#define writeVOLOffsetLHSiN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LHSI*8+2,NN //18
#define writeVOLOffsetRHSiN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RHSI*8+2,NN //19
#define writeVOLOffsetLHBkN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LHBK*8+2,NN //20
#define writeVOLOffsetRHBkN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RHBK*8+2,NN //21
#define writeVOLOffsetCHBkN(NN)	 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_CHBK*8+2,NN //22
//..
// Three channels -unassigned viz 23,24,25
//..
#define writeVOLOffsetLCtrHdN(NN) 0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LCTR_HD*8+2,NN  //26
#define writeVOLOffsetRCtrHdN(NN)	0xcb00+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RCTR_HD*8+2,NN //27
//..
// Four channels 28,29,30,31 -unassigned ie till PAF_VOL_TRIM_BASE+PAF_MAXNUMCHAN_AF*8


//==========================================================================================



//
//PAF_VOL_TRIM_BASE+2 => PAF_VolumeQuad :: offset
//
#define readVOLOffsetLeft  0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LEFT*8+2  //0
//
#define readVOLOffsetRght  0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RGHT*8+2  //1
//
#define readVOLOffsetCntr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_CNTR*8+2  //2
//
#define readVOLOffsetLCtr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LCTR*8+2  //2
#define readVOLOffsetRCtr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RCTR*8+2  //3
//
#define readVOLOffsetWide 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_WIDE*8+2  //4
#define readVOLOffsetLWid 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LWID*8+2  //4
//
#define readVOLOffsetRWid 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RWID*8+2  //5
//
//PAF_OVER	6 : Undefined
//PAF_LOVR	6
//#define readVOLOffsetOver 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_OVER*8+2  //6
//#define readVOLOffsetLOvr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LOVR*8+2  //6
//
#define readVOLOffsetROvr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_ROVR*8+2  //7
//
#define readVOLOffsetLtmd 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LTMD*8+2  //6
#define readVOLOffsetRtmd 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RTMD*8+2  //7

#define readVOLOffsetSurr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_SURR*8+2  //8
#define readVOLOffsetLSur 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LSUR*8+2  //8
//
#define readVOLOffsetRSur 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RSUR*8+2  //9
#define readVOLOffsetBack 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_BACK*8+2  //10
#define readVOLOffsetLBak 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LBAK*8+2  //10
//
#define readVOLOffsetRBak 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RBAK*8+2  //11
#define readVOLOffsetSubw 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_SUBW*8+2  //12
#define readVOLOffsetLSub 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LSUB*8+2  //12
//
#define readVOLOffsetRSub 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RSUB*8+2  //13
#define readVOLOffsetLHed 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LHED*8+2  //14
#define readVOLOffsetRHed 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RHED*8+2  //15
//
#define readVOLOffsetCHed   	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_CHED*8+2  //16
#define readVOLOffsetTOverHd  0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_OVER_HD*8+2  //17
#define readVOLOffsetLHSi 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LHSI*8+2  //18
#define readVOLOffsetRHSi 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RHSI*8+2  //19
#define readVOLOffsetLHBk 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LHBK*8+2  //20
#define readVOLOffsetRHBk 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RHBK*8+2  //21
#define readVOLOffsetCHBk 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_CHBK*8+2  //22
//..
// Three channels -unassigned viz 23,24,25
//..
#define readVOLOffsetLCtrHd  0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LCTR_HD*8+2   //26
#define readVOLOffsetRCtrHd 	0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RCTR_HD*8+2  //27
//..
// Four channels 28,29,30,31 -unassigned ie till PAF_VOL_TRIM_BASE+PAF_MAXNUMCHAN_AF*8

//==========================================================================================



//
//PAF_VOL_TRIM_BASE+6 => PAF_VolumeQuad :: internal status
//
#define readVOLInternalStatusLeft 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LEFT*8+4 //0
//
#define readVOLInternalStatusRght 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RGHT*8+4 //1
//
#define readVOLInternalStatusCntr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_CNTR*8+4 //2
//
#define readVOLInternalStatusLCtr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LCTR*8+4 //2
#define readVOLInternalStatusRCtr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RCTR*8+4 //3
//
#define readVOLInternalStatusWide 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_WIDE*8+4 //4
#define readVOLInternalStatusLWid 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LWID*8+4 //4
//
#define readVOLInternalStatusRWid 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RWID*8+4 //5
//
//PAF_OVER	6 : Undefined
//PAF_LOVR	6
//#define readVOLInternalStatusOver 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_OVER*8+4 //6
//#define readVOLInternalStatusLOvr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LOVR*8+4 //6
//
#define readVOLInternalStatusROvr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_ROVR*8+4 //7
//
#define readVOLInternalStatusLtmd 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LTMD*8+4 //6
#define readVOLInternalStatusRtmd 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RTMD*8+4 //7

#define readVOLInternalStatusSurr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_SURR*8+4 //8
#define readVOLInternalStatusLSur 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LSUR*8+4 //8
//
#define readVOLInternalStatusRSur 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RSUR*8+4 //9
#define readVOLInternalStatusBack 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_BACK*8+4 //10
#define readVOLInternalStatusLBak 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LBAK*8+4 //10
//
#define readVOLInternalStatusRBak 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RBAK*8+4 //11
#define readVOLInternalStatusSubw 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_SUBW*8+4 //12
#define readVOLInternalStatusLSub 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LSUB*8+4 //12
//
#define readVOLInternalStatusRSub 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RSUB*8+4 //13
#define readVOLInternalStatusLHed 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LHED*8+4 //14
#define readVOLInternalStatusRHed 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RHED*8+4 //15
//
#define readVOLInternalStatusCHed   	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_CHED*8+4 //16
#define readVOLInternalStatusTOverHd	0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_OVER_HD*8+4 //17
#define readVOLInternalStatusLHSi 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LHSI*8+4 //18
#define readVOLInternalStatusRHSi 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RHSI*8+4 //19
#define readVOLInternalStatusLHBk 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LHBK*8+4 //20
#define readVOLInternalStatusRHBk 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RHBK*8+4 //21
#define readVOLInternalStatusCHBk 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_CHBK*8+4 //22
//..
// Three channels -unassigned viz 23,24,25
//..
#define readVOLInternalStatusLCtrHd	0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LCTR_HD*8+4  //26
#define readVOLInternalStatusRCtrHd 	0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RCTR_HD*8+4 //27
//..
// Four channels 28,29,30,31 -unassigned ie till PAF_VOL_TRIM_BASE+PAF_MAXNUMCHAN_AF*8
//
//==========================================================================================
//
//

//
//PAF_VOL_TRIM_BASE+6 => PAF_VolumeQuad :: external status
//
#define readVOLExternalStatusLeft 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LEFT*8+6 //0
//
#define readVOLExternalStatusRght 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RGHT*8+6 //1
//
#define readVOLExternalStatusCntr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_CNTR*8+6 //2
//
#define readVOLExternalStatusLCtr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LCTR*8+6 //2
#define readVOLExternalStatusRCtr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RCTR*8+6 //3
//
#define readVOLExternalStatusWide 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_WIDE*8+6 //4
#define readVOLExternalStatusLWid 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LWID*8+6 //4
//
#define readVOLExternalStatusRWid 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RWID*8+6 //5
//
//PAF_OVER	6 : Undefined
//PAF_LOVR	6
//#define readVOLExternalStatusOver 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_OVER*8+6 //6
//#define readVOLExternalStatusLOvr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LOVR*8+6 //6
//
#define readVOLExternalStatusROvr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_ROVR*8+6 //7
//
#define readVOLExternalStatusLtmd 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LTMD*8+6 //6
#define readVOLExternalStatusRtmd 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RTMD*8+6 //7

#define readVOLExternalStatusSurr 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_SURR*8+6 //8
#define readVOLExternalStatusLSur 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LSUR*8+6 //8
//
#define readVOLExternalStatusRSur 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RSUR*8+6 //9
#define readVOLExternalStatusBack 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_BACK*8+6 //10
#define readVOLExternalStatusLBak 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LBAK*8+6 //10
//
#define readVOLExternalStatusRBak 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RBAK*8+6 //11
#define readVOLExternalStatusSubw 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_SUBW*8+6 //12
#define readVOLExternalStatusLSub 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LSUB*8+6 //12
//
#define readVOLExternalStatusRSub 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RSUB*8+6 //13
#define readVOLExternalStatusLHed 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LHED*8+6 //14
#define readVOLExternalStatusRHed 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RHED*8+6 //15
//
#define readVOLExternalStatusCHed   	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_CHED*8+6 //16
#define readVOLExternalStatusTOverHd	0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_OVER_HD*8+6 //17
#define readVOLExternalStatusLHSi 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LHSI*8+6 //18
#define readVOLExternalStatusRHSi 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RHSI*8+6 //19
#define readVOLExternalStatusLHBk 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LHBK*8+6 //20
#define readVOLExternalStatusRHBk 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RHBK*8+6 //21
#define readVOLExternalStatusCHBk 	 0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_CHBK*8+6 //22
//..
// Three channels -unassigned viz 23,24,25
//..
#define readVOLExternalStatusLCtrHd	0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_LCTR_HD*8+6  //26
#define readVOLExternalStatusRCtrHd 	0xc300+STD_BETA_VOLUME,PAF_VOL_TRIM_BASE+PAF_RCTR_HD*8+6 //27
//..
// Four channels 28,29,30,31 -unassigned ie till PAF_VOL_TRIM_BASE+PAF_MAXNUMCHAN_AF*8
//
//==========================================================================================
//
//


#define wroteVOLTrimLeft 0x800+readVOLTrimLeft	
#define wroteVOLTrimRght 0x800+readVOLTrimRght	
#define wroteVOLTrimCntr 0x800+readVOLTrimCntr	
#define wroteVOLTrimWide 0x800+readVOLTrimWide	
#define wroteVOLTrimLWid 0x800+readVOLTrimLWid	
#define wroteVOLTrimRWid 0x800+readVOLTrimRWid	
#define wroteVOLTrimROvr 0x800+readVOLTrimROvr	//7
#define wroteVOLTrimLtmd 0x800+readVOLTrimLtmd	//6
#define wroteVOLTrimRtmd 0x800+readVOLTrimRtmd	//7
#define wroteVOLTrimSurr 0x800+readVOLTrimSurr	//8
#define wroteVOLTrimLSur 0x800+readVOLTrimLSur	//8
#define wroteVOLTrimRSur 0x800+readVOLTrimRSur	//9
#define wroteVOLTrimBack 0x800+readVOLTrimBack	//10
#define wroteVOLTrimLBak 0x800+readVOLTrimLBak	//10
#define wroteVOLTrimRBak 0x800+readVOLTrimRBak	//11
#define wroteVOLTrimSubw 0x800+readVOLTrimSubw	//12
#define wroteVOLTrimLSub 0x800+readVOLTrimLSub	//12
#define wroteVOLTrimRSub 0x800+readVOLTrimRSub	//13
#define wroteVOLTrimLHed 0x800+readVOLTrimLHed	//14
#define wroteVOLTrimRHed 0x800+readVOLTrimRHed	//15
#define wroteVOLTrimCHed 0x800+readVOLTrimCHed	//16
#define wroteVOLTrimTOverHd 0x800+readVOLTrimTOverHd	
#define wroteVOLTrimLHSi 0x800+readVOLTrimLHSi	//18
#define wroteVOLTrimRHSi 0x800+readVOLTrimRHSi	//19
#define wroteVOLTrimLHBk 0x800+readVOLTrimLHBk	//20
#define wroteVOLTrimRHBk 0x800+readVOLTrimRHBk	//21
#define wroteVOLTrimCHBk 0x800+readVOLTrimCHBk	//22
#define wroteVOLTrimRCtrHd 0x800+readVOLTrimRCtrHd	//27

#define wroteVOLOffsetLeft 0x800+readVOLOffsetLeft	
#define wroteVOLOffsetRght 0x800+readVOLOffsetRght	
#define wroteVOLOffsetCntr 0x800+readVOLOffsetCntr	
#define wroteVOLOffsetWide 0x800+readVOLOffsetWide	
#define wroteVOLOffsetLWid 0x800+readVOLOffsetLWid	
#define wroteVOLOffsetRWid 0x800+readVOLOffsetRWid	
#define wroteVOLOffsetROvr 0x800+readVOLOffsetROvr	//7
#define wroteVOLOffsetLtmd 0x800+readVOLOffsetLtmd	//6
#define wroteVOLOffsetRtmd 0x800+readVOLOffsetRtmd	//7
#define wroteVOLOffsetSurr 0x800+readVOLOffsetSurr	//8
#define wroteVOLOffsetLSur 0x800+readVOLOffsetLSur	//8
#define wroteVOLOffsetRSur 0x800+readVOLOffsetRSur	//9
#define wroteVOLOffsetBack 0x800+readVOLOffsetBack	//10
#define wroteVOLOffsetLBak 0x800+readVOLOffsetLBak	//10
#define wroteVOLOffsetRBak 0x800+readVOLOffsetRBak	//11
#define wroteVOLOffsetSubw 0x800+readVOLOffsetSubw	//12
#define wroteVOLOffsetLSub 0x800+readVOLOffsetLSub	//12
#define wroteVOLOffsetRSub 0x800+readVOLOffsetRSub	//13
#define wroteVOLOffsetLHed 0x800+readVOLOffsetLHed	//14
#define wroteVOLOffsetRHed 0x800+readVOLOffsetRHed	//15
#define wroteVOLOffsetCHed 0x800+readVOLOffsetCHed	//16
#define wroteVOLOffsetTOverHd 0x800+readVOLOffsetTOverHd	
#define wroteVOLOffsetLHSi 0x800+readVOLOffsetLHSi	//18
#define wroteVOLOffsetRHSi 0x800+readVOLOffsetRHSi	//19
#define wroteVOLOffsetLHBk 0x800+readVOLOffsetLHBk	//20
#define wroteVOLOffsetRHBk 0x800+readVOLOffsetRHBk	//21
#define wroteVOLOffsetCHBk 0x800+readVOLOffsetCHBk	//22
#define wroteVOLOffsetRCtrHd 0x800+readVOLOffsetRCtrHd	//27

#define wroteVOLExternalStatusLeft 0x800+readVOLExternalStatusLeft	
#define wroteVOLExternalStatusRght 0x800+readVOLExternalStatusRght	
#define wroteVOLExternalStatusCntr 0x800+readVOLExternalStatusCntr	
#define wroteVOLExternalStatusWide 0x800+readVOLExternalStatusWide	
#define wroteVOLExternalStatusLWid 0x800+readVOLExternalStatusLWid	
#define wroteVOLExternalStatusRWid 0x800+readVOLExternalStatusRWid	
#define wroteVOLExternalStatusROvr 0x800+readVOLExternalStatusROvr	//7
#define wroteVOLExternalStatusLtmd 0x800+readVOLExternalStatusLtmd	//6
#define wroteVOLExternalStatusRtmd 0x800+readVOLExternalStatusRtmd	//7
#define wroteVOLExternalStatusSurr 0x800+readVOLExternalStatusSurr	//8
#define wroteVOLExternalStatusLSur 0x800+readVOLExternalStatusLSur	//8
#define wroteVOLExternalStatusRSur 0x800+readVOLExternalStatusRSur	//9
#define wroteVOLExternalStatusBack 0x800+readVOLExternalStatusBack	//10
#define wroteVOLExternalStatusLBak 0x800+readVOLExternalStatusLBak	//10
#define wroteVOLExternalStatusRBak 0x800+readVOLExternalStatusRBak	//11
#define wroteVOLExternalStatusSubw 0x800+readVOLExternalStatusSubw	//12
#define wroteVOLExternalStatusLSub 0x800+readVOLExternalStatusLSub	//12
#define wroteVOLExternalStatusRSub 0x800+readVOLExternalStatusRSub	//13
#define wroteVOLExternalStatusLHed 0x800+readVOLExternalStatusLHed	//14
#define wroteVOLExternalStatusRHed 0x800+readVOLExternalStatusRHed	//15
#define wroteVOLExternalStatusCHed 0x800+readVOLExternalStatusCHed	//16
#define wroteVOLExternalStatusTOverHd 0x800+readVOLExternalStatusTOverHd	
#define wroteVOLExternalStatusLHSi 0x800+readVOLExternalStatusLHSi	//18
#define wroteVOLExternalStatusRHSi 0x800+readVOLExternalStatusRHSi	//19
#define wroteVOLExternalStatusLHBk 0x800+readVOLExternalStatusLHBk	//20
#define wroteVOLExternalStatusRHBk 0x800+readVOLExternalStatusRHBk	//21
#define wroteVOLExternalStatusCHBk 0x800+readVOLExternalStatusCHBk	//22
#define wroteVOLExternalStatusRCtrHd 0x800+readVOLExternalStatusRCtrHd	//27

#define wroteVOLInternalStatusLeft 0x800+readVOLInternalStatusLeft	
#define wroteVOLInternalStatusRght 0x800+readVOLInternalStatusRght	
#define wroteVOLInternalStatusCntr 0x800+readVOLInternalStatusCntr	
#define wroteVOLInternalStatusWide 0x800+readVOLInternalStatusWide	
#define wroteVOLInternalStatusLWid 0x800+readVOLInternalStatusLWid	
#define wroteVOLInternalStatusRWid 0x800+readVOLInternalStatusRWid	
#define wroteVOLInternalStatusROvr 0x800+readVOLInternalStatusROvr	//7
#define wroteVOLInternalStatusLtmd 0x800+readVOLInternalStatusLtmd	//6
#define wroteVOLInternalStatusRtmd 0x800+readVOLInternalStatusRtmd	//7
#define wroteVOLInternalStatusSurr 0x800+readVOLInternalStatusSurr	//8
#define wroteVOLInternalStatusLSur 0x800+readVOLInternalStatusLSur	//8
#define wroteVOLInternalStatusRSur 0x800+readVOLInternalStatusRSur	//9
#define wroteVOLInternalStatusBack 0x800+readVOLInternalStatusBack	//10
#define wroteVOLInternalStatusLBak 0x800+readVOLInternalStatusLBak	//10
#define wroteVOLInternalStatusRBak 0x800+readVOLInternalStatusRBak	//11
#define wroteVOLInternalStatusSubw 0x800+readVOLInternalStatusSubw	//12
#define wroteVOLInternalStatusLSub 0x800+readVOLInternalStatusLSub	//12
#define wroteVOLInternalStatusRSub 0x800+readVOLInternalStatusRSub	//13
#define wroteVOLInternalStatusLHed 0x800+readVOLInternalStatusLHed	//14
#define wroteVOLInternalStatusRHed 0x800+readVOLInternalStatusRHed	//15
#define wroteVOLInternalStatusCHed 0x800+readVOLInternalStatusCHed	//16
#define wroteVOLInternalStatusTOverHd 0x800+readVOLInternalStatusTOverHd	
#define wroteVOLInternalStatusLHSi 0x800+readVOLInternalStatusLHSi	//18
#define wroteVOLInternalStatusRHSi 0x800+readVOLInternalStatusRHSi	//19
#define wroteVOLInternalStatusLHBk 0x800+readVOLInternalStatusLHBk	//20
#define wroteVOLInternalStatusRHBk 0x800+readVOLInternalStatusRHBk	//21
#define wroteVOLInternalStatusCHBk 0x800+readVOLInternalStatusCHBk	//22
#define wroteVOLInternalStatusRCtrHd 0x800+readVOLInternalStatusRCtrHd	//27

#else

#error unsupported option

#endif /* PAF_MAXNUMCHAN */

#define  readVOLStatus 0xc508,STD_BETA_VOLUME

#if  PAF_MAXNUMCHAN_AF==32
#define  readVOLControl \
         readVOLMode, \
         readVOLChannelCount, \
         readVOLImplementation, \
         readVOLRampTime, \
         readVOLControlMaster, \
         readVOLOffsetMaster, \
         readVOLTrimLeft, \
         readVOLOffsetLeft, \
         readVOLTrimRght, \
         readVOLOffsetRght, \
         readVOLTrimCntr, \
         readVOLOffsetCntr, \
         readVOLTrimWide, \
         readVOLOffsetWide, \
         readVOLTrimRWid, \
         readVOLOffsetRWid, \
         readVOLTrimROvr, \
         readVOLOffsetROvr, \
         readVOLTrimSurr, \
         readVOLOffsetSurr, \
         readVOLTrimRSur, \
         readVOLOffsetRSur, \
         readVOLTrimBack, \
         readVOLOffsetBack, \
         readVOLTrimRBak, \
         readVOLOffsetRBak, \
         readVOLTrimSubw, \
         readVOLOffsetSubw, \
         readVOLTrimRSub, \
         readVOLOffsetRSub, \
         readVOLTrimRHed, \
         readVOLOffsetRHed
         
/*         readVOLTrimHead, \
         readVOLOffsetHead, \
 */

#else

#define  readVOLControl \
         readVOLMode, \
         readVOLChannelCount, \
         readVOLImplementation, \
         readVOLRampTime, \
         readVOLControlMaster, \
         readVOLOffsetMaster, \
         readVOLTrimLeft, \
         readVOLOffsetLeft, \
         readVOLTrimRght, \
         readVOLOffsetRght, \
         readVOLTrimCntr, \
         readVOLOffsetCntr, \
         readVOLTrimLCtr,\
         readVOLOffsetLCtr,\
         readVOLTrimWide, \
         readVOLOffsetWide, \
         readVOLTrimRWid, \
         readVOLOffsetRWid, \
         readVOLTrimOver, \
         readVOLOffsetOver, \
         readVOLTrimROvr, \
         readVOLOffsetROvr, \
         readVOLTrimSurr, \
         readVOLOffsetSurr, \
         readVOLTrimRSur, \
         readVOLOffsetRSur, \
         readVOLTrimBack, \
         readVOLOffsetBack, \
         readVOLTrimRBak, \
         readVOLOffsetRBak, \
         readVOLTrimSubw, \
         readVOLOffsetSubw, \
         readVOLTrimRSub, \
         readVOLOffsetRSub, \
         readVOLTrimHead, \
         readVOLOffsetHead, \
         readVOLTrimRHed, \
         readVOLOffsetRHed

#endif

/* in support of inverse compilation only */
#define writeVOLChannelCountN__16__ writeVOLChannelCountN(16)

/* removed duplicates (for PAF_MAXNUMCHAN==8/16)
   in order to reduce size of "readVOLControl" */

#if 0
         readVOLTrimLCtr, \
         readVOLOffsetLCtr, \
         readVOLTrimLWid, \
         readVOLOffsetLWid, \
         readVOLTrimLOvr, \
         readVOLOffsetLOvr, \
         readVOLTrimLSur, \
         readVOLOffsetLSur, \
         readVOLTrimLBak, \
         readVOLOffsetLBak, \
         readVOLTrimLSub, \
         readVOLOffsetLSub, \
         readVOLTrimLHed, \
         readVOLOffsetLHed, \

#endif

#endif /* _PAFVOL_A */
