/******************************************************************************
 * Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Texas Instruments Incorporated nor the
 *         names of its contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 *   THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

#ifndef _DEM_A
#define _DEM_A

#include <paftyp_a.h>
#include <acpbeta.h>

#define  readDEMMode 0xc200+STD_BETA_DEM,0x0400
#define writeDEMModeDisable 0xca00+STD_BETA_DEM,0x0400
#define writeDEMModeEnable  0xca00+STD_BETA_DEM,0x0401

#define  readDEMFilterMode 0xc200+STD_BETA_DEM,0x0500
#define writeDEMFilterModeOff   0xca00+STD_BETA_DEM,0x0500
#define writeDEMFilterModeAuto  0xca00+STD_BETA_DEM,0x0501
#define writeDEMFilterModeOn    0xca00+STD_BETA_DEM,0x0502

#define  readDEMFilterActive 0xc200+STD_BETA_DEM,0x0600
#define wroteDEMFilterActiveNo  0xca00+STD_BETA_DEM,0x0600
#define wroteDEMFilterActiveYes 0xca00+STD_BETA_DEM,0x0601

#if PAF_MAXNUMCHAN <= 8     /* PAF_ChannelMask = XDAS_Int8 */

#define  readDEMChannelEnable 0xc200+STD_BETA_DEM,0x0800
#define  readDEMChannelActive 0xc200+STD_BETA_DEM,0x0900

#define writeDEMChannelEnableN(NN) 0xca00+STD_BETA_DEM,0x0800+((NN)&0xff)

#elif PAF_MAXNUMCHAN <= 16  /* PAF_ChannelMask = XDAS_Int16 */

#define  readDEMChannelEnable 0xc300+STD_BETA_DEM,0x0008
#define  readDEMChannelActive 0xc300+STD_BETA_DEM,0x000a

#define writeDEMChannelEnableN(NN)    0xcb00+STD_BETA_DEM,0x0008,NN

#elif PAF_MAXNUMCHAN <= 32  /* PAF_ChannelMask = XDAS_Int32 */

#define  readDEMChannelEnable 0xc400+STD_BETA_DEM,0x0008
#define  readDEMChannelActive 0xc400+STD_BETA_DEM,0x000c

#define writeDEMChannelEnableN(NN) 0xcc00+STD_BETA_DEM,0x0008,LSW(NN),MSW(NN)

#else
#error unsupported option: PAF_MAXNUMCHAN > 32
#endif

#if PAF_MAXNUMCHAN == 2

#define writeDEMChannelEnableNone     0xca00+STD_BETA_DEM,0x0800
#define writeDEMChannelEnableLeftRght 0xca00+STD_BETA_DEM,0x0803
#define writeDEMChannelEnableAll      writeDEMChannelEnableLeftRght

#define wroteDEMChannelActiveNone     0xca00+STD_BETA_DEM,0x0900
#define wroteDEMChannelActiveLeftRght 0xca00+STD_BETA_DEM,0x0903

#elif PAF_MAXNUMCHAN == 4

#define writeDEMChannelEnableNone     0xca00+STD_BETA_DEM,0x0800
#define writeDEMChannelEnableLeftRght 0xca00+STD_BETA_DEM,0x0803
#define writeDEMChannelEnableCntrSurr 0xca00+STD_BETA_DEM,0x080c
#define writeDEMChannelEnableAll      0xca00+STD_BETA_DEM,0x080f

#define wroteDEMChannelActiveNone     0xca00+STD_BETA_DEM,0x0900
#define wroteDEMChannelActiveLeftRght 0xca00+STD_BETA_DEM,0x0903
#define wroteDEMChannelActiveCntrSurr 0xca00+STD_BETA_DEM,0x090c
#define wroteDEMChannelActiveAll      0xca00+STD_BETA_DEM,0x090f

#elif PAF_MAXNUMCHAN == 6

#define writeDEMChannelEnableNone     0xca00+STD_BETA_DEM,0x0800
#define writeDEMChannelEnableLeftRght 0xca00+STD_BETA_DEM,0x0803
#define writeDEMChannelEnableLSurRSur 0xca00+STD_BETA_DEM,0x0818
#define writeDEMChannelEnableCntrSubw 0xca00+STD_BETA_DEM,0x0824
#define writeDEMChannelEnableAll      0xca00+STD_BETA_DEM,0x083f

#define wroteDEMChannelActiveNone     0xca00+STD_BETA_DEM,0x0900
#define wroteDEMChannelActiveLeftRght 0xca00+STD_BETA_DEM,0x0903
#define wroteDEMChannelActiveLSurRSur 0xca00+STD_BETA_DEM,0x0918
#define wroteDEMChannelActiveCntrSubw 0xca00+STD_BETA_DEM,0x0924
#define wroteDEMChannelActiveAll      0xca00+STD_BETA_DEM,0x093f

#elif PAF_MAXNUMCHAN == 8

#define writeDEMChannelEnableNone     0xca00+STD_BETA_DEM,0x0800
#define writeDEMChannelEnableLeftRght 0xca00+STD_BETA_DEM,0x0803
#define writeDEMChannelEnableLSurRSur 0xca00+STD_BETA_DEM,0x0818
#define writeDEMChannelEnableLBakRBak 0xca00+STD_BETA_DEM,0x0860
#define writeDEMChannelEnableCntrSubw 0xca00+STD_BETA_DEM,0x0824
#define writeDEMChannelEnableAll      0xca00+STD_BETA_DEM,0x08ff

#define wroteDEMChannelActiveNone     0xca00+STD_BETA_DEM,0x0900
#define wroteDEMChannelActiveLeftRght 0xca00+STD_BETA_DEM,0x0903
#define wroteDEMChannelActiveLSurRSur 0xca00+STD_BETA_DEM,0x0918
#define wroteDEMChannelActiveLBakRBak 0xca00+STD_BETA_DEM,0x0960
#define wroteDEMChannelActiveCntrSubw 0xca00+STD_BETA_DEM,0x0924
#define wroteDEMChannelActiveAll      0xca00+STD_BETA_DEM,0x09ff

#elif PAF_MAXNUMCHAN == 16

#define writeDEMChannelEnableNone     0xcb00+STD_BETA_DEM,0x0008,0x0000
#define writeDEMChannelEnableLeftRght 0xcb00+STD_BETA_DEM,0x0008,0x0003
#define writeDEMChannelEnableLCtrRCtr 0xcb00+STD_BETA_DEM,0x0008,0x000c
#define writeDEMChannelEnableLWidRWid 0xcb00+STD_BETA_DEM,0x0008,0x0030
#define writeDEMChannelEnableLOvrROvr 0xcb00+STD_BETA_DEM,0x0008,0x00c0

#define writeDEMChannelEnableLTrrRTrr 0xcb00+STD_BETA_DEM,0x0008,0x00c0
#define writeDEMChannelEnableLTrhRTrh 0xcb00+STD_BETA_DEM,0x0008,0x00c0

#define writeDEMChannelEnableLSurRSur 0xcb00+STD_BETA_DEM,0x0008,0x0300
#define writeDEMChannelEnableLBakRBak 0xcb00+STD_BETA_DEM,0x0008,0x0c00
#define writeDEMChannelEnableLSubRSub 0xcb00+STD_BETA_DEM,0x0008,0x3000
#define writeDEMChannelEnableLHedRHed 0xcb00+STD_BETA_DEM,0x0008,0xc000

#define writeDEMChannelEnableLTmdRTmd 0xcb00+STD_BETA_DEM,0x0008,0xc000
#define writeDEMChannelEnableLTftRTft 0xcb00+STD_BETA_DEM,0x0008,0xc000
#define writeDEMChannelEnableLTfhRTfh 0xcb00+STD_BETA_DEM,0x0008,0xc000

#define writeDEMChannelEnableAll      0xcb00+STD_BETA_DEM,0x0008,0xffff

#define wroteDEMChannelActiveNone     0xcb00+STD_BETA_DEM,0x000a,0x0000
#define wroteDEMChannelActiveLeftRght 0xcb00+STD_BETA_DEM,0x000a,0x0003
#define wroteDEMChannelActiveLCtrRCtr 0xcb00+STD_BETA_DEM,0x000a,0x000c
#define wroteDEMChannelActiveLWidRWid 0xcb00+STD_BETA_DEM,0x000a,0x0030
#define wroteDEMChannelActiveLOvrROvr 0xcb00+STD_BETA_DEM,0x000a,0x00c0

#define wroteDEMChannelActiveLTrrRTrr 0xcb00+STD_BETA_DEM,0x000a,0x00c0
#define wroteDEMChannelActiveLTrhRTrh 0xcb00+STD_BETA_DEM,0x000a,0x00c0

#define wroteDEMChannelActiveLSurRSur 0xcb00+STD_BETA_DEM,0x000a,0x0300
#define wroteDEMChannelActiveLBakRBak 0xcb00+STD_BETA_DEM,0x000a,0x0c00
#define wroteDEMChannelActiveLSubRSub 0xcb00+STD_BETA_DEM,0x000a,0x3000
#define wroteDEMChannelActiveLHedRHed 0xcb00+STD_BETA_DEM,0x000a,0xc000

#define wroteDEMChannelActiveLTmdRTmd 0xcb00+STD_BETA_DEM,0x000a,0xc000
#define wroteDEMChannelActiveLTftRTft 0xcb00+STD_BETA_DEM,0x000a,0xc000
#define wroteDEMChannelActiveLTfhRTfh 0xcb00+STD_BETA_DEM,0x000a,0xc000

#define wroteDEMChannelActiveAll      0xcb00+STD_BETA_DEM,0x000a,0xffff

#elif PAF_MAXNUMCHAN == 32

#define writeDEMChannelEnableAll      0xcc00+STD_BETA_DEM,0x0008,0xffff,0xffff
#define wroteDEMChannelActiveAll      0xcc00+STD_BETA_DEM,0x000c,0xffff,0xffff

#else /* PAF_MAXNUMCHAN */
#error unsupported option: PAF_MAXNUMCHAN > 32
#endif /* PAF_MAXNUMCHAN */

#define  readDEMStatus 0xc508,STD_BETA_DEM
#define  readDEMControl \
         readDEMMode, \
         readDEMFilterMode, \
         readDEMChannelEnable

#endif /* _DEM_A */

