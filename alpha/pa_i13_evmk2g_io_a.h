
/*
Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef _PAI_EVMK2G_IO_A_H_
#define _PAI_EVMK2G_IO_A_H_

#include <acpbeta.h>
#include <i13_a.h>

// execPAIInOutError : if returned, input/output "none" selected
#define  execPAIInOutError    0xf1ff

// -----------------------------------------------------------------------------
// IB SIO Select Register is set by the execPAIIn* shortcuts

#define  execPAIInNone          0xf120
#define  execPAIInHDMIStereo    0xf121
#define  execPAIInHDMI		    0xf122
#define  execPAIInDigital       0xf123
#define  execPAIInAnalog	    0xf124

// These values reflect the definition of devinp[]
#define DEVINP_NULL             0
#define DEVINP_HDMI_STEREO		1
#define DEVINP_HDMI      		2
#define DEVINP_DIR              3
#define DEVINP_ADC              4
#define DEVINP_N                5

#define wroteIBSioCommandNone           0xca00+STD_BETA_IB,0x0500+DEVINP_NULL
#define wroteIBSioCommandHDMIStereo     0xca00+STD_BETA_IB,0x0500+DEVINP_HDMI_STEREO
#define wroteIBSioCommandHDMI		    0xca00+STD_BETA_IB,0x0500+DEVINP_HDMI
#define wroteIBSioCommandDigital        0xca00+STD_BETA_IB,0x0500+DEVINP_DIR
#define wroteIBSioCommandAnalog         0xca00+STD_BETA_IB,0x0500+DEVINP_ADC

#define wroteIBSioSelectNone            0xca00+STD_BETA_IB,0x0580+DEVINP_NULL
#define wroteIBSioSelectHDMIStereo      0xca00+STD_BETA_IB,0x0580+DEVINP_HDMI_STEREO
#define wroteIBSioSelectHDMI		    0xca00+STD_BETA_IB,0x0580+DEVINP_HDMI
#define wroteIBSioSelectDigital         0xca00+STD_BETA_IB,0x0580+DEVINP_DIR
#define wroteIBSioSelectAnalog          0xca00+STD_BETA_IB,0x0580+DEVINP_ADC


// -----------------------------------------------------------------------------
// OB SIO Select Register is set by the execPAIOut* shortcuts

#define  execPAIOutNone                 0xf130
#define  execPAIOutAnalog               0xf131 //8 channel output analog (24bit)
#define  execPAIOutAnalogSlave          0xf132 //8 channel output analog (24bit)
#define  execPAIOutAnalog12Ch           0xf133 //12 channel output analog (24bit)
#define  execPAIOutAnalog16Ch           0xf134 //16 channel output analog (24bit)

// These values reflect the definition of devout[]
#define DEVOUT_NULL             0
#define DEVOUT_DAC              1
#define DEVOUT_DAC_SLAVE        2
#define DEVOUT_DAC_12CH         3
#define DEVOUT_DAC_16CH         4
#define DEVOUT_N                5


#define wroteOBSioCommandNone                0xca00+STD_BETA_OB,0x0500+DEVOUT_NULL
#define wroteOBSioCommandAnalog              0xca00+STD_BETA_OB,0x0500+DEVOUT_DAC
#define wroteOBSioCommandAnalogSlave         0xca00+STD_BETA_OB,0x0500+DEVOUT_DAC_SLAVE
#define wroteOBSioCommandAnalog12Ch          0xca00+STD_BETA_OB,0x0500+DEVOUT_DAC_12CH
#define wroteOBSioCommandAnalog16Ch          0xca00+STD_BETA_OB,0x0500+DEVOUT_DAC_16CH

#define wroteOBSioSelectNone                 0xca00+STD_BETA_OB,0x0580+DEVOUT_NULL
#define wroteOBSioSelectAnalog               0xca00+STD_BETA_OB,0x0580+DEVOUT_DAC
#define wroteOBSioSelectAnalogSlave          0xca00+STD_BETA_OB,0x0580+DEVOUT_DAC_SLAVE
#define wroteOBSioSelectAnalog12Ch           0xca00+STD_BETA_OB,0x0580+DEVOUT_DAC_12CH
#define wroteOBSioSelectAnalog16Ch           0xca00+STD_BETA_OB,0x0580+DEVOUT_DAC_16CH
// -----------------------------------------------------------------------------

#endif // _PAI_EVMK2G_IO_A_H_
