
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// ACP alpha codes
//
//
//

#ifndef _ACP_A
#define _ACP_A

// phi functions

#define ACP_PHI_ECHO  0x00
#define ACP_PHI_ARITHMETIC 0x01
#define ACP_PHI_ALLOC 0x02
#define ACP_PHI_FREE  0x03
#define ACP_PHI_SLEEP 0x04
#define ACP_PHI_STAT  0x05

// arithmetic phi function (1) symbol definitions

#define ACP_OP 0x80

#define ACP_OP_IDENTITY 0x00
#define ACP_OP_ADD 0x01
#define ACP_OP_SADD 0x02
#define ACP_OP_AND 0x03
#define ACP_OP_ANDNOT 0x04
#define ACP_OP_OR 0x05
#define ACP_OP_ORNOT 0x06
#define ACP_OP_FLDWRT 0x07

#define ACP_OP_SADD16 0x42

#define ACP_EQ 0x80

#define ACP_EQ_IDENTITY (ACP_EQ+ACP_OP_IDENTITY)
#define ACP_EQ_ADD (ACP_EQ+ACP_OP_ADD)
#define ACP_EQ_SADD (ACP_EQ+ACP_OP_SADD)
#define ACP_EQ_AND (ACP_EQ+ACP_OP_AND)
#define ACP_EQ_ANDNOT (ACP_EQ+ACP_OP_ANDNOT)
#define ACP_EQ_OR (ACP_EQ+ACP_OP_OR)
#define ACP_EQ_ORNOT (ACP_EQ+ACP_OP_ORNOT)
#define ACP_EQ_FLDWRT (ACP_EQ+ACP_OP_FLDWRT)

#define ACP_EQ_SADD16 (ACP_EQ+ACP_OP_SADD16)

// arithmetic alpha code symbol definitions

#define writeACPBinaryOp(OP,XX,YY) \
    0xc90c,                   /* alpha[12]            */ \
    0xcdf1,(XX)&0xffff,       /* args[1] = XX (LSW)   */ \
    0xcdf9,((XX)>>16)&0xffff, /* args[1] = XX (MSW)   */ \
    0xcdf2,(YY)&0xffff,       /* args[2] = YY (LSW)   */ \
    0xcdfa,((YY)>>16)&0xffff, /* args[2] = YY (MSW)   */ \
    0xc801,0x1800+(OP),       /* args[1] OP= args[2]  */ \
                              /* *args[1] OP= args[2] */ \
    0xcdf0,0xc100             /* args[0] = 0xffffc100 */

#define writeACPOpIdentity(XX,YY) writeACPBinaryOp(ACP_OP_IDENTITY,XX,YY)
#define writeACPOpAdd(XX,YY) writeACPBinaryOp(ACP_OP_ADD,XX,YY)
#define writeACPOpSatAdd(XX,YY) writeACPBinaryOp(ACP_OP_SADD,XX,YY)
#define writeACPOpAnd(XX,YY) writeACPBinaryOp(ACP_OP_AND,XX,YY)
#define writeACPOpAndNot(XX,YY) writeACPBinaryOp(ACP_OP_ANDNOT,XX,YY)
#define writeACPOpOr(XX,YY) writeACPBinaryOp(ACP_OP_OR,XX,YY)
#define writeACPOpOrNot(XX,YY) writeACPBinaryOp(ACP_OP_ORNOT,XX,YY)

#define writeACPOpSatAdd16(XX,YY) writeACPBinaryOp(ACP_OP_SADD16,XX,YY)

#define writeACPEqIdentity(XX,YY) writeACPBinaryOp(ACP_EQ_IDENTITY,XX,YY)
#define writeACPEqAdd(XX,YY) writeACPBinaryOp(ACP_EQ_ADD,XX,YY)
#define writeACPEqSatAdd(XX,YY) writeACPBinaryOp(ACP_EQ_SADD,XX,YY)
#define writeACPEqAnd(XX,YY) writeACPBinaryOp(ACP_EQ_AND,XX,YY)
#define writeACPEqAndNot(XX,YY) writeACPBinaryOp(ACP_EQ_ANDNOT,XX,YY)
#define writeACPEqOr(XX,YY) writeACPBinaryOp(ACP_EQ_OR,XX,YY)
#define writeACPEqOrNot(XX,YY) writeACPBinaryOp(ACP_EQ_ORNOT,XX,YY)
#define writeACPEqFldWrt(XX,YY) writeACPBinaryOp(ACP_EQ_FLDWRT,XX,YY)

#define writeACPEqSatAdd16(XX,YY) writeACPBinaryOp(ACP_EQ_SADD16,XX,YY)

// comparison code (xi) symbol definitions

#define ACP_XI_TRUE 0
#define ACP_XI_FALSE 1
#define ACP_XI_EQ 2
#define ACP_XI_NE 3
#define ACP_XI_GE 4
#define ACP_XI_GT 5
#define ACP_XI_LE 6
#define ACP_XI_LT 7
#define ACP_XI_AND 8
#define ACP_XI_NOR 9

// memory statistics alpha code symbol definitions

#define ACP_SEG_IRAM  0
#define ACP_SEG_SDRAM 1
#define ACP_SEG_L3RAM 2

#define ACP_STAT_SIZE   0
#define ACP_STAT_USED   1
#define ACP_STAT_LENGTH 2
#define ACP_STAT_FREE   3

#define readACPStat(XX,YY) \
    0xcdf0,ACP_SEG_##XX, \
        /* Type 5-24X Write: arg[0] = ACP_SEG_* -- response=null */ \
    0xc800+ACP_PHI_STAT,0x0d00+ACP_STAT_##YY \
        /* Type 0 Write: IACP_stat( arg[0], (arg[1] = ACP_STAT_*) */ \
        /* -- response=2 words (binary data: LSW, MSW of requested statistic) */

#define readACPStatIRAMSize   readACPStat(IRAM,SIZE)
#define readACPStatIRAMUsed   readACPStat(IRAM,USED)
#define readACPStatIRAMLength readACPStat(IRAM,LENGTH)
#define readACPStatIRAMFree   readACPStat(IRAM,FREE)

#define readACPStatSDRAMSize   readACPStat(SDRAM,SIZE)
#define readACPStatSDRAMUsed   readACPStat(SDRAM,USED)
#define readACPStatSDRAMLength readACPStat(SDRAM,LENGTH)
#define readACPStatSDRAMFree   readACPStat(SDRAM,FREE)

#define readACPStatL3RAMSize   readACPStat(L3RAM,SIZE)
#define readACPStatL3RAMUsed   readACPStat(L3RAM,USED)
#define readACPStatL3RAMLength readACPStat(L3RAM,LENGTH)
#define readACPStatL3RAMFree   readACPStat(L3RAM,FREE)

// await alpha code symbol definitions

#endif /* _ACP_A */
