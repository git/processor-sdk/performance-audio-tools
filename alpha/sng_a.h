/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
//
// SNG Decoder alpha codes
//

#ifndef _SNG_A
#define _SNG_A

#include <acpbeta.h>

#define  readSNGMode 0xc200+STD_BETA_SNG,0x0400
#define writeSNGModeDisable 0xca00+STD_BETA_SNG,0x0400
#define writeSNGModeEnable 0xca00+STD_BETA_SNG,0x0401

#define  readSNGChannel 0xc200+STD_BETA_SNG,0x0500
#define writeSNGChannel(N) 0xca00+STD_BETA_SNG,0x0500+((N)&0xff)
/* in support of inverse compilation only */
#define writeSNGChannel__0__ writeSNGChannel(0)

#define  readSNGCommand 0xc200+STD_BETA_SNG,0x0600
#define writeSNGCommandNone 0xca00+STD_BETA_SNG,0x0600
#define writeSNGCommandReset 0xca00+STD_BETA_SNG,0x0601
#define writeSNGCommandAdvance 0xca00+STD_BETA_SNG,0x0602
#define writeSNGCommandPause 0xca00+STD_BETA_SNG,0x0603
#define writeSNGCommandContinue writeSNGCommandNone 

#define  readSNGSong 0xc200+STD_BETA_SNG,0x0700
#define writeSNGSongNone 0xca00+STD_BETA_SNG,0x0700
#define writeSNGSongNoiseWhiteSatChannel 0xcb00+STD_BETA_SNG,0x0006,0x1001
#define writeSNGSongNoiseWhiteSatChannelContinue 0xca00+STD_BETA_SNG,0x0710
#define writeSNGSongNoiseWhiteSatSequence 0xcb00+STD_BETA_SNG,0x0006,0x1801
#define writeSNGSongNoiseWhiteSatSequenceContinue 0xca00+STD_BETA_SNG,0x0718
#define writeSNGSongNoiseWhiteSubChannel 0xcb00+STD_BETA_SNG,0x0006,0x1101
#define writeSNGSongNoiseWhiteSubChannelContinue 0xca00+STD_BETA_SNG,0x0711
#define writeSNGSongNoiseWhiteSubSequence 0xcb00+STD_BETA_SNG,0x0006,0x1901
#define writeSNGSongNoiseWhiteSubSequenceContinue 0xca00+STD_BETA_SNG,0x0719
#define writeSNGSongNoiseDolbySatChannel 0xcb00+STD_BETA_SNG,0x0006,0x2001
#define writeSNGSongNoiseDolbySatChannelContinue 0xca00+STD_BETA_SNG,0x0720
#define writeSNGSongNoiseDolbySatSequence 0xcb00+STD_BETA_SNG,0x0006,0x2801
#define writeSNGSongNoiseDolbySatSequenceContinue 0xca00+STD_BETA_SNG,0x0728
#define writeSNGSongNoiseTISubChannel 0xcb00+STD_BETA_SNG,0x0006,0x2101
#define writeSNGSongNoiseTISubChannelContinue 0xca00+STD_BETA_SNG,0x0721
#define writeSNGSongNoiseTISubSequence 0xcb00+STD_BETA_SNG,0x0006,0x2901
#define writeSNGSongNoiseTISubSequenceContinue 0xca00+STD_BETA_SNG,0x0729
#define writeSNGSongNoiseTHXSatChannel 0xcb00+STD_BETA_SNG,0x0006,0x3001
#define writeSNGSongNoiseTHXSatChannelContinue 0xca00+STD_BETA_SNG,0x0730
#define writeSNGSongNoiseTHXSatSequence 0xcb00+STD_BETA_SNG,0x0006,0x3801
#define writeSNGSongNoiseTHXSatSequenceContinue 0xca00+STD_BETA_SNG,0x0738
#define writeSNGSongNoiseTHXSubChannel 0xcb00+STD_BETA_SNG,0x0006,0x3101
#define writeSNGSongNoiseTHXSubChannelContinue 0xca00+STD_BETA_SNG,0x0731
#define writeSNGSongNoiseTHXSubSequence 0xcb00+STD_BETA_SNG,0x0006,0x3901
#define writeSNGSongNoiseTHXSubSequenceContinue 0xca00+STD_BETA_SNG,0x0739
#define writeSNGSongToneSatChannel 0xcb00+STD_BETA_SNG,0x0006,0x4001
#define writeSNGSongToneSatChannelContinue 0xca00+STD_BETA_SNG,0x0740
#define writeSNGSongToneSatSequence 0xcb00+STD_BETA_SNG,0x0006,0x4801
#define writeSNGSongToneSatSequenceContinue 0xca00+STD_BETA_SNG,0x0748
#define writeSNGSongToneSubChannel 0xcb00+STD_BETA_SNG,0x0006,0x4101
#define writeSNGSongToneSubChannelContinue 0xca00+STD_BETA_SNG,0x0741
#define writeSNGSongToneSubSequence 0xcb00+STD_BETA_SNG,0x0006,0x4901
#define writeSNGSongToneSubSequenceContinue 0xca00+STD_BETA_SNG,0x0749
#define writeSNGSongMultiTone 0xcb00+STD_BETA_SNG,0x0006,0x5001

#define writeSNGSongNoisePinkSatChannel 0xcb00+STD_BETA_SNG,0x0006,0x6001
#define writeSNGSongNoisePinkSatChannelContinue 0xca00+STD_BETA_SNG,0x0760
#define writeSNGSongNoisePinkSatSequence 0xcb00+STD_BETA_SNG,0x0006,0x6801
#define writeSNGSongNoisePinkSatSequenceContinue 0xca00+STD_BETA_SNG,0x0768
#define writeSNGSongNoisePinkSubChannel 0xcb00+STD_BETA_SNG,0x0006,0x6101
#define writeSNGSongNoisePinkSubChannelContinue 0xca00+STD_BETA_SNG,0x0761
#define writeSNGSongNoisePinkSubSequence 0xcb00+STD_BETA_SNG,0x0006,0x6901
#define writeSNGSongNoisePinkSubSequenceContinue 0xca00+STD_BETA_SNG,0x0769


#define  readSNGChannelConfigurationProgram 0xc600+STD_BETA_SNG,0x2008
#define writeSNGChannelConfigurationProgramUnknown 0xcc00+STD_BETA_SNG,0x0020,0x0000,0x0000

#define writeSNGChannelConfigurationProgramNone 0xcc00+STD_BETA_SNG,0x0020,0x0001,0x0000
#define writeSNGChannelConfigurationProgramMono 0xcc00+STD_BETA_SNG,0x0020,0x0002,0x0000
#define writeSNGChannelConfigurationProgramStereo 0xcc00+STD_BETA_SNG,0x0020,0x0003,0x0000
#define writeSNGChannelConfigurationProgramStereoLtRt 0xcc00+STD_BETA_SNG,0x0020,0x0003,0x0002
#define writeSNGChannelConfigurationProgramStereoMono 0xcc00+STD_BETA_SNG,0x0020,0x0003,0x0003
#define writeSNGChannelConfigurationProgram3Stereo 0xcc00+STD_BETA_SNG,0x0020,0x0108,0x0000
#define writeSNGChannelConfigurationProgramPhantom 0xcc00+STD_BETA_SNG,0x0020,0x0105,0x0000
#define writeSNGChannelConfigurationProgramSurround 0xcc00+STD_BETA_SNG,0x0020,0x010a,0x0000

#define writeSNGChannelConfigurationProgramNone_0 0xcc00+STD_BETA_SNG,0x0020,0x0001,0x0000
#define writeSNGChannelConfigurationProgramMono_0 0xcc00+STD_BETA_SNG,0x0020,0x0002,0x0000
#define writeSNGChannelConfigurationProgramPhantom0_0 0xcc00+STD_BETA_SNG,0x0020,0x0003,0x0000
#define writeSNGChannelConfigurationProgramPhantom0Stereo_0 0xcc00+STD_BETA_SNG,0x0020,0x0003,0x0001
#define writeSNGChannelConfigurationProgramPhantom0LtRt_0 0xcc00+STD_BETA_SNG,0x0020,0x0003,0x0002
#define writeSNGChannelConfigurationProgramPhantom0Mono_0 0xcc00+STD_BETA_SNG,0x0020,0x0003,0x0003
#define writeSNGChannelConfigurationProgramPhantom0Dual_0 0xcc00+STD_BETA_SNG,0x0020,0x0003,0x0004
#define writeSNGChannelConfigurationProgramPhantom1_0 0xcc00+STD_BETA_SNG,0x0020,0x0004,0x0000
#define writeSNGChannelConfigurationProgramPhantom2_0 0xcc00+STD_BETA_SNG,0x0020,0x0005,0x0000
#define writeSNGChannelConfigurationProgramPhantom2Stereo_0 0xcc00+STD_BETA_SNG,0x0020,0x0005,0x0001
#define writeSNGChannelConfigurationProgramPhantom2LtRt_0 0xcc00+STD_BETA_SNG,0x0020,0x0005,0x0002
#define writeSNGChannelConfigurationProgramPhantom2Mono_0 0xcc00+STD_BETA_SNG,0x0020,0x0005,0x0003
#define writeSNGChannelConfigurationProgramPhantom3_0 0xcc00+STD_BETA_SNG,0x0020,0x0006,0x0000
#define writeSNGChannelConfigurationProgramPhantom4_0 0xcc00+STD_BETA_SNG,0x0020,0x0007,0x0000
#define writeSNGChannelConfigurationProgramSurround0_0 0xcc00+STD_BETA_SNG,0x0020,0x0008,0x0000
#define writeSNGChannelConfigurationProgramSurround1_0 0xcc00+STD_BETA_SNG,0x0020,0x0009,0x0000
#define writeSNGChannelConfigurationProgramSurround2_0 0xcc00+STD_BETA_SNG,0x0020,0x000a,0x0000
#define writeSNGChannelConfigurationProgramSurround2Stereo_0 0xcc00+STD_BETA_SNG,0x0020,0x000a,0x0001
#define writeSNGChannelConfigurationProgramSurround2LtRt_0 0xcc00+STD_BETA_SNG,0x0020,0x000a,0x0002
#define writeSNGChannelConfigurationProgramSurround2Mono_0 0xcc00+STD_BETA_SNG,0x0020,0x000a,0x0003
#define writeSNGChannelConfigurationProgramSurround3_0 0xcc00+STD_BETA_SNG,0x0020,0x000b,0x0000
#define writeSNGChannelConfigurationProgramSurround4_0 0xcc00+STD_BETA_SNG,0x0020,0x000c,0x0000

#define writeSNGChannelConfigurationProgramNone_1 0xcc00+STD_BETA_SNG,0x0020,0x0101,0x0000
#define writeSNGChannelConfigurationProgramMono_1 0xcc00+STD_BETA_SNG,0x0020,0x0102,0x0000
#define writeSNGChannelConfigurationProgramPhantom0_1 0xcc00+STD_BETA_SNG,0x0020,0x0103,0x0000
#define writeSNGChannelConfigurationProgramPhantom0Stereo_1 0xcc00+STD_BETA_SNG,0x0020,0x0103,0x0001
#define writeSNGChannelConfigurationProgramPhantom0LtRt_1 0xcc00+STD_BETA_SNG,0x0020,0x0103,0x0002
#define writeSNGChannelConfigurationProgramPhantom0Mono_1 0xcc00+STD_BETA_SNG,0x0020,0x0103,0x0003
#define writeSNGChannelConfigurationProgramPhantom0Dual_1 0xcc00+STD_BETA_SNG,0x0020,0x0103,0x0004
#define writeSNGChannelConfigurationProgramPhantom1_1 0xcc00+STD_BETA_SNG,0x0020,0x0104,0x0000
#define writeSNGChannelConfigurationProgramPhantom2_1 0xcc00+STD_BETA_SNG,0x0020,0x0105,0x0000
#define writeSNGChannelConfigurationProgramPhantom2Stereo_1 0xcc00+STD_BETA_SNG,0x0020,0x0105,0x0001
#define writeSNGChannelConfigurationProgramPhantom2LtRt_1 0xcc00+STD_BETA_SNG,0x0020,0x0105,0x0002
#define writeSNGChannelConfigurationProgramPhantom2Mono_1 0xcc00+STD_BETA_SNG,0x0020,0x0105,0x0003
#define writeSNGChannelConfigurationProgramPhantom3_1 0xcc00+STD_BETA_SNG,0x0020,0x0106,0x0000
#define writeSNGChannelConfigurationProgramPhantom4_1 0xcc00+STD_BETA_SNG,0x0020,0x0107,0x0000
#define writeSNGChannelConfigurationProgramSurround0_1 0xcc00+STD_BETA_SNG,0x0020,0x0108,0x0000
#define writeSNGChannelConfigurationProgramSurround1_1 0xcc00+STD_BETA_SNG,0x0020,0x0109,0x0000
#define writeSNGChannelConfigurationProgramSurround2_1 0xcc00+STD_BETA_SNG,0x0020,0x010a,0x0000
#define writeSNGChannelConfigurationProgramSurround2Stereo_1 0xcc00+STD_BETA_SNG,0x0020,0x010a,0x0001
#define writeSNGChannelConfigurationProgramSurround2LtRt_1 0xcc00+STD_BETA_SNG,0x0020,0x010a,0x0002
#define writeSNGChannelConfigurationProgramSurround2Mono_1 0xcc00+STD_BETA_SNG,0x0020,0x010a,0x0003
#define writeSNGChannelConfigurationProgramSurround3_1 0xcc00+STD_BETA_SNG,0x0020,0x010b,0x0000
#define writeSNGChannelConfigurationProgramSurround4_1 0xcc00+STD_BETA_SNG,0x0020,0x010c,0x0000

//ATMOS
#define writeSNGChannelConfigurationProgramSurround2ATMOSMono_0(n) 0xce00+STD_BETA_SNG,0x2008,0x000a,0x0003,(n<<8),0x0000
#define writeSNGChannelConfigurationProgramSurround2ATMOSMono_1(n) 0xce00+STD_BETA_SNG,0x2008,0x010a,0x0003,(n<<8),0x0000
#define writeSNGChannelConfigurationProgramSurround2ATMOSMono_2(n) 0xce00+STD_BETA_SNG,0x2008,0x020a,0x0003,(n<<8),0x0000

#define writeSNGChannelConfigurationProgramSurround2LwRwATMOSMono_0(n) 0xce00+STD_BETA_SNG,0x2008,0x000a,0x0103,(n<<8),0x0000
#define writeSNGChannelConfigurationProgramSurround2LwRwATMOSMono_1(n) 0xce00+STD_BETA_SNG,0x2008,0x010a,0x0103,(n<<8),0x0000
#define writeSNGChannelConfigurationProgramSurround2LwRwATMOSMono_2(n) 0xce00+STD_BETA_SNG,0x2008,0x020a,0x0103,(n<<8),0x0000

#define writeSNGChannelConfigurationProgramSurround4ATMOSMono_0(n) 0xce00+STD_BETA_SNG,0x2008,0x000c,0x0003,(n<<8),0x0000
#define writeSNGChannelConfigurationProgramSurround4ATMOSMono_1(n) 0xce00+STD_BETA_SNG,0x2008,0x010c,0x0003,(n<<8),0x0000
#define writeSNGChannelConfigurationProgramSurround4ATMOSMono_2(n) 0xce00+STD_BETA_SNG,0x2008,0x020c,0x0003,(n<<8),0x0000

#define writeSNGChannelConfigurationProgramSurround4LwRwATMOSMono_0(n) 0xce00+STD_BETA_SNG,0x2008,0x000c,0x0103,(n<<8),0x0000
#define writeSNGChannelConfigurationProgramSurround4LwRwATMOSMono_1(n) 0xce00+STD_BETA_SNG,0x2008,0x010c,0x0103,(n<<8),0x0000
#define writeSNGChannelConfigurationProgramSurround4LwRwATMOSMono_2(n) 0xce00+STD_BETA_SNG,0x2008,0x020c,0x0103,(n<<8),0x0000

#define writeSNGChannelConfigurationProgramSurround2LtfRtfLtrRtr_0 writeSNGChannelConfigurationProgramSurround2ATMOSMono_0(0xA)
#define writeSNGChannelConfigurationProgramSurround2LtfRtfLtrRtr_1 writeSNGChannelConfigurationProgramSurround2ATMOSMono_1(0xA)
#define writeSNGChannelConfigurationProgramSurround2LtfRtfLtrRtr_2 writeSNGChannelConfigurationProgramSurround2ATMOSMono_2(0xA)

#define writeSNGChannelConfigurationProgramSurround4LtmRtm_0 writeSNGChannelConfigurationProgramSurround4ATMOSMono_0(0x4)
#define writeSNGChannelConfigurationProgramSurround4LtmRtm_1 writeSNGChannelConfigurationProgramSurround4ATMOSMono_1(0x4)
#define writeSNGChannelConfigurationProgramSurround4LtmRtm_2 writeSNGChannelConfigurationProgramSurround4ATMOSMono_2(0x4)

#define writeSNGChannelConfigurationProgramSurround2LwRwLtfRtfLtrRtr_0 writeSNGChannelConfigurationProgramSurround2LwRwATMOSMono_0(0xA)
#define writeSNGChannelConfigurationProgramSurround2LwRwLtfRtfLtrRtr_1 writeSNGChannelConfigurationProgramSurround2LwRwATMOSMono_1(0xA)
#define writeSNGChannelConfigurationProgramSurround2LwRwLtfRtfLtrRtr_2 writeSNGChannelConfigurationProgramSurround2LwRwATMOSMono_2(0xA)

#define writeSNGChannelConfigurationProgramSurround4LtfRtfLtrRtr_0 writeSNGChannelConfigurationProgramSurround4ATMOSMono_0(0xA)
#define writeSNGChannelConfigurationProgramSurround4LtfRtfLtrRtr_1 writeSNGChannelConfigurationProgramSurround4ATMOSMono_1(0xA)
#define writeSNGChannelConfigurationProgramSurround4LtfRtfLtrRtr_2 writeSNGChannelConfigurationProgramSurround4ATMOSMono_2(0xA)

#define writeSNGChannelConfigurationProgramSurround4LwRwLtfRtfLtrRtr_0 writeSNGChannelConfigurationProgramSurround4LwRwATMOSMono_0(0xA)
#define writeSNGChannelConfigurationProgramSurround4LwRwLtfRtfLtrRtr_1 writeSNGChannelConfigurationProgramSurround4LwRwATMOSMono_1(0xA)
#define writeSNGChannelConfigurationProgramSurround4LwRwLtfRtfLtrRtr_2 writeSNGChannelConfigurationProgramSurround4LwRwATMOSMono_2(0xA)


#define  readSNGVolume 0xc200+STD_BETA_SNG,0x0c00
#define writeSNGVolume(N) 0xca00+STD_BETA_SNG,0x0c00+((N)&0xff)
/* in support of inverse compilation only */
#define writeSNGVolume__0__ writeSNGVolume(0)

#define  readSNGIntervalMS 0xc300+STD_BETA_SNG,0x000e
#define writeSNGIntervalMS(N) 0xcb00+STD_BETA_SNG,0x000e,((N)&0xffff)
#define wroteSNGIntervalMS 0xcb00+STD_BETA_SNG,0x000e

#define  readSNGFreqSatHz 0xc300+STD_BETA_SNG,0x0010
#define writeSNGFreqSatHz(N) 0xcb00+STD_BETA_SNG,0x0010,((N)&0xffff)
#define wroteSNGFreqSatHz 0xcb00+STD_BETA_SNG,0x0010

#define  readSNGFreqSubHz 0xc300+STD_BETA_SNG,0x0012
#define writeSNGFreqSubHz(N) 0xcb00+STD_BETA_SNG,0x0012,((N)&0xffff)
#define wroteSNGFreqSubHz 0xcb00+STD_BETA_SNG,0x0012

#define  readSNGMultSatQ8 0xc300+STD_BETA_SNG,0x0014
#define writeSNGMultSatQ8(N) 0xcb00+STD_BETA_SNG,0x0014,((N)&0xffff)
#define wroteSNGMultSatQ8 0xcb00+STD_BETA_SNG,0x0014

#define  readSNGMultSubQ8 0xc300+STD_BETA_SNG,0x0016
#define writeSNGMultSubQ8(N) 0xcb00+STD_BETA_SNG,0x0016,((N)&0xffff)
#define wroteSNGMultSubQ8 0xcb00+STD_BETA_SNG,0x0016

#define  readSNGPhaseSatMS 0xc300+STD_BETA_SNG,0x0018
#define writeSNGPhaseSatMS(N) 0xcb00+STD_BETA_SNG,0x0018,((N)&0xffff)
#define wroteSNGPhaseSatMS 0xcb00+STD_BETA_SNG,0x0018

#define  readSNGPhaseSubMS 0xc300+STD_BETA_SNG,0x001a
#define writeSNGPhaseSubMS(N) 0xcb00+STD_BETA_SNG,0x001a,((N)&0xffff)
#define wroteSNGPhaseSubMS 0xcb00+STD_BETA_SNG,0x001a

#define  readSNGStatus 0xc508,0x0000+STD_BETA_SNG
#define  readSNGControl \
         readSNGMode, \
         readSNGChannel, \
         readSNGCommand, \
         readSNGSong, \
         readSNGChannelConfigurationProgram, \
         readSNGVolume, \
         readSNGIntervalMS, \
         readSNGFreqSatHz, \
         readSNGFreqSubHz, \
         readSNGMultSubQ8, \
         readSNGMultSubQ8, \
         readSNGPhaseSatMS, \
         readSNGPhaseSubMS

#endif /* _SNG_A */
