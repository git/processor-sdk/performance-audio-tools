
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Series 17 -- System Error Report Alpha File
//
//
//

#include <acpbeta.h>

/* Alpha codes to read First Error reset condition.
   A value of zero means that First Error has been reset
   after alpha code to reset the First Error is sent
*/
#define readASPProcessingResetError			0xc200+STD_BETA_SYSERR,0x0800
#define readAFPProcessingResetError			0xc200+STD_BETA_SYSERR,0x0a00
#define readAIPProcessingResetError			0xc200+STD_BETA_SYSERR,0x0c00

/* Alpha codes to reset the First Error*/
#define writeASPProcessingResetError		0xca00+STD_BETA_SYSERR,0x0801
#define writeAFPProcessingResetError		0xca00+STD_BETA_SYSERR,0x0a01
#define writeAIPProcessingResetError		0xca00+STD_BETA_SYSERR,0x0c01

/* Error mapping for Audio Stream Processing(ASP):
   - All audio stream processing errors are of type Int i.e. 32bits. 
   - byte 0-1 represents the algorithm level errors. 0 means no error.   
   - bit 0-3 of byte 2 denotes the PAF components that has caused the error.
     See paferr.h for PAF components that are relevant here.
   - bit 4-7 of byte 2 denotes the Framework execution status.
     See paferr.h for various Framework execution status that are relevant here.
   - byte 3 is reserved.   
*/

/* Read AIP at-boot and AFP alpha code processing errors */
/* A response of 
  - Zero means at boot processing succeeded.
  - Non-zero value reports the at-boot error.
    Error value 0x13 indicates that BETA value is not valid.
    Error value 0x15 indicates that GAMMA value is not valid.
    Error value 0x21 indicates that TIMEOUT occurred.
    Any other Error indicates Unknown error.
*/

/* Alpha codes to read first Error after it is reset*/
#define readASPProcessingFirstError			0xc400+STD_BETA_SYSERR,0x001c
#define readAFPProcessingFirstError			0xc400+STD_BETA_SYSERR,0x0024
#define readAIPProcessingFirstError			0xc400+STD_BETA_SYSERR,0x002c

/* Alpha codes to read Current Error*/
#define readASPProcessingCurrentError		0xc400+STD_BETA_SYSERR,0x0044
#define readAFPProcessingCurrentError		0xc400+STD_BETA_SYSERR,0x004c
#define readAIPProcessingCurrentError		0xc400+STD_BETA_SYSERR,0x0054
