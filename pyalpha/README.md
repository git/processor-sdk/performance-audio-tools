# PyAlpha
PyAlpha sends alpha commands over a UART connection (via RS-232), useful for
configuring an audio device under test.

---

## Dependencies
- Windows 7
- [Python 2.7 for Windows](https://www.python.org/ftp/python/2.7.12/python-2.7.12.msi)

---

## Usage
**All commands run from the /tools/ directory**

`$ python pyalpha <options> [alphas]`

#### Configuration
By default, configuration options are set in `tools/config.ini`. Setting a
CLI flag will override the INI configuration for that specific option.
The INI file can configure port, header file, baud rate, alpha directory,
binary directory, and temporary storage location.

#### Alphas
PyAlpha requires a list of one or more alpha commands separated by spaces or
commas in their alphanumeric and/or hex representations.

#### Options
- `-?`, `--help` display help dialogue
- `-h <file>` uses this header for symbol definitions, as well as using a
  corresponding .hdM file for decomposition of received alpha commands. Similar
  to calfa, these header files are located in an `alpha/` directory which by
  default is expected to be found at `./alpha`, but this location can be
  changed with the `-I` flag.
- `-p <port>` use this active serial communication port (default: COM1)
- `-I <dir>` use this directory in the search path for alpha headers
- `--repeat <num> <time>` repeat sending an alpha command <num> times with a delay
of <time> seconds (floating point) between each send  
- `--fast` Don't perform any post-processing
- `-r` Retain temporary files
- `-s` Start a session using provided settings
  - type `exit` or `quit` to exit the session
  - type `cls` or `clear` to clear the session screen
  - From within a session, you can start typing and press `tab` to get a list of alpha commands
(note: you must type 3 or more letters before `tab`-completion will work, and completion will
not be effective if attempting to complete on 'write')

More options are listed in `--help`

#### Example
All examples are run from the `tools/` directory
-`$ python pyalpha readDECSourceProgram readPCMMode`
	- sends alpha commands `readDECSourceProgram` and `readPCMMode` using settings
specified in the INI configuration
-`$ python pyalpha -s`
	- starts a pyalpha session using settings specified in the INI configuration
-`$ python pyalpha -p COM5 -h pa_other_hdr_a readDECSourceProgram`
	- sends `readDECSourceProgram` with `COM5` and `pa_other_hdr_a` overriding
port and header settings in the INI configuration
