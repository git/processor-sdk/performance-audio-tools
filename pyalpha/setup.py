from setuptools import setup

setup(name='pyAlpha',
      install_requires=[
          'pyserial',
          'pyreadline'
      ],
      zip_safe=False)
