"""
Copyright (c) 2004 - 2017, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
"""
__version__='1.2.2'

#### Library imports ####
import os
import re
import sys
import time
import errno
import subprocess
from serial import Serial
from random import randint

import logging
log = logging.getLogger('PASTA')

#### Package imports ####
from srecord import sRecord

##########################
#### Module functions ####
def silentremove(filename):
    """Delete a file if exists."""
    try:
        os.remove(filename)
    except OSError as e:
        if e.errno != errno.ENOENT:
            raise # re-raise exception if a different error occured
#### Module Functions ####
##########################

###########################
#### Module Exceptions ####
class ConversionError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)        
#### Module Exceptions ####
###########################

#################
#### PyAlpha ####
class PyAlpha:
    """Interface for sending alpha commands"""


    @staticmethod
    def asdf():
        print "AYYYYY"
    
    def __init__(self, header, port='COM1', baud=19200, retain=False,
                 inc='./alpha', bins='./bin', alfas='.', defs=[]):
        self.port  = port            
        self.baud = baud
        self.header = header
        self.inc = inc
        self.bins = bins
        self.alfas = alfas
        self.defs = defs
        
        self.inverse = {}
        self.retain = retain
        self.build_inverse_mapping() # populates self.inverse

    def send(self, alpha_list, fast=False, separate=False):

        if separate:
            resp = []
            for alpha in alpha_list:
                resp.append(self._send([alpha])) # making a list is a workaround, for now

            log.debug(resp)

            resp = [r.lstrip('alpha ') for r in resp if r != "alpha /* none */"]
            if not resp:
                resp = ['/*none*/']

            return 'alpha ' + ','.join(resp)

        else:
            return self._send(alpha_list, fast)
            
    def _send(self, alpha_list, fast=False):
        """Sends alpha_list as S-Record over UART to the DUT"""
        
        if not alpha_list: return # die early
        
        # Test number a la calfa
        testnum = randint(1000,9999)
        fbase = self.alfas +  '/alfa' + str(testnum)
        
        # Generate C file
        self.gen_c(alpha_list, fbase)

        # Preprocess C file
        self.preprocess(fbase)

        # Convert to S-Record
        ret = self.itox(fbase)
        if ret:
            self.__clean(fbase)
            raise ConversionError('itox failed with code: ' + str(ret))

        # Send S-Record to target device and read response
        ret = self.write_read(fbase)
        if not ret:
            self.__clean(fbase)
            return 'TIMEOUT'

        # Don't do any postprocessing, just return after receiving S-record
        if fast:
            self.__clean(fbase)
            return ret
            
        # Parse S-Record into alpha code
        self.xtoi(fbase)

        # Parse alpha code to alpha command, remove CRLF from the end
        ret = self.inverse_compile(fbase).strip()
        
        # Clean up temporary files
        self.__clean(fbase)

        return ret

                
    def write_read(self, fbase, timeout=5):
        """Read S-Record file (.s) and write. Read response and save to S-Record response file (.x)"""
        s_fname = fbase + '.s'
        x_fname = fbase + '.x'
        
        with Serial(self.port, self.baud, timeout=1) as ser:
            # WRITE
            with open(s_fname, 'rb') as sfile:
                records = sfile.read()
                log.debug('Sending: \n%s', str(records))
                ser.write('\r\n') # A single CRLF must be sent first for proper comm
                ser.write(records)

            # READ
            response = []
            line = None
            t = 0
            while line != "S9030000FC\r\n" and t < timeout:
                line = ser.readline()
                if line:
                    response.append(line)
                else:
                    t += 1

            # Strip away the random leading chars and emove empty lines
            outarr = [line.lstrip('\x13\x11') for line in response if line]
            log.debug('Received: %s', ''.join(outarr))

        with open(x_fname, 'w') as xfile:
            xfile.write(''.join(outarr))

        return outarr
    
    def gen_c(self, alpha_list, fbase):
        """Generate a C file (.c) to be pre-processed"""
        c_fname = fbase + '.c'
        
        with open(c_fname, 'w') as cfile:
            cfile.write('#include "' + self.header + '.h"\n')
            cfile.write('alpha ' + ','.join(alpha_list) + '\n')
    
    def preprocess(self, fbase):
        """Pre-process the C file (.c) to get hex representations of alpha commands in (.pp) file"""
        pp_fname = fbase + '.pp'

        # Begin building arguments list
        args = [ self.bins + '/acp6x.exe']

        # Add pre-processor macro definitions
        for d in self.defs:
            if d:
                args.append("-D%s" % d)

        # Include the specified file, export to the current alfa*.c file
        args += [
            '-I', self.inc,
            '-E',
            fbase + '.c',
        ]

        # Run the pre-processor
        with open(pp_fname, 'w') as ppfile:
            print args
            pid = subprocess.Popen(args, stdout=ppfile, stderr=subprocess.PIPE)
            pid.wait()

    def itox(self, fbase):
        """Calls itox.exe to convert .pp file to S-Record format (.s)"""
        pp_fname = fbase + '.pp'
        s_fname = fbase + '.s'

        args = [
            self.bins + '/itox.exe',
            '-S', # S-record
        ]

        retcode = None
        with open(s_fname, 'wb') as sfile, open(pp_fname, 'rb') as ppfile:
            pid = subprocess.Popen(args, stdout=sfile, stdin=ppfile)
            retcode = pid.wait()

        return retcode
    
    def xtoi(self, fbase):
        """Calls xtoi.exe to convert .x file to hex representation (.y)"""
        x_fname = fbase + '.x'
        y_fname = fbase + '.y'
        
        args = [
            self.bins + '/xtoi.exe',
            '-S'
        ]
        
        with open(x_fname, 'rb') as xfile, open(y_fname, 'wb') as yfile:
            pid = subprocess.Popen(args, stdout=yfile, stdin=xfile)
            pid.wait()
            

    def inverse_compile(self, fbase):
        """Converts hex representation (.y) to textual representation of alpha command (.z)"""
        y_fname = fbase + '.y'
        z_fname = fbase + '.z'

        s = ''
        with open(y_fname, 'rb') as yfile:
            s = yfile.read()

            for code,name in self.inverse.iteritems():
                s = re.sub(code, name, s)

        with open(z_fname, 'wb') as zfile:
            zfile.write(s)

        return s

    def __clean(self, fbase):
        """Removes temporary files, unless otherwise specified (retain)"""

        if not self.retain:
            self.clean_up(fbase)

    
    def clean_up(self, fbase):
        """Removes temporary files"""
        
        file_types = ['.c', '.pp', '.s', '.x', '.y', '.z']

        # Apply file extensions to the fbase
        fnames = [fbase + ft for ft in file_types]
        map(silentremove, fnames)

    def build_inverse_mapping(self):
        """Parses through inverse-compilation file (.hdM) and creates a mapping
        for creation of .z files"""

        # .hdM file is in the include directory with specified name
        fname = self.inc + '/' + self.header + '.hdM'
        
        # Each line in hdM is of the form:
        #	# define alphaCommandName 0x####,0x####,...
        define_str = r"^#define"
        alpha_str  = r"(?P<alpha>\S+)"
        hex_str    = r"(?P<hex>(0x[a-f0-9]{4},?)+)[\r\n]+$"

        # Compile the regular expression
        alpha_re = re.compile(define_str + " " +
                              alpha_str + " " +
                              hex_str)

        # Parse file
        with open(fname, 'rb') as f:
            for line in f:
                mat = re.match(alpha_re, line)
                if not mat: continue

                name = mat.group('alpha')
                codes = mat.group('hex')

                self.inverse[codes] = name

#### PyAlpha ####
#################
