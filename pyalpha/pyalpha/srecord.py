"""
Copyright (c) 2004 - 2017, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
"""
class sRecord:
    header = "S0030000FC"
    footer = "S9030000FC"
    
    @staticmethod
    def num_to_byte_list(num, num_bytes, big_endian=False):
        outarr = []
        for i in range(num_bytes):
            outarr.append(num & 0xff)
            num >>= 8
            
        if big_endian: outarr.reverse()
        return outarr

    @staticmethod
    def length_sequence():
        pass

    @staticmethod
    def generate_record(word_list, record_number):
        
        byte_list = [len(word_list) * 2 + 3]
        byte_list += sRecord.num_to_byte_list(record_number * 15, 2, big_endian=True)

        for word in word_list:
            byte_list += sRecord.num_to_byte_list(word, 2)

        chksum = ~sum(byte_list) & 0xff
        byte_list.append(chksum)
        
        record = 'S1' + ''.join('%02x' % (byte) for byte in byte_list )
        return record.upper()
                    
    @staticmethod
    def generate_message(word_list):
        """
        Converts integer data list into a Little Endian S-Record payload.
        - words: list of 16-bit words
        return :: ASCII S-Record
        """

        message_length = len(word_list) + 0xc900
        word_list = [message_length] + word_list
        
        records = []
        for i in range(len(word_list)/32 + 1):
            idx = i * 32
            left = len(word_list) - idx
            offset = left if left <= 32 else 32
            records.append(sRecord.generate_record(word_list[idx:idx+offset], i))

        
        outarr = [sRecord.header] + records + [sRecord.footer]
        
        return outarr

    

    class MessageParser:
        """
        Not really using it at this point, but allows for future improvements in
        message parsing (e.g. actually using command length fields) if necessary
        """
        
        def __init__(self, message):
            self.commands = []
            self.total_length = 0
            self.command_length = 0

            #print "Parsing", message

            if not message:
                raise Exception('Empty message!')
            if message[0] != sRecord.header:
                raise Exception('Bad S-Message header: ' + message[0])
            if message[-1] != sRecord.footer:
                raise Exception('Bad S-Message footer: ' + message[-1])
            
            for record in message[1:-1]:
                self.parse_record(record)

    
        def parse_record(self, record):
            split = [record[i:i+2] for i in range(0,len(record),2)]

            head   = split[0]
            length = int(split[1], 16)
            body   = split[2:-1]
            chksum = int(split[-1], 16)
            checked = ~sum(int(x, 16) for x in split[1:-1]) & 0xff

            if head != 'S1':
                raise Exception('Bad S-Record header')
            if len(body) + 1 != length:
                raise Exception('Bad S-Record length')
            if chksum != checked:
                raise Exception('Bad S-Record checksum: ' + str(chksum) + "," + str(checked))

            #print body
            fileLength = int(body.pop(1) + body.pop(0), 16)

            while body:

                # c900 yields /* none */
                # c9xx 
                # cd01 - LENGTH

                word = int(body.pop(1) + body.pop(0), 16)
                #if word & 0xff00 == 0xc900:
                    #print "FOUND SIZE", hex(word & 0xff)
                 #   continue

                self.commands.append(word)

    
    @staticmethod
    def parse_message(message):
        mp = sRecord.MessageParser(message)
        return mp.commands
