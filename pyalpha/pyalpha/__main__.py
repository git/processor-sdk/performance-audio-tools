"""
Copyright (c) 2004 - 2017, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
"""
import re
import os
import sys
import time
import argparse
import ConfigParser
import readline
from pyalpha import PyAlpha, ConversionError

import logging
log = logging.getLogger('PASTA')

def death(message):
    logging.error(message)
    sys.exit(-2)

##########################
#### Module Variables ####
confname = "./config.ini"
confcust = "./config_cust.ini"
#### Module Variables ####
##########################

# TODO USE ARGPARSE
parser = argparse.ArgumentParser(description='Alpha Command Communicator',
                                 prog='PyAlpha',
                                 add_help=False)

general_args = [
    (['-?', '--help'], {
        'help' : "show this help message and exit",
        'action' : 'help',
    }),
    
    (['-h', '--header'], {
        'help' : "Add FILE_a.h to symbol definition list and FILE_a.hdM to symbol decomposition list",
        'metavar' : 'FILE',
    }),

    (['-I', '--include'], {
        'help' : "Add DIR to the include path. [default: ./alpha]",
        'metavar' : 'DIR',
        #'default' : './alpha',
    }),

    (['-p', '--port'], {
        'help' : "Communication port [default: COM1]",
        #'default' : 'COM1',
    }),

    (['-b', '--baud'], {
        'help' : "Baud rate [default: 19200]",
        'type' : int,
        #'default' : 19200,
    }),

    (['-t', '--temp'], {
        'help' : "Temporary file save location [default: .]",
        'metavar' : 'DIR',
        #'default' : '.',
    }),

    (['-bin'], {
        'help' : "Binary file location [default: ./bin]",
        'metavar' : 'DIR',
        #'default' : './bin',
    }),


    (['-r', '--retain'], {
        'help' : "Retain temporary files",
        'action' : 'store_true',
    }),

    (['-s', '--session'], {
        'help' : "Run a PyAlpha session, retains startup settings",
        'action' : 'store_true',
    }),

    (['--fast'], {
        'help' : "Skip post-processing",
        'action' : 'store_true',
    }),

    (['--repeat'], {
        'help' : "Repeat an alpha command N times with a T second break between",
        'type' : float,
        'nargs' : 2,
        'metavar' : 'N T',
        'default' : [1.0, 0.0],
    }),
    
    (['alpha_list'], {
        'help' : "Alpha commands, separated by spaces",
        'nargs' : '*',
        'metavar' : 'ALPHAS',
    }),

    (['-log', '--loglevel'], {
        'help' : "Set log level",
        'metavar' : 'LEVEL',
        'default' : 'INFO',
     }),

    (['-D'], {
        'help' : "GCC-style macro definitions",
        'action' : 'append',
        'dest' : 'defs',
    }),
]

# Add CLI arguments to parser
for arg in general_args:
    parser.add_argument(*arg[0], **arg[1])

# Parse arguments
args = parser.parse_args()

# Setup logging
logging.basicConfig(filename='', level=args.loglevel.upper())

# User must provide alpha commands
if not args.alpha_list and not args.session:
    parser.print_usage()
    print 'No ALPHAS provided!'
    sys.exit(-1)

# Set initial values
port =    args.port
header =  args.header
baud =    args.baud
include = args.include
temp =    args.temp
bins =    args.bin
defs =    args.defs
    
# Parse INI file
config = ConfigParser.ConfigParser()
dataset = config.read(confname)

# Check backup INI file
if not len(dataset):
    dataset = config.read(confcust)

if len(dataset):
    port =    port    or config.get('pyalpha', 'port')
    header =  header  or config.get('pyalpha', 'hfile')
    baud =    baud    or config.get('pyalpha', 'baud')
    include = include or config.get('pyalpha', 'include')
    temp =    temp    or config.get('pyalpha', 'temp')
    bins =    bins    or config.get('pyalpha', 'bins')
    defs =    defs    or config.get('pyalpha', 'preprocessor_definitions').split(',')
else:
    log.warning("Could not find config file %s. Resorting to defaults.", confname)

# Options are passed, skipped .INI, apply defaults
if not header:
    parser.print_usage()
    print 'Please provide header! [-h]'
    sys.exit(-1)

p = PyAlpha(header, port = port, baud=baud, retain=args.retain,
            inc=include, bins=bins, alfas=temp, defs=defs)

ALPHAS = []
def parse_alphas():
    alpha_re = re.compile(r"#define\s(?P<alpha>[a-z]\S+)")

    alpha_set = set()
    for fname in os.listdir(include):
        fname = os.path.join(include,fname)
        if fname.lower().endswith('.h') or fname.lower().endswith('.hdm'):
            with open(fname, 'r') as file:
                for line in file:
                    mat = re.match(alpha_re, line)
                    if mat:
                        #print mat.group('alpha')
                        alpha_set.add(mat.group('alpha'))

    global ALPHAS
    ALPHAS = sorted(list(alpha_set))
    
                        
def completer(text, state):
    # Any query under 3 characters will return too many commands
    if len(text) < 3:
        return None

    # Ignore 'write' queries
    if len(text) < 6 and text.startswith('w'):
        return None
    
    for alpha in ALPHAS:
        #if alpha.startswith(text):
        if text in alpha:
            if not state:
                return alpha
            else:
                state -= 1


if args.session:
    # Enable tab-completion
    parse_alphas()
    readline.parse_and_bind("tab: complete")
    readline.set_completer(completer)
    #raise ConversionError('wooo')
    
    while True:
        response = raw_input(">")
        
        if response in ['quit', 'exit']:
            break
        elif response in ['clear', 'cls']:
            os.system('cls' if os.name == 'nt' else 'clear')                
        else:
            alpha_list = response.split(' ')
            try:
                ret = p.send(alpha_list=alpha_list, fast=args.fast)
                print ret
            except ConversionError, e:
                pass
                
else:
    try:
        for i in range(int(args.repeat[0])):
            ret = p.send(alpha_list=args.alpha_list, fast=args.fast)
            time.sleep(args.repeat[1])
            print ret
    except ConversionError, e:
        pass
