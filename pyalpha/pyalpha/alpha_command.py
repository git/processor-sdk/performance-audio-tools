"""
Copyright (c) 2004 - 2016, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
"""
import re
import struct

class AlphaCommand:
    """Object representation of an Alpha Command

    Attributes:
	codes: tuple of integers representing the numerical value of the command
    	name: textual representation (i.e. readPCEMode)
    	default_parameter: default value for parameterized alpha commands
        parameter: will attempt to apply this parameter to the alpha command
    """
    
    @staticmethod
    def num_bytes(to_check):
        """Calculates the number of bytes in a given integer"""
        if to_check == 0: return 1
        
        num = 0
        while to_check:
            to_check >>= 8
            num += 1
            
        return num
    
    def __init__(self, name, codes, parameter=None, default_parameter=None):
        self.codes = codes
        self.name  = name
        self.default_parameter = default_parameter
        self.parameter_bytes = None
        self.parameter = parameter

        # Set the default parameter, if exists
        if default_parameter is not None:
            self.parameter_bytes = AlphaCommand.num_bytes(default_parameter)

        # Apply the parameter
        if parameter is not None and self.default_parameter is not None:
            if parameter > self.default_parameter:
                raise Exception('INVALID PARAMETER: value too high')
            if AlphaCommand.num_bytes(parameter) > self.parameter_bytes:
                raise Exception('INVALID PARAMETER: too many bytes')

            # Clear bits
            mask = 0xffff ^ int(''.join(['f'] * self.parameter_bytes),16)
            codes = list(self.codes)	# Can't assign into a tuple
            codes[-1] &= mask		# Clear out parameter bits
            codes[-1] += parameter	# Apply the parameter
            self.codes = tuple(codes)   # Bring it back to the hashable tuple

        self.codestr = ','.join("0x{:04x}".format(x) for x in codes)

    # Clones current instance with optional new parameter
    def clone(self, parameter=None):
        clone = AlphaCommand(self.name, self.codes,
                             parameter, self.default_parameter)
        print map(hex,clone.codes)
        print clone.parameter
        return clone
        
    def __eq__(self, other):
        return ( type(self) is type(other)
                 and set(self.codes) == set(other.codes))
    
    def __ne__(self, other):
        return not self.__eq__(other)

    @property
    def size(self):
        return len(self.codes)
    
class AlphaCommands:
    """Object representation of hdM file, effectively
    
    Basically a cache of alpha commands, hashed by textual representaiton and numeric representation

    Attributes:
    	
    """
    define_str = r"#define"
    alpha_str  = r"(?P<alpha>[^ \(]+)(\((?P<parameter>[0-9]+)\))?"
    hex_str    = r"(?P<hex>(0x[a-f0-9]{4},?)+)\r?$"

    def __init__(self, header):
        self.header = header
        self.__alphas = {}

        self._parse_header()

    def naive_replace(self, hexstr):
        for key, alpha in self.__alphas.iteritems():
            hexstr = re.sub(alpha.codestr, alpha.name, hexstr)
        
        return hexstr

    def _parse_header(self):
        #alpha_str  = r"(?P<rw>[a-z]+)(?P<module>[A-Z]+)?(?P<rest>([A-Z]|[0-9])\S+) "    
        alpha_re = re.compile(AlphaCommands.define_str + " " +
                              AlphaCommands.alpha_str + " " +
                              AlphaCommands.hex_str)
        
        with open(self.header, 'r') as f:
            for line in f:
                mat = re.match(alpha_re, line)
                if not mat: continue

                parameter = mat.group('parameter')
                #if parameter: print "parameter", parameter
                name = mat.group('alpha')
                codestr = mat.group('hex')
                codes = tuple(int(x, base=16) for x in codestr.split(','))
                #hextuple = tuple(map(int, hexstr.split(',')))

                if parameter: parameter = int(parameter)
                ac = AlphaCommand(name, codes, default_parameter=parameter)
                
                self.__alphas[codes] = ac
                self.__alphas[name] = ac

    def get_alpha(self, key):        
        if key in self.__alphas:
            return self.__alphas[key]

        alpha = None
        mat = re.match(AlphaCommands.alpha_str, key)
        if mat and mat.group('parameter') and mat.group('alpha') in self.__alphas:
            og_alpha = self.__alphas[mat.group('alpha')]
            alpha = og_alpha.clone(int(mat.group('parameter')))
            
        return alpha

    def get_codes(self, key):
        """Returns list of (integer) codes for alpha command. If passed a code, will just return it"""

        if type(key) is int:
            return [key]
        
        alpha = self.get_alpha(key)
        if alpha:
            return list(alpha.codes)
        else:
            return [int(key, 16)]
